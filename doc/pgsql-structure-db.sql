/*
File name: C:\Users\cuong.ly\Desktop\pgsql-structure-db.sql
Creation date: 05/28/2017
Created by MySQL to PostgreSQL 3.3 [Demo]
--------------------------------------------------
More conversion tools at http://www.convert-in.com
*/

/*
Table structure for table 'public.customers'
*/

DROP TABLE IF EXISTS "public"."customers" CASCADE;
CREATE TABLE "public"."customers" (
	"id" SERIAL NOT NULL,
	"name" VARCHAR(255) ,
	"phone" VARCHAR(45) ,
	"email" VARCHAR(60) ,
	"address" VARCHAR(255) ,
	"gender" VARCHAR(10) ,
	"group_id" DECIMAL(10,0),
	"email_work" VARCHAR(45) ,
	"phone_work" VARCHAR(45) ,
	"status" VARCHAR(45) ,
	"birthday" TIMESTAMP,
	"delivery_address" VARCHAR(45) ,
	"zipcode" VARCHAR(45) ,
	"city" INTEGER,
	"district" INTEGER,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.customers'
*/


/*
Table structure for table 'public.district'
*/

DROP TABLE IF EXISTS "public"."district" CASCADE;
CREATE TABLE "public"."district" (
	"districtid" SERIAL NOT NULL,
	"name" VARCHAR(45) ,
	"type" VARCHAR(45) 
) WITH OIDS;

/*
Dumping data for table 'public.district'
*/


/*
Table structure for table 'public.import_has_product'
*/

DROP TABLE IF EXISTS "public"."import_has_product" CASCADE;
CREATE TABLE "public"."import_has_product" (
	"id" SERIAL NOT NULL,
	"products_id" INTEGER NOT NULL,
	"import_product_id" INTEGER NOT NULL,
	"quantity_product" INTEGER,
	"cost_product" DOUBLE PRECISION,
	"total" DOUBLE PRECISION,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.import_has_product'
*/


/*
Table structure for table 'public.import_product'
*/

DROP TABLE IF EXISTS "public"."import_product" CASCADE;
CREATE TABLE "public"."import_product" (
	"id" SERIAL NOT NULL,
	"users_id" INTEGER NOT NULL,
	"name" VARCHAR(45) ,
	"status" VARCHAR(45) ,
	"description" TEXT,
	"provider" VARCHAR(45) ,
	"total" VARCHAR(45) ,
	"discount" DOUBLE PRECISION,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.import_product'
*/


/*
Table structure for table 'public.order_has_products'
*/

DROP TABLE IF EXISTS "public"."order_has_products" CASCADE;
CREATE TABLE "public"."order_has_products" (
	"id" SERIAL NOT NULL,
	"cost_product" DOUBLE PRECISION,
	"quantity_product" INTEGER,
	"vat_product" DOUBLE PRECISION,
	"discount" DOUBLE PRECISION,
	"total" DOUBLE PRECISION,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"products_id" INTEGER NOT NULL,
	"orders_id" INTEGER NOT NULL
) WITH OIDS;

/*
Dumping data for table 'public.order_has_products'
*/


/*
Table structure for table 'public.orders'
*/

DROP TABLE IF EXISTS "public"."orders" CASCADE;
CREATE TABLE "public"."orders" (
	"id" SERIAL NOT NULL,
	"customers_id" INTEGER NOT NULL,
	"users_id" INTEGER NOT NULL,
	"name" VARCHAR(45) ,
	"description" VARCHAR(45) ,
	"total" VARCHAR(45) ,
	"discount" VARCHAR(45) ,
	"status" INTEGER,
	"created_at" VARCHAR(45) ,
	"updated_at" VARCHAR(45) 
) WITH OIDS;

/*
Dumping data for table 'public.orders'
*/


/*
Table structure for table 'public.permissions'
*/

DROP TABLE IF EXISTS "public"."permissions" CASCADE;
CREATE TABLE "public"."permissions" (
	"id" SERIAL NOT NULL,
	"key" VARCHAR(45) ,
	"name" VARCHAR(45) ,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.permissions'
*/


/*
Table structure for table 'public.products'
*/

DROP TABLE IF EXISTS "public"."products" CASCADE;
CREATE TABLE "public"."products" (
	"id" SERIAL NOT NULL,
	"promotions_id" INTEGER,
	"suppliers_id" INTEGER NOT NULL,
	"name" VARCHAR(255) ,
	"cost" DOUBLE PRECISION,
	"quantity" INTEGER,
	"description" TEXT,
	"expired_date" TIMESTAMP,
	"barcode" VARCHAR(45) ,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.products'
*/


/*
Table structure for table 'public.promotions'
*/

DROP TABLE IF EXISTS "public"."promotions" CASCADE;
CREATE TABLE "public"."promotions" (
	"id" SERIAL NOT NULL,
	"name" VARCHAR(255) ,
	"cost" DOUBLE PRECISION,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.promotions'
*/


/*
Table structure for table 'public.province'
*/

DROP TABLE IF EXISTS "public"."province" CASCADE;
CREATE TABLE "public"."province" (
	"provinceid" SERIAL NOT NULL,
	"name" VARCHAR(45) ,
	"type" VARCHAR(45) ,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.province'
*/


/*
Table structure for table 'public.role_permissions'
*/

DROP TABLE IF EXISTS "public"."role_permissions" CASCADE;
CREATE TABLE "public"."role_permissions" (
	"id" SERIAL NOT NULL,
	"role_id" INTEGER NOT NULL,
	"permission_id" INTEGER NOT NULL,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.role_permissions'
*/


/*
Table structure for table 'public.roles'
*/

DROP TABLE IF EXISTS "public"."roles" CASCADE;
CREATE TABLE "public"."roles" (
	"id" SERIAL NOT NULL,
	"name" VARCHAR(45)  NOT NULL,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.roles'
*/


/*
Table structure for table 'public.suppliers'
*/

DROP TABLE IF EXISTS "public"."suppliers" CASCADE;
CREATE TABLE "public"."suppliers" (
	"id" SERIAL NOT NULL,
	"name" VARCHAR(255) ,
	"phone" VARCHAR(45) ,
	"address" VARCHAR(255) ,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.suppliers'
*/


/*
Table structure for table 'public.user_roles'
*/

DROP TABLE IF EXISTS "public"."user_roles" CASCADE;
CREATE TABLE "public"."user_roles" (
	"id" SERIAL NOT NULL,
	"role_id" INTEGER NOT NULL,
	"user_id" INTEGER NOT NULL,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.user_roles'
*/


/*
Table structure for table 'public.users'
*/

DROP TABLE IF EXISTS "public"."users" CASCADE;
CREATE TABLE "public"."users" (
	"id" SERIAL NOT NULL,
	"name" VARCHAR(255) ,
	"email" VARCHAR(45) ,
	"phone" VARCHAR(45) ,
	"password" VARCHAR(255) ,
	"remember_token" VARCHAR(100) ,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) WITH OIDS;

/*
Dumping data for table 'public.users'
*/

