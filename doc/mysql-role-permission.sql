-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table supermarket.roles: ~1 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Role', '2017-05-28 01:27:48', '2017-05-28 01:27:48');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table supermarket.role_permissions: ~8 rows (approximately)
/*!40000 ALTER TABLE `role_permissions` DISABLE KEYS */;
INSERT INTO `role_permissions` (`id`, `role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
	(3, 1, 27, '2017-05-28 01:28:16', '2017-05-28 01:28:16'),
	(4, 1, 29, '2017-05-28 01:28:27', '2017-05-28 01:28:27'),
	(5, 1, 28, '2017-05-28 01:28:27', '2017-05-28 01:28:27'),
	(6, 1, 30, '2017-05-28 01:28:44', '2017-05-28 01:28:44'),
	(7, 1, 31, '2017-05-28 01:28:56', '2017-05-28 01:28:56'),
	(8, 1, 33, '2017-05-28 01:29:04', '2017-05-28 01:29:04'),
	(9, 1, 32, '2017-05-28 01:29:20', '2017-05-28 01:29:20'),
	(10, 1, 34, '2017-05-28 01:29:34', '2017-05-28 01:29:34');
/*!40000 ALTER TABLE `role_permissions` ENABLE KEYS */;

-- Dumping data for table supermarket.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'administrator', 'admin@gmail.com', NULL, '$2y$10$mDQDTyXpESGmltl6dWJY6OHEXFmlNFOdhJlwwjjijY03kOht8HI6.', NULL, '2017-05-28 01:25:41', '2017-05-28 01:25:41');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table supermarket.user_roles: ~1 rows (approximately)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '2017-05-28 01:27:57', '2017-05-28 01:27:57');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
