<?php
	$config_url =  "http://".$_SERVER['HTTP_HOST']."/supermarket";
?>
<!DOCTYPE HTML>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Your Website</title>
      <!-- FONT AWESOME -->
      <?php include "templates/layout/css_js.php";?>
   </head>
   <body>
      <div id="container">
         <?php include "templates/layout/navleft.php";?>
		 <?php include "templates/layout/index.php";?>
      </div>
   </body>
</html>