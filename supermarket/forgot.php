<?php
	$config_url =  "http://".$_SERVER['HTTP_HOST']."/supermarket";
?>
<!DOCTYPE HTML>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Your Website</title>
      <!-- FONT AWESOME -->
      <?php include "templates/layout/css_js.php";?>
   </head>
   <body>
      <body id="login-form">
		<div class="container main-login">
<form data-toggle="validator" role="form" action="login.php" method="post">     <img class='logo-login' src="<?=$config_url?>/assets/img/logo.png" />
    <p class='title'>Phần mềm quản lý bán hàng</p>
        <div class="inputText" style="text-align:center;">
            <h3 class="text-center margin-xl-bottom">Bạn quên mật khẩu?</h3>
            <p  style="text-align: center; color: #ff6060;">
                Vui lòng nhập địa chỉ Email tài khoản của bạn để lấy lại mật khẩu
            </p>
			<div class="field required form-group">
            <input required class="form-control login-input" data-error="Nhập vào Email" id="Email" name="Email" placeholder="Địa chỉ Email" type="text" value="" />           
            <div class="has-error">
						<span class="help-block with-errors"></span>			
					</div>
			</div>
            <a class="forget-pass" href="login.php">Đăng nhập trở lại</a>

            <input type="submit" value="Gửi" />
        </div>
</form> 
    <img class="line-login" src="<?=$config_url?>/assets/img/line.png" />
    <p class='title'>Tổng đài hỗ trợ khách hàng: 1900_ _ _ _ _</p>
    
</div>


   </body>
</html>