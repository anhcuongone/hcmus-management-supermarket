$(document).ready(function() {
    NProgress.start()
});
$(window).load(function() {
    NProgress.done()
});

$(document).ready(function () {
	$('.format-date').datepicker({
		format: 'mm-dd-yyyy',
			   todayBtn: 'linked'
	});
	
    $(".all-bulk-action").click(function () {
        $(".bulk-action-item").prop('checked', $(this).prop('checked'));
    });
});