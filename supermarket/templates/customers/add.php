<?php
   $config_url =  "http://".$_SERVER['HTTP_HOST']."/supermarket";
   ?>
<!DOCTYPE HTML>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Your Website</title>
      <!-- FONT AWESOME -->
      <?php include "../../templates/layout/css_js.php";?>
   </head>
   <body>
      <div id="container">
         <?php include "../../templates/layout/navleft.php";?>
         <div class="colRight">
            <div class="page-body">
               <div class="container-right">
                  <div class='page-heading page-heading-md page-heading-border-bottom'>
                     <h2 class='header__main'><span class='breadcrumb'><a href=''>khách hàng</a> /</span><span class='title'>Thêm mới khách hàng</span></h2>
                     <div class="header-right">
                     </div>
                  </div>
                  <form action="" data-toggle="validator" role="form" enctype="multipart/form-data" id="createProduct" method="post">
                     <div class="col-md-12 .col-lg-12 .col-xl-12">
                        <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                           <h3>Chi tiết khách hàng</h3>
                           <span class="colorNote">
                           Điền tên khách hàng và thông tin chi tiết. Khách hàng có thể tạo đơn hàng.
                           </span>
                        </div>
                        <div class="col-md-9 .col-lg-9 .col-xl-9 marginContent">
                           <div class="tour-white">
                              <div class="field required form-group">
                                 <label class="control-label">Tên khách hàng</label>
                                 <div class="controls">
                                    <input data-error="Vui lòng nhập tên khách hàng." required class="form-control product-input" id="Name" name="Name" placeholder="Nhập tên khách hàng" type="text" value="" />
                                    <span class="help-block with-errors"></span>
                                 </div>
                              </div>
                              <div class="field form-group">
                                 <label class="control-label">Nhóm khách hàng</label>
                                 <div id="selectSupplier" style="display: inline-block;">
                                    <select data-error="Vui lòng chọn nhà cung cấp." class="product-input form-control" id="supplierid" name="GroupName">
                                       <option selected="selected" value="0">Nhóm khách hàng</option>
                                       <option value="1">Bán sỉ</option>
                                       <option value="2">Bán lẻ</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="field form-group">
                                 <label class="control-label">Mã khách hàng</label>
                                 <div class="controls">
                                    <input class="form-control product-input" name="CustomerCode_code" type="text" placeholder="Nhập mã khách hàng">
                                 </div>
                              </div>
                              <div class="form-group required field">
                                 <label class="control-label font-normal">Số điện thoại</label>
                                 <div class="controls">
                                    <input required data-error="Không được để trống" class="form-control product-input" id="PhoneNumber" name="PhoneNumber" placeholder="Nhập số điện thoại" type="text" value="" />
                                    <span class="help-block with-errors"></span>
                                 </div>
                              </div>
                              <div class="form-group field">
                                 <label class="control-label font-normal"> Email</label>
                                 <div class="controls">
                                    <input class="form-control product-input" id="Email" name="Email" placeholder="Nhập địa chỉ email" type="email" value="" />
                                    <span class="help-block help-block-margin"></span>
                                 </div>
                              </div>
                              <div class="field form-group" id="sex">
                                 <label class="control-label">Giới tính</label>
                                 <div class="controls">
                                    <select class="product-input form-control" name="sex">
                                       <option value="male">Nam</option>
                                       <option value="female">Nữ</option>
                                       <option value="other">Khác</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="field form-group clearfix">
                                 <label class="control-label">Ngày sinh</label>
                                 <div class="controls">
                                    <div class="dateofbirth order-menu-screen__right_detailorder-issued order-item-right-text datepicker-input fl controls" style="border-color: #CBD5DE;">
                                       <input type="text" class="form-control format-date inline filter-input no-margin" id="birth_transdate" name="Dob" placeholder="Nhập ngày sinh" value="">
                                       <span class="input-group-addon padding-lg-right"><i class="fa fa-calendar"></i></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 .col-lg-12 .col-xl-12 ">
                        <div class="h_Box">
                           <div class="col-md-3 .col-lg-3 .col-xl-3 col-sm-3 box-description">
                              <h3>Địa chỉ khách hàng</h3>
                              <span class="colorNote">
                              Thêm địa chỉ khách hàng phục vụ  cho việc tạo đơn hàng, xuất hóa đơn và chuyển hàng.
                              </span>
                           </div>
                           <div class="col-md-9 .col-lg-9 .col-xl-9 col-sm-9 marginContent">
                              <div class="tour-white">
								<div class='row'>
                                 <div class="col-xs-6">
                                    <div class="required form-group field">
                                       <label class="control-label mar-bottom5">Địa điểm</label>
                                       <input required class="form-control product-input" data-error="Không được để trống" id="Address_Label" name="Address.Label" placeholder="Nơi thanh toán, giao hàng" type="text" value="" />
									   <span class="help-block with-errors"></span>
                                    </div>
									
									
                                    <div class="field form-group">
                                       <label class="control-label mar-bottom5">Email</label>
                                       
                                          <input bind="address.email" class="form-control product-input" data-val="true" data-val-email="Email không đúng định dạng" id="Address_Email" name="Address.Email" placeholder="Nhập địa chỉ email" type="text" value="" />
                                          <span class="help-block help-block-margin"><span class="field-validation-valid" data-valmsg-for="Address.Email" data-valmsg-replace="true"></span></span>
                                       
                                    </div>
                                    <div class="field form-group">
                                       <label class="control-label">Số điện thoại</label>
                                       
                                          <input bind="address.phone_number" class="form-control product-input"  id="Address_PhoneNumber" name="Address.PhoneNumber" placeholder="Nhập số điện thoại" type="text" value="" />
                                         
                                       
                                    </div>
                                    <div class="form-group field">
                                       <label class="control-label">Mã vùng</label>
                                       <input bind="zip_code" class="product-input form-control" id="Address_ZipCode" name="Address.ZipCode" placeholder="Nhập mã vùng" type="text" value="" />
                                    </div>
                                 </div>
                                 <div class="col-xs-6">
                                    <div class="field required form-group">
                                       <label class="control-label required  mar-bottom5">Địa chỉ</label>
                                       <input required class="form-control product-input" data-error="Không được để trống" id="Address_Address1" name="Address.Address1" placeholder="Địa chỉ" type="text" value="" />
                                       <span class="help-block with-errors"></span>
                                    </div>
                                    
                                    <div class="field form-group">
                                       <label class="control-label required  mar-bottom5">Tỉnh/Thành phố</label>
                                       <div id="div-city">
                                          <select Id="city" class="product-input form-control" id="CityId" name="CityId">
                                             <option selected="selected" value="0">Chọn Tỉnh/Thành phố</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="field form-group">
                                       <label class="control-label required  mar-bottom5">Quận/Huyện</label>
                                       <div id="div-district">
                                          <select Id="district" class="product-input form-control" id="DistrictId" name="DistrictId" styledropdown="width: 200px;left: 160px;">
                                             <option selected="selected" value="0">Chọn Quận/Huyện</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
								 
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="fixed-btm-padding">
                        <div class="fixed-btm-actions products form-group" style="margin-bottom: 0;">
                           <div class="actions">
                              <div class="right">
                                 <button type="reset" class="buttonfooter button-cancel">
                                 Hủy bỏ
                                 </button>
                                 <button class="buttonfooter button_primary website-submit" type="submit">
                                 Thêm
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>