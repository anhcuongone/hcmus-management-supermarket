<?php
   $config_url =  "http://".$_SERVER['HTTP_HOST']."/supermarket";
   ?>
<!DOCTYPE HTML>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Your Website</title>
      <!-- FONT AWESOME -->
      <?php include "../../templates/layout/css_js.php";?>
   </head>
   <body>
      <div id="container">
         <?php include "../../templates/layout/navleft.php";?>
         <div class="colRight">
            <div id="load-view-right-content" class="fix-height-right-content">
               <div id="product-detail-form">
                  <form method="get" action="" id="fatherFormFilterAndSavedSearch">
                     <div class="header-common-right">
                        <div class="page-heading page-heading-md page-heading-border-bottom">
                           <h2 class="header__main"><span class="title">Khách hàng</span></h2>
                           <div class="header-right">
                              <div>
                                 <div class="header-fr">
                                    <a class="btn btn-default pull-right btn-a-active" href="<?=$config_url?>/templates/customers/add.php" style="margin-right:0px;">
                                    Thêm mới
                                    </a>
                                 </div>
                                 <div class="header-fr header-button-margin">
                                    <a href="javascript:void(0)" class="btn btn-default pull-right header-button-border-default header-button-gl export_supplier">
                                    Xuất file
                                    </a>
                                 </div>
                                 <div class="header-fr">
                                    <a href="javascript:void(0)" class="btn btn-default pull-right header-button-border-default header-button-gr import_supplier">
                                    Nhập file
                                    </a>
                                 </div>
                              </div>
                              <div class="search-layout-common header-fr header-input-search" style="margin-right: 8px; background: white; text-align: left;">
                                 <i class="fa fa-search"></i>
                                 <input type="text" class="form-control search-input ui-autocomplete-input" placeholder="Tìm kiếm khách hàng ..." value="">
                              </div>
                           </div>
                        </div>
                        <div class="filter-search-nav">
                           <ul class="filter-tab-list overflow-hidden-x ps-container ps-theme-default" id="filter-tab-list" style="position: relative;">
                              <li class="filter-tab-item filter-tab-active" data-tab-index="1" style="">
                                 <a href="javascript:void(0)" class="filter-tab filter-tab-active">
                                 Danh sách khách hàng
                                 </a>
                              </li>
							  <li class="filter-tab-item main-filter" id="remove-filter-common">
								<a href="#" class="filter-tab"><i class="fa fa-times" aria-hidden="true"></i> Xóa</a>
							  </li>

                           </ul>
                        </div>
                     </div>
                     <div class="boder-table">
                        <div id="table-height" class="bulk-action-context" >
                           <table class="table defaul-table" id="parent-variant">
                              <thead>
                                 <tr>
                                    <th style="width: 3%;" class="text-center">
                                       <span><input type="checkbox" class='js-no-dirty all-bulk-action'></span>
                                    </th>
                                    <th style="width: 12%;">Mã</th>
									<th style="width: 15%;">Tên</th>
                                    <th style="width: 15%;">Nhóm khách hàng</th>
                                    <th style="width: 12%;">Email liên hệ</th>
									<th style="width: 10%;" class='text-center'>Số điện thoại</th>
									<th style="width: 13%;" class='text-center'>Trạng thái</th>
									<th style="width: 10%;" class='text-center'>Edit</th>
                                 </tr>
                              </thead>
                              <tbody class="tbody-scoler">
								<?php for($i=0; $i< 10; $i++) {?>
                                 <tr>
                                    <td class="left-td"><input type="checkbox" value="" class="bulk-action-item"></td>
							
                                    <td class="left-td"><span>MAKH</span></td>
                                    <td class="left-td"><span>TENKH</span></td>
                                    <td class="left-td"><span>(Bán Sỷ, Bán lẻ)</span></td>
                                    <td class="left-td"><span>email@gmail.com</span></td>
									<td class="left-td text-center"><span>09999999999</span></td>
									<td class="left-td text-center"><span>Trạng thái</span></td>
									<td class="left-td text-center">
										<a href="#">Edit</a>
										&nbsp;&nbsp;|&nbsp;
										<a href="#">Delete</a>
									</td>
                                 </tr>
								<?php } ?>
                              </tbody>
                           </table>
                        </div>
                        <div class="t-grid-pager-boder row">
                           <div class="t-pager t-reset">
                              <div class="col-xs-4">
                                 <div class="t-status-text dataTables_info">Hiển thị kết quả từ 1 - 1 trên tổng 1 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>