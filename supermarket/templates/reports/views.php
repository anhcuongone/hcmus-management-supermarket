<?php
   $config_url =  "http://".$_SERVER['HTTP_HOST']."/supermarket";
   ?>
<!DOCTYPE HTML>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Your Website</title>
      <!-- FONT AWESOME -->
      <?php include "../../templates/layout/css_js.php";?>
   </head>
   <body>
      <div id="container">
      <?php include "../../templates/layout/navleft.php";?>
      <div class="colRight">
         <div class="container-report">
            <div context="dasboardReport" class="dasboardReport" define="{dasboardReport: new Sapo.dasboardReport(this,{})}">
               <div class="title-product " style="background-color:#eaeff3">
                  <div class="col-md-12 " style="font-size: 20px;">
                     Báo cáo bán hàng
                  </div>
               </div>
               <div class="content-right-product container-report__main" style="overflow:initial">
                  <div class="col-md-12 content-table">
                     <div class="col-md-6">
                        <div class='box-sale'>
                           <a href="">
                              <div class="img">
                                 <img width="60" height="60" src="<?=$config_url?>/assets/img/bao-cao-ban-hang.png" />
                              </div>
                              <div class="text-report">
                                 <div class="text-head">Báo cáo bán hàng cuối ngày</div>
                                 <div>Quản lý doanh thu, thực thu, tiền hàng trả lại trong một ngày.</div>
                              </div>
                           </a>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class='box-sale'>
                           <a href="">
                              <div class="img">
                                 <img width="60" height="60" src="<?=$config_url?>/assets/img/ban-hang-theo-tg.png" />
                              </div>
                              <div class="text-report">
                                 <div class="text-head">Báo cáo bán hàng theo thời gian</div>
                                 <div>Theo dõi được doanh thu bán hàng theo thời gian của một chi nhánh hoặc cả hệ thống.</div>
                              </div>
                           </a>
                        </div>
                     </div>
					 </div>
                     <div class="col-md-12 content-table">
                        <div class="col-md-6">
                           <div class='box-sale'>
                              <a href="">
                                 <div class="img">
                                    <img width="60" height="60" src="<?=$config_url?>/assets/img/ban-chay.png" />
                                 </div>
                                 <div class="text-report">
                                    <div class="text-head">Báo cáo hàng bán chạy</div>
                                    <div>Theo dõi được mặt hàng bán chạy trong 1 khoảng thời gian.</div>
                                 </div>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class='box-sale'>
                              <a href="">
                                 <div class="img">
                                    <img width="60" height="60" src="<?=$config_url?>/assets/img/theo-kho-hang.png" />
                                 </div>
                                 <div class="text-report">
                                    <div class="text-head">Báo cáo bán hàng theo chi nhánh</div>
                                    <div>Theo dõi được doanh thu bán hàng theo chi nhánh của 1 chi nhánh hoặc cả hệ thống.</div>
                                 </div>
                              </a>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 content-table">
                        <div class="col-md-6">
                           <div class='box-sale'>
                              <a href="">
                                 <div class="img">
                                    <img width="60" height="60" src="<?=$config_url?>/assets/img/tra-hang.png" />
                                 </div>
                                 <div class="text-report">
                                    <div class="text-head">Báo cáo trả hàng</div>
                                    <div>Theo dõi được số mặt hàng trả và giá trị hàng trả.</div>
                                 </div>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class='box-sale'>
                              <a href="">
                                 <div class="img">
                                    <img width="60" height="60" src="<?=$config_url?>/assets/img/theo-nhan-vien.png" />
                                 </div>
                                 <div class="text-report">
                                    <div class="text-head">Báo cáo bán hàng theo nhân viên</div>
                                    <div>Theo dõi được doanh thu bán hàng theo nhân viên của 1 chi nhánh hoặc cả hệ thống.</div>
                                 </div>
                              </a>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 content-table">
                        <div class="col-md-6">
                           <div class='box-sale'>
                              <a href="">
                                 <div class="img">
                                    <img width="60" height="60" src="<?=$config_url?>/assets/img/don-hang-chi-tiet.png" />
                                 </div>
                                 <div class="text-report">
                                    <div class="text-head">Báo cáo bán hàng theo đơn hàng chi tiết</div>
                                    <div>Theo dõi được doanh thu bán hàng theo đơn hàng chi tiết của 1 chi nhánh hoặc cả hệ thống.</div>
                                 </div>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class='box-sale'>
                              <a href="">
                                 <div class="img">
                                    <img width="60" height="60" src="<?=$config_url?>/assets/img/theo-khach-hang.png" />
                                 </div>
                                 <div class="text-report">
                                    <div class="text-head">Báo cáo bán hàng theo khách hàng</div>
                                    <div>Theo dõi được doanh thu bán hàng theo khách hàng của 1 chi nhánh hoặc cả hệ thống.</div>
                                 </div>
                              </a>
                           </div>
                        </div>
                     </div>
                  
                  <div class="col-md-12 content-table">
                     <div class="col-md-6">
                        <div class='box-sale'>
                           <a href="">
                              <div class="img">
                                 <img width="60" height="60" src="<?=$config_url?>/assets/img/report-variant.png" />
                              </div>
                              <div class="text-report">
                                 <div class="text-head">Báo cáo tồn kho</div>
                                 <div>Quản lý giá trị tồn kho của chi nhánh và toàn hệ thống</div>
                              </div>
                           </a>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class='box-sale'>
                           <a href="">
                              <div class="img">
                                 <img width="60" height="60" src="<?=$config_url?>/assets/img/by-source.png" />
                              </div>
                              <div class="text-report">
                                 <div class="text-head">Xuất nhập tồn</div>
                                 <div>Quản lý giá trị tồn đầu kì, nhập trong kì, xuất trong kì, tồn cuối kì.</div>
                              </div>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>