<?php
   $config_url =  "http://".$_SERVER['HTTP_HOST']."/supermarket";
   ?>
<!DOCTYPE HTML>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Your Website</title>
      <!-- FONT AWESOME -->
      <?php include "../../templates/layout/css_js.php";?>
   </head>
   <body>
      <div id="container" class='nav-active'>
         <?php include "../../templates/layout/navleft.php";?>
         <div class="colRight">
            
            <div class="page-body">
               <div class="container-right">
                  <div class='page-heading page-heading-md page-heading-border-bottom'>
                     <h2 class='header__main'><span class='breadcrumb'><a href=''>Nhà cung cấp</a> /</span><span class='title'>Thêm mới nhà cung cấp</span></h2>
                     <div class="header-right">
                     </div>
                  </div>
                  <form action="" data-toggle="validator" role="form" enctype="multipart/form-data" id="form-create-supplier" method="post">
                     <div class="row">
                        <div class="col-md-12 .col-lg-12 .col-xl-12">
                           <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                              <h3>Chi tiết nhà cung cấp</h3>
                              <span class="colorNote">
                              Điền tên và thông tin chi tiết của nhà cung cấp. Nhà cung cấp có thể tạo đơn nhập hàng
                              </span>
                           </div>
                           <div class="col-md-9 .col-lg-9 .col-xl-9 marginContent">
                              <div class="tour-white">
                                 <div class="field required form-group">
                                    <label class="control-label">Tên nhà cung cấp</label>
                                    <div class="controls">
                                       <input required data-error="Không được để trống." class="form-control product-input" id="Name" name="Name" placeholder="Nhập tên nhà cung cấp" type="text" value="" />
									   <span class="help-block with-errors"></span>
                                    </div>
                                 </div>
                                 <div class="field form-group">
                                    <label class="control-label">Mã nhà cung cấp</label>
                                    <input class="form-control product-input" id="SupplierCode_code" name="SupplierCode.code" placeholder="Nhập mã nhà cung cấp" type="text" value="" />
                                    <span class="help-block help-block-margin"><span class="field-validation-valid" ></span></span>
                                 </div>
                                 <div class=" form-group field">
                                    <label class="control-label">Số điện thoại</label>
                                    <div class="controls">
                                       <input class="form-control product-input" id="PhoneNumber" name="PhoneNumber" placeholder="Nhập số điện thoại" type="text" value="" />
                                    </div>
                                 </div>
                                 <div class=" form-group field">
                                    <label class="control-label"> Email</label>
                                    <div class="controls">
                                       <input class="form-control product-input" id="Email" name="Email" placeholder="Nhập địa chỉ email" type="email" value="" />
                                       <span class="help-block help-block-margin"><span class="field-validation-valid"></span></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12 .col-lg-12 .col-xl-12">
                           <div class="h_Box">
                              <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                                 <h3>Địa chỉ nhà cung cấp</h3>
                                 <span class="colorNote">
                                 Thêm địa chỉ nhà cung cấp để phục vụ cho việc tạo đơn nhập hàng sau
                                 </span>
                              </div>
                              <div class="col-md-9 .col-lg-9 .col-xl-9 col-sm-9 marginContent" >
                                 <div class="tour-white">
                                       <div class="field required form-group">
                                          <label class="control-label mar-bottom5">Địa điểm</label>
										  <div class="controls">
                                          <input required data-error="Không được để trống." class="form-control product-input" id="Address_Label" name="Address.Label" placeholder="Nơi thanh toán, giao hàng" type="text" value="" />
                                          <span class="help-block with-errors"></span>
										  </div>
                                       </div>
                                       <div class="field form-group">
                                          <label class="control-label mar-bottom5">Email</label>
                                          
										 <input class="form-control product-input" id="Address_Email" name="Address.Email" placeholder="Nhập địa chỉ email" type="email" value="" />
									  
                                       </div>
                                       <div class="field form-group">
                                          <label class="control-label">Số điện thoại</label>
                                          <div class="controls">
                                             <input class="form-control product-input" id="Address_PhoneNumber" name="Address.PhoneNumber" placeholder="Nhập số điện thoại" type="text" value="" />
                                          </div>
                                       </div>
                                    
                                       <div class="field required form-group">
                                          <label class="control-label  mar-bottom5">Địa chỉ</label>
										  <div class="controls">
                                          <input required data-error="Không được để trống." class="form-control product-input" id="Address_Address1" name="Address.Address1" placeholder="Nhập địa chỉ" type="text" value="" />
                                          <span class="help-block with-errors"></span>
										  </div>
                                       </div>
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="fixed-btm-padding">
                        <div class="fixed-btm-actions products form-group" style="margin-bottom: 0;">
                           <div class="actions">
                              <div class="right pull-right">
                                 <button type="reset" class="buttonfooter button-cancel">
                                 Hủy bỏ
                                 </button>
                                 <button type="submit" class="buttonfooter button_primary">
                                 Thêm
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>