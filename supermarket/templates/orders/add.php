<?php
   $config_url =  "http://".$_SERVER['HTTP_HOST']."/supermarket";
   ?>
<!DOCTYPE HTML>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Your Website</title>
      <!-- FONT AWESOME -->
      <?php include "../../templates/layout/css_js.php";?>
   </head>
   <body>
      <div id="container" class='nav-active'>
         <?php include "../../templates/layout/navorder.php";?>
         <div class="colRight">
            <div class="page-body website-menu-expand" id="load-view-right-content">
               <div context="editOrder">
                  <form action="/admin/orders/create" enctype="multipart/form-data" id="formOrder" method="post" onsubmit="return false;">
                     <div class='page-heading page-heading-md'>
                        <h2 class='header__main'><span class='breadcrumb'><a href=''>Đơn hàng</a> /</span><span class='title'>Tạo đơn hàng</span></h2>
                        <div class="header-right">
                           <a class="btn btn-default pull-right btn-a-active" target="_blank" href="">Tạo đơn hàng</a>
                        </div>
                     </div>
                     
                     
                     <div class="order-menu-screen">
                        <div class="menu-left-new">
                           <div class="order-menu-screen__left order-menu-screen__left-order " id="order-menu-screen__left" style="position: relative;">
                              
                              <div class="order-menu-screen__left-scroll w100">
                                 <div style="padding: 0 7.5px" class="clearfix">
                                    <div class="col-sm-3 no-padding">
                                       <div class="form-group col-sm-12 no-padding left-inner-addon disabled" style="width: 100%; height: 69.5px;">
                                          <div id="input-customer">
                                             <i class="fa fa-user"></i>
                                             <input type="text" style="height: 29px;" class="form-control search-customer" placeholder="Chọn khách hàng" />
                                          </div>
                                          
                                       </div>
                                       
                                       <div class="form-group col-sm-12 search-layout-common no-padding no-margin" style="width: 100%">
                                          <i class="fa fa-search"></i>
                                          <input style="height: 29px; font-size: 12px;" id="search-input-prd" type="text" class="form-control search-input ui-autocomplete-input" placeholder="Tìm kiếm sản phẩm">
                                       </div>
                                    </div>
                                    <div class="col-sm-9 no-padding">
                                       <div class="col-sm-12 no-padding">
                                          <div class="form-group col-sm-4 no-padding ui-autocomplete-group " id="address-shipto-select">
                                             <input class="form-control shipto-select" placeholder="Chọn nơi giao đến"/>
                                          </div>
                                          <div class="form-group col-sm-4 no-padding  ">
                                             <select class="form-control" id="LocationId" name="LocationId" style="">
                                                <option selected="selected" value="">Chính sách giá</option>
												<option value="">Giá bán lẻ</option>
												<option value="">Giá buôn</option>
                                             </select>
                                          </div>
                                          <div class="form-group col-sm-4 no-padding">
                                             
                                             <input type="text" id="input-ordercode" class="form-control"value="" placeholder="Mã đơn hàng">
                                          </div>
                                       </div>
                                       <div class="col-sm-12 no-padding">
                                          <div class="form-group col-sm-4 no-padding">
                                             <input class="form-control" id="PhoneNumber" name="PhoneNumber" placeholder="Điện thoại" type="text" value="" />
                                          </div>
                                          <div class="form-group col-sm-4 no-padding">
                                             <div class="form-datepicker-common">
                                                <input type="text" class="format-date form-control inline filter-input no-margin ship_on" id="transdate" placeholder="Hẹn giao hàng">
                                                <i class="fa fa-calendar"></i>
                                             </div>
                                             <span style="position: absolute; right: 6px; top: 9px;" class="helpAddmin"title="">
                                             
                                             </span>
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       <div class="col-sm-12 no-padding ">
                                          <div class="form-group col-sm-4 no-padding">
                                             <select class="form-control" id="LocationId" name="LocationId" style="" style="width: calc(100% - 15px);left: 7.5px;">
                                                <option selected="selected" value="17465">Chi nhánh mặc định</option>
                                             </select>
                                          </div>
                                          <div class="form-group col-sm-4 no-padding">
                                             <select select-value="0" class="form-control valid" styledropdown="width: calc(100% - 15px);left: 7.5px;" id="TaxCheck" name="TaxCheck">
                                                <option value="0">Giá chưa bao gồm thuế</option>
                                                <option value="1">Giá đã bao gồm thuế</option>
                                             </select>
                                          </div>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 <div class="order-menu-screen__left-variant clearfix">
                                    <div class="order-menu-screen__left-variant-scroll w100 table-order-new">
                                       <table class="table defaul-table popup-house table-order-edit" id="table-order">
                                          <thead>
                                             <tr>
                                                <th style="width: 12%" class="left-th">Mã SKU</th>
                                                <th style="width: 28%" class="left-th">Tên hàng hóa</th>
                                                <th style="width: 12%" class="right-th">Số lượng</th>
                                                <th style="width: 12%" class="right-th">Đơn giá</th>
                                                <th style="width: 12%" class="right-th">Chiết khấu</th>
                                                <th style="width: 10%" class="right-th">Thuế (%)</th>
                                                <th style="width: 12%" class="right-th">Thành tiền</th>
                                                <th style="width: 2%"></th>
                                             </tr>
                                          </thead>
                                          <tbody id="line_item_rows">
                                             
                                             
                                          </tbody>
                                       </table>
                                       <div id="no-prd">
                                          
                                       </div>
                                    </div>
                                 </div>
								 
								 
							
									
										 <div class="order-menu-screen__left-totalamount" style="bottom: 46px;display:block">
											<div class='clearfix'>
									<div class='col-md-6 col-xs-12 pull-right'>
											<div class="order-menu-screen__left-totalamount_total order-payment-total__head pull-left">
											   <ul>
												  <li>
													 <span class="order-payment-total__head_text">Tổng tiền</span>
													 <span class="order-payment-total__head_value">0đ</span>
												  </li>
											   </ul>
											
											   <ul>
												  <li>
													 <span style="color: #1cb467;" class="order-payment-total__head_text discount-order">
													 Chiết khấu <i class="fa fa-caret-down"></i>
													 </span>
													 <span class="order-payment-total__head_value">0đ</span>
												  </li>
											   </ul>
											   <ul>
												  <li>
													 <span style="color: #1cb467;" class="order-payment-total__head_text shipping-cost"> Phí giao hàng <i class="fa fa-caret-down"></i></span>
													 <span class="order-payment-total__head_value">0đ</span>
												  </li>
											   </ul>
											   <ul>
												  <li>
													 <span class="order-payment-total__head_text" style="font-weight: 500;">Khách phải trả</span>
													 <span class="order-payment-total__head_value" style="font-weight: 500;">0đ</span>
												  </li>
											   </ul>
											</div>
										 </div>
										 
									
									
									<div class="col-md-6 col-xs-12">
										<div class="fixed-noteTag">
											 <textarea class="form-control" cols="20" id="Note" name="Note" placeholder="Ghi chú" rows="2" style="margin-bottom:7px;padding:7px 10px">
											 </textarea>
											 
										  </div>
									</div>
									</div>
								 </div>
                              </div>
                              
                              <div class="order-menu-screen__left-payment" style="background: #fff;">
                                 <div class="actions">
                                    <div class="left" style="width: 38%; display: inline-block">
                                    </div>
                                    <div class="right">
                                       <a href="/admin/orders/create" class="buttonfooter button-cancel" style="text-align: center;">
                                       Hủy
                                       </a>
                                       <a href="javascript: void(0)" class="buttonfooter button_primary website-submit" style="padding: 6px 14px; width: 96px; text-align: center;">
                                       Đặt hàng
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                       
                        <div class="order-menu-screen__right pull-left menu-right-new" id="order-menu-screen__right">
                           <div class="order-menu-screen__right-scoll pull-left w100 ">
                              <div class="order-menu-screen__right-scoll_invoice pull-left w100">
                                 <div class="order-menu-screen__right-scoll_invoice-header pull-left w100">
                                    <div class="order-menu-screen__right-scoll_invoice-header-left pull-left" style="width: 100%;">
                                       <i class="fa fa-file-text"></i> Đơn hàng<span class='pull-right'></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                    
                  </form>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>