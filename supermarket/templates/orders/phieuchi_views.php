<?php
   $config_url =  "http://".$_SERVER['HTTP_HOST']."/supermarket";
   ?>
<!DOCTYPE HTML>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Your Website</title>
      <!-- FONT AWESOME -->
      <?php include "../../templates/layout/css_js.php";?>
   </head>
   <body>
      <div id="container" class='nav-active'>
         <?php include "../../templates/layout/navorder.php";?>
         <div class="colRight">
            <div id="load-view-right-content" class="fix-height-right-content">
               <div id="product-detail-form">
                  <form method="get" action="" id="fatherFormFilterAndSavedSearch">
                     <div class="header-common-right">
                        <div class="page-heading page-heading-md page-heading-border-bottom">
                           <h2 class="header__main"><span class="title">Phiếu chi</span></h2>
                           <div class="header-right">
                              <div>
                                 <div class="header-fr">
                                    <a class="btn btn-default pull-right btn-a-active" href="<?=$config_url?>/templates/orders/phieuchi_add.php" style="margin-right:0px;">
                                    Thêm mới
                                    </a>
                                 </div>
                                 
                              </div>
                              <div class="search-layout-common header-fr header-input-search" style="margin-right: 8px; background: white; text-align: left;">
                                 <i class="fa fa-search"></i>
                                 <input type="text" class="form-control search-input ui-autocomplete-input" placeholder="Tìm kiếm Phiếu chi ..." value="">
                              </div>
                           </div>
                        </div>
                        <div class="filter-search-nav">
                           <ul class="filter-tab-list overflow-hidden-x ps-container ps-theme-default" id="filter-tab-list" style="position: relative;">
                              <li class="filter-tab-item filter-tab-active" data-tab-index="1" style="">
                                 <a href="javascript:void(0)" class="filter-tab filter-tab-active">
                                 Danh sách Phiếu chi
                                 </a>
                              </li>
							  <li class="filter-tab-item main-filter" id="remove-filter-common">
								<a href="#" class="filter-tab"><i class="fa fa-times" aria-hidden="true"></i> Xóa</a>
							  </li>

                           </ul>
                        </div>
                     </div>
                     <div class="boder-table">
                        <div id="table-height" class="bulk-action-context" >
                           <table class="table defaul-table" id="parent-variant">
                              <thead>
                                 <tr>
                                    <th style="width: 3%;" class="text-center">
                                       <span><input type="checkbox" class='js-no-dirty all-bulk-action'></span>
                                    </th>
                                    <th style="width: 10%;">Mã phiếu</th>
									<th style="width: 10%;">Đối tượng</th>
                                    <th style="width: 12%;">Loại phiếu</th>
                                    <th style="width: 16%;">Phương thức thanh toán</th>
									<th style="width: 12%;">Người tạo</th>
									<th style="width: 10%;" class='text-right'>Số tiền chi</th>
									<th style="width: 10%;" class='text-center'>Ngày ghi nhận</th>
									<th style="width: 10%;" class='text-center'>Edit</th>
                                 </tr>
                              </thead>
                              <tbody class="tbody-scoler">
								<?php for($i=0; $i< 10; $i++) {?>
                                 <tr>
                                    <td class="left-td"><input type="checkbox" value="" class="bulk-action-item"></td>
                                    <td class="left-td"><span>MAPHIEU</span></td>
                                    <td class="left-td"><span>Nhà cung cấp</span></td>
                                    <td class="left-td"><span>Thu nợ khách hàng</span></td>
                                    <td class="left-td"><span>Tiền mặt</span></td>
									<td class="left-td"><span>Ninh Bùi</span></td>
									<td class="left-td text-right"><span>1.000.000đ</span></td>
									<td class="left-td"><span><?=date("d/m/Y", time());?></span></td>
									<td class="left-td text-center">
										<a href="#">Edit</a>
										&nbsp;&nbsp;|&nbsp;
										<a href="#">Delete</a>
									</td>
                                 </tr>
								<?php } ?>
                              </tbody>
                           </table>
                        </div>
                        <div class="t-grid-pager-boder row">
                           <div class="t-pager t-reset">
                              <div class="col-xs-4">
                                 <div class="t-status-text dataTables_info">Hiển thị kết quả từ 1 - 1 trên tổng 1 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>