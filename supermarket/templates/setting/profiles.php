<?php
   $config_url =  "http://".$_SERVER['HTTP_HOST']."/supermarket";
   ?>
<!DOCTYPE HTML>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Your Website</title>
      <!-- FONT AWESOME -->
      <?php include "../../templates/layout/css_js.php";?>
   </head>
   <body>
      <div id="container">
         <?php include "../../templates/layout/navleft.php";?>
         <div class="colRight">
            <div class="page-body">
               <div class="container-right">
                  <div class="title-product ">
                     <div class='page-heading page-heading-md'>
                        <h2 class='header__main'><span class='title'>Thông tin nhân viên</span></h2>
                        <div class="header-right">
                        </div>
                     </div>
                  </div>
                  <form action="" data-toggle="validator" role="form" enctype="multipart/form-data" id="updateCurrentAccount" method="post">
                     <input bind="id" data-val="true" data-val-number="The field Id must be a number." hidden="hidden" id="Id" name="Id" type="text" value="19266" />            
                     <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="col-md-3 col-lg-3 col-xl-3 box-description">
                        </div>
                        <div class="col-md-9 col-lg-9 col-xl-9 marginContent">
                           <div class="field required form-group ">
                              <label class="control-label">Tên nhân viên</label>
                              <div class="controls">
                                 <input required class="form-control product-input"  data-error="Không được để trống" id="FullName" maxlength="250" name="FullName" placeholder="Nhập tên" type="text" value="Ninh Bùi" />
                                 <span class="help-block with-errors"></span>
                              </div>
                           </div>
                           <div class="field required form-group">
                              <label class="control-label">Số điện thoại</label>
                              <div class="controls">
                                 <input required class="form-control product-input" data-error="Không được để trống" id="Mobile" maxlength="250" name="Mobile" placeholder="Nhập số điện thoại" type="text" value="01223344556" />
                                 <span class="help-block with-errors"></span>
                              </div>
                           </div>
                           <div class="field form-group">
                              <label class="control-label">Địa chỉ</label>
                              <div class="controls">
                                 <input bind="address" class="form-control product-input" id="Address" maxlength="250" name="Address" placeholder="Nhập địa chỉ" type="text" value="Khu PMQT" />
                              </div>
                           </div>
                           <div class="field form-group clearfix">
                              <label class="control-label">Ngày sinh</label>
                              <div class="controls">
                                 <div class="dateofbirth order-menu-screen__right_detailorder-issued order-item-right-text datepicker-input fl controls" style="border-color: #CBD5DE">
                                    <input type="text" class="format-date form-control inline filter-input no-margin" id="birth_transdate" name="Dob" placeholder="Nhập ngày sinh" value="">
                                    <span class="input-group-addon padding-lg-right"><i class="fa fa-calendar"></i></span>
                                 </div>
                              </div>
                           </div>
                           <div class="field form-group clearfix">
                              <label class="control-label">Email</label>
                              <label class='text-left'>ninhbui.nina@gmail.com</label>
                           </div>
                           <div class="form-group field">
                              <label class="control-label">Mật khẩu</label>
                              <table class="product-description" style="border:none">
                                 <tr>
                                    <td style="border:none; padding-top:5px">
                                       <label style="width: 65%; text-align:left;">********</label>
                                       <a class="btn-a-active" data-toggle="modal" data-target="#exampleModal" style="color: #27B466; width: 20%; text-align: right;">Đổi mật khẩu</a>
                                    </td>
                                 </tr>
                              </table>
                           </div>
                           <div class="field form-group clearfix">
                              <label class="control-label">Ghi chú</label>
                              <table class="product-description" style="border:none">
                                 <tr>
                                    <td style="border:none; padding-top:5px">
                                       <textarea bind="decription" class="form-control" cols="20" id="Description" name="Description" placeholder="" rows="2" style="height: 120px; width:80%;border-radius:0" type="text">
                                       </textarea>
                                       <span class="field-validation-valid" data-valmsg-for="Description" data-valmsg-replace="true"></span>
                                    </td>
                                 </tr>
                              </table>
                           </div>
						   <div class="field form-group clearfix">
                              <label class="control-label">Email</label>
                              <label class="text-left">ninhbui.nina@gmail.com</label>
                           </div>
                           <div class="field form-group clearfix">
                              <label class="control-label">Trạng thái làm việc</label>
                              <label class="text-left">Đang làm việc</label>
                           </div>
                           <div class="field form-group clearfix">
                              <label class="control-label">Chi nhánh làm việc</label>
                              <label class="text-left" style="width:500px">Chi nhánh mặc định</label>
                           </div>
                        </div>
                     </div>
                     <div class="fixed-btm-padding">
                        <div class="fixed-btm-actions products form-group" style="margin-bottom: 0;">
                           <div class="actions">
                              <div class="pull-right">
                                 <button class="buttonfooter button_primary website-submit" type="submit">
                                 Cập nhật
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
	  
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
         <div class="modal-dialog" role="document">
            <div class="modal-content" style='max-width: 500px; margin: 0 auto;'>
               <div>
                  <form action="" enctype="multipart/form-data" id="form-change-password" method="post">
                     <div class="modal-header">
                        <button class="close" type="button"></button>
                        <span class="ui-dialog-title" id="ui-dialog-title-1">
                        Đổi mật khẩu
                        </span>
                        <a href="#" class="ui-dialog-titlebar-close ui-corner-all">
                        <span class="ui-icon ui-icon-closethick"></span>
                        </a>
                     </div>
                     <div class="modal-body" style="padding-left: 10px;">
                        <div id="new-currency">
                           <div style="width:90%;margin:0 auto">
                              <div class="row">
                                 <div class="field required form-group">
                                    <label class="control-label text-right ">Mật khẩu hiện tại</label>
                                    <div class="controls">
                                       <input required class="form-control product-input"data-error="Không được để trống" id="Password" maxlength="255" name="Password" placeholder="Nhập mật khẩu hiện tại" type="password">
                                       <span class="help-block with-errors"></span>
                                    </div>
                                 </div>
                                 <div class="field form-group">
                                    <label class="control-label text-right ">Mật khẩu mới</label>
                                    <div class="controls">
                                       <input required class="form-control product-input" data-error="Không được để trống" id="NewPassword" maxlength="255" name="NewPassword" placeholder="Nhập mật khẩu mới" type="password">
                                       <span class="help-block with-errors"></span>
                                    </div>
                                 </div>
                                 <div class="field form-group">
                                    <label class="control-label text-right ">Nhập lại mật khẩu</label>
                                    <div class="controls">
                                       <input required class="form-control product-input" data-error="Không được để trống" id="SubmitPassword" maxlength="255" name="SubmitPassword" placeholder="Nhập lại mật khẩu mới" type="password">
                                       <span class="help-block with-errors"></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <div class="action-btn clearfix">
                           <button class="btn btn-default pull-right " type="submit">Lưu</button>
                           <button class="btn btn-closes" id="close-popup" type="button" data-dismiss="modal">Thoát</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
   </body>
</html>