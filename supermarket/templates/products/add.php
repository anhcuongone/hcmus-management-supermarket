<?php
   $config_url =  "http://".$_SERVER['HTTP_HOST']."/supermarket";
   ?>
<!DOCTYPE HTML>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Your Website</title>
      <!-- FONT AWESOME -->
      <?php include "../../templates/layout/css_js.php";?>
   </head>
   <body>
      <div id="container" class='nav-active'>
         <?php include "../../templates/layout/navleft.php";?>
         <div class="colRight">
           
            <div class="page-body">
               <div class="container-right">
                  <div class='page-heading page-heading-md page-heading-border-bottom'>
                     <h2 class='header__main'><span class='breadcrumb'><a href=''>Hàng hóa</a> /</span><span class='title'>Thêm mới hàng hóa</span></h2>
                     <div class="header-right">
                     </div>
                  </div>
                  <form action="" data-toggle="validator" role="form" enctype="multipart/form-data" id="createProduct" method="post">
                     <div class="col-md-12 .col-lg-12 .col-xl-12">
                        <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                           <h3>Chi tiết hàng hóa</h3>
                           <span class="colorNote">
                           Nhập thông tin chi tiết như tên, loại hàng hóa, nhà cung cấp để Thêm mới hàng hóa của bạn.<br />
                           </span>
                        </div>
                        <div class="col-md-9 .col-lg-9 .col-xl-9 marginContent">
                           <div class="tour-white">
                              <div class="field required form-group">
                                 <label class="control-label">Tên hàng hóa</label>
                                 <div class="controls">
                                    <input data-error="Vui lòng nhập tên sản phẩm." required class="form-control product-input" id="Name" name="Name" placeholder="Nhập tên sản phẩm" type="text" value="" />
									<span class="help-block with-errors"></span>
                                 </div>
                              </div>
							  
                              <div class="field form-group">
								 <label class="control-label">Nhà cung cấp</label>
                                 <div id="selectSupplier" style="display: inline-block;">
                                    <select data-error="Vui lòng chọn nhà cung cấp." class="product-input form-control" id="supplierid" name="SupplierId">
                                       <option selected="selected" value="0">Chọn nhà cung cấp</option>
                                    </select>
						
                                 </div>
                              </div>
							  
							  
							  <div class="field form-group">
                                 <label class="control-label">Loại hàng hóa</label>
                                 <div class="controls">
                                    <input class="form-control product-input" name="Category" type="text" placeholder="Nhập loại sản phẩm">
									
                                 </div>
                              </div>
							  
							  <div class="field form-group">
                                 <label class="control-label">Nhãn hiệu</label>
                                 <div class="controls">
                                    <input name="Brand" type="text" placeholder="Nhập nhãn hiệu" class="form-control product-input">
									
                                 </div>
                              </div>
							  
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 .col-lg-12 .col-xl-12 ">
                        <div class="h_Box">
                           <div class="col-md-3 .col-lg-3 .col-xl-3 col-sm-3 box-description">
                              <h3>Thiết lập ban đầu</h3>
                              <span class="colorNote">
                              Thiết lập định mức tồn kho ban đầu và giá trị hàng hóa của bạn.<br /> <br />
                              Thông tin này sau đó được sử dụng để đảm bảo giá cả, chi phí chính xác cũng như	báo cáo trong tương lai.
                              </span>
                           </div>
                           <div class="col-md-9 .col-lg-9 .col-xl-9 col-sm-9 marginContent">
                              <div class="tour-white row">
                                 <div class="col-md-6 col-xs-12">
                                    <div id="initialStockLevel" class="form-group field required">
                                       <label class="control-label">Tồn kho</label>
                                       <input required class="form-control number1 product-input textNumber" id="InitialStock" name="InitStock" type="number" min="0" value="0"> 
									   
                                    </div>
                                    <div id="initialCostPrice" class="form-group field required">
                                       <label class="control-label">Giá khởi tạo</label>
                                       <input required class="form-control number1 product-input textNumber intConvert valid" id="InitialCostPrice" name="InitialCostPrice" value="0" style="padding-right:20px">
                                       
                                    </div>
                                    
                                    <div class="field form-group">
                                       <label class="control-label">Mã SKU</label>
                                       
                                          <input class="form-control product-input" id="sku" name="Sku" placeholder="SP1243" type="text" value="" />

                                      
                                    </div>
                                 </div>
                                 <div class="col-md-6 col-xs-12">
                                    <div id="field-buyPrice" class="form-group field hide-buy-prices">
                                       <label class="control-label">Giá nhập</label>
                                       <input class="form-control product-input textNumber intConvert valid" id="import_price" value="">
                                  
                                    </div>
                                    <div id="field-wholesalePrice" class="form-group hide-buy-prices  field">
                                       <label class="control-label">Giá bán buôn</label>
                                       <input class="product-input number1 form-control textNumber intConvert valid" 
                                          id="whole-sale-price"  name="WholeSalePrice" type-number="number" value="">
                                       
                                    </div>
                                    <div id="field-retailPrice" class="form-group field hide-buy-prices">
                                       <label class="control-label">Giá bán lẻ</label>
                                       <input class="product-input number1 form-control textNumber intConvert valid" 
                                          id="retai-price" name="RetailPrice" value="">
                                       
                                    </div>
                                   
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                     <div class="fixed-btm-padding">
                        <div class="fixed-btm-actions products form-group" style="margin-bottom: 0;">
                           <div class="actions">
                              <div class="right">
                                 <button type="reset" class="buttonfooter button-cancel">
                                 Hủy bỏ
                                 </button>
                                 <button class="buttonfooter button_primary website-submit" type="submit">
                                 Thêm
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                  </form>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>