<div class="colLeft">
   <nav id="left-nav" class="website-menu">
      <div class="website-menu-top-header">
         <div class="menu-top-logo">
            <img src="<?=$config_url?>/assets/img/logo.png" class="website-menu-top-logo--short" />
         </div>
      </div>
      <div class="website-menu-inner-wrapper clearfix">
         <div class="website-menu-primary pull-left">
            <div class="website-menu-primary-inner">
               <ul class="website-menu-list website-menu-list--primary">
                  <li class="website-menu-list-item website-menu-list-item--primary website-menu-list-item--active">
                     <a href="<?=$config_url?>/index.php" class="website-menu-list-item-link website-menu-list-item-link--primary" >
                     <i class="website-menu-list-item-icon fa fa-home"></i>
                     <span class="website-menu-list-item-label">Tổng quan</span>
                     </a>
                  </li>
                  <li class="website-menu-list-item website-menu-list-item--primary ">
                     <a href="<?=$config_url?>/templates/orders/views.php" class="website-menu-list-item-link website-menu-list-item-link--primary" >
                     <i class="website-menu-list-item-icon fa fa-file-text"></i>
                     <span class="website-menu-list-item-label">Đơn hàng</span>
                     </a>
                  </li>
                  <li class="website-menu-list-item website-menu-list-item--primary ">
                     <a href="<?=$config_url?>/templates/customers/views.php" class="website-menu-list-item-link website-menu-list-item-link--primary" >
                     <i class="website-menu-list-item-icon fa fa-users"></i>
                     <span class="website-menu-list-item-label">Khách hàng</span>
                     </a>
                  </li>
                  <li class="website-menu-list-item website-menu-list-item--primary ">
                     <a href="<?=$config_url?>/templates/suppliers/views.php" class="website-menu-list-item-link website-menu-list-item-link--primary" >
                     <i class="website-menu-list-item-icon fa fa-cubes"></i>
                     <span class="website-menu-list-item-label">Hàng hóa</span>
                     </a>
                  </li>
                  <li class="website-menu-list-item website-menu-list-item--primary ">
                     <a href="<?=$config_url?>/templates/reports/views.php" class="website-menu-list-item-link website-menu-list-item-link--primary" >
                     <i class="website-menu-list-item-icon fa fa-bar-chart"></i>
                     <span class="website-menu-list-item-label">Báo cáo</span>
                     </a>
                  </li>
                  
                  <li class="website-menu-list-item website-menu-list-item--primary ">
                     <a href="" class="website-menu-list-item-link website-menu-list-item-link--primary">
                     <i class="website-menu-list-item-icon fa fa-cog"></i>
                     <span class="website-menu-list-item-label">Cài đặt</span>
                     </a>
                  </li>
               </ul>
            </div>
            <div class="website-menu-primary-inner--fix-bottom">
               <ul class="website-menu-list website-menu-list--primary">
                  <li class="website-menu-list-item website-menu-list-item--primary website-menu-list-item__user-info dropup">
                     <a href="javascript:void(0)" class="website-menu-list-item-link" data-toggle="dropdown">
                     <span class="website-menu-list-item-icon-wrapper text-center">
                     <i class="website-menu-list-item-icon fa fa-user website-menu-list-item-icon--user-info"></i>
                     </span>
                     </a>
                     <div class="dropdown-menu">
                        <div class="dropdown-menu-arrow"></div>
                        <ul class="dropdown-menu-list">
                           <li>
                              <a href="<?=$config_url?>/templates/setting/profiles.php" target="_blank">
                              <i class="fa fa-user"></i>
                              <span class="nav-label">Hồ sơ cá nhân</span>
                              </a>
                           </li>
                           <li>
                              <a href="<?=$config_url?>/login.php">
                              <i class="fa fa-sign-out"></i>
                              <span class="nav-label">Thoát</span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
         <div class="website-menu-secondary pull-left">
            <ul class="website-menu-list website-menu-list--secondary">
               <li class="website-menu-list-item website-menu-list-item--secondary ">
                  <a href="<?=$config_url?>/templates/products/views.php" class="website-menu-list-item-link website-menu-list-item-link--secondary ">Hàng hóa</a>
               </li>
               <li class="website-menu-list-item website-menu-list-item--secondary ">
                  <a href="" class="website-menu-list-item-link website-menu-list-item-link--secondary ">Nhập hàng</a>
               </li>
               <li class="website-menu-list-item website-menu-list-item--secondary ">
                  <a href="" class="website-menu-list-item-link website-menu-list-item-link--secondary ">Hóa đơn</a>
               </li>
               <li class="website-menu-list-item website-menu-list-item--secondary ">
                  <a href="" class="website-menu-list-item-link website-menu-list-item-link--secondary ">Nhận hàng</a>
               </li>
               <li class="website-menu-list-item website-menu-list-item--secondary ">
                  <a href="" class="website-menu-list-item-link website-menu-list-item-link--secondary ">Kiểm hàng</a>
               </li>
               <li class="website-menu-list-item website-menu-list-item--secondary ">
                  <a href="" class="website-menu-list-item-link website-menu-list-item-link--secondary ">Chuyển hàng</a>
               </li>
               <li class="website-menu-list-item website-menu-list-item--secondary ">
                  <a href="" class="website-menu-list-item-link website-menu-list-item-link--secondary ">Trả hàng</a>
               </li>
               <li class="website-menu-list-item website-menu-list-item--secondary ">
                  <a href="<?=$config_url?>/templates/suppliers/views.php" class="website-menu-list-item-link website-menu-list-item-link--secondary website-menu-list-item-link--secondary-active">Nhà cung cấp</a>
               </li>
            </ul>
         </div>
      </div>
   </nav>
</div>