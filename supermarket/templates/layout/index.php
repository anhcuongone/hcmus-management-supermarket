<div class="colRight">
	<div class="background-help">
	   <div class="">
		  <div class="help">
			 <h2>Chào mừng bạn đến với Website!</h2>
			 <p>Hãy hoàn thành 3 bước để bắt đầu bán hàng ngay bây giờ</p>
			 <div class="list-btn" style="height:180px">
				<a class="btn-help" href="<?=$config_url?>/templates/products/add.php" target="_blank">
				   <div class="btn-img-help">
					  <img alt="sapo" src="<?=$config_url?>/assets/img/hanghoa.png"/>
				   </div>
				   <p>Thêm hàng hóa</p>
				</a>
				<div class="ico-arrow">
				   <img alt="sapo" src="assets/img/arrow.png"/>
				</div>
				<a class="btn-help" href="<?=$config_url?>/templates/customers/add.php" target="_blank">
				   <div class="btn-img-help" style="padding-top: 25px;">
					  <img alt="sapo" src="<?=$config_url?>/assets/img/khachhang.png"/>
				   </div>
				   <p>Thêm khách hàng</p>
				</a>
				<div class="ico-arrow">
				   <img alt="sapo" src="<?=$config_url?>/assets/img/arrow.png"/>
				</div>
				<a class="btn-help" href="<?=$config_url?>/templates/orders/add.php" target="_blank">
				   <div class="btn-img-help"  style="padding-top: 30px;">
					  <img alt="sapo" src="assets/img/banhang.png"/>
				   </div>
				   <p>Bắt đầu bán hàng</p>
				</a>
			 </div>
			 <div class="col-sm-12" >
				<a href="" class="btn-skip btn"  bind-event-click="UpdateStatus()">
				Về trang tổng quan
				</a>
			 </div>
		  </div>
	   </div>
	</div>
</div>