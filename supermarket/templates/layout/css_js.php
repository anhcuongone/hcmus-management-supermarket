<!-- FONT AWESOME -->
<link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<?=$config_url?>/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=$config_url?>/assets/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?=$config_url?>/assets/css/nprogress.css">
<link rel="stylesheet" type="text/css" href="<?=$config_url?>/assets/css/style.css">
<script type="text/javascript" src="<?=$config_url?>/assets/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?=$config_url?>/assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=$config_url?>/assets/js/validator.min.js"></script>
<script type="text/javascript" src="<?=$config_url?>/assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?=$config_url?>/assets/js/nprogress.js"></script>
<script type="text/javascript" src="<?=$config_url?>/assets/js/icheck.min.js"></script>
<script type="text/javascript" src="<?=$config_url?>/assets/js/script.js"></script>

