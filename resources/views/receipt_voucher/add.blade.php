@extends('layouts.app')

@section('content')
    <div class="colRight">
            
            <div class="page-body">
               <div class="container-right">
                  <div class='page-heading page-heading-md page-heading-border-bottom'>
                     <h2 class='header__main'><span class='breadcrumb'><a href=''>Phiếu thu</a> /</span><span class='title'>Thêm mới phiếu thu</span></h2>
                     <div class="header-right">
                     </div>
                  </div>
                  <form action="{{ route('receipt_vouchers_store') }}" data-toggle="validator" role="form" enctype="multipart/form-data" id="createProduct" method="post">
                     {{ csrf_field() }}   
                <div class="row">
                <div class="col-md-12 .col-lg-12 .col-xl-12">
                    <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                        <h3>Thông tin phiếu</h3>
                        <span class="colorNote">                         
                            Phiếu thu khi nhận tiền thanh toán đơn hàng. công nợ, thanh toán hàng
                            trả lại nhà cung cấp.
                         
                        </span>
                    </div>
                    <div class="col-md-9 .col-lg-9 .col-xl-9 marginContent">
                        <div class="tour-white">

                            <div class="form-group field">
                                <label class="control-label">Đối tượng</label>
                                <div class="controls">
                                    <select Id="use-object-id" class="form-control product-input" datatype="dropdown" id="ObjectType" name="type_customer"><option selected="selected" value="customer">Khách hàng</option>
<option value="supplier">Nhà cung cấp</option>
<option value="employee">Nhân viên</option>
<option value="other">Đối tượng khác</option>
</select>
                                </div>
                                <span class="help-block with-errors">{{ $errors->first('type_customer') }}</span>
                            </div>
                       
                            <div class="field required form-group" id="customer">
                                <label class="control-label">Khách hàng</label>
                                <div class="controls">
                                    <input name="name_customer" type="text" data-error="Nhập tên khách hàng" required placeholder="Nhập tên khách hàng" class="form-control product-input search-customer" value="{{ old('name_customer') }}" />
                           <span class="help-block with-errors">{{ $errors->first('name_customer') }}</span>
                                </div>
                            </div>

                            <div class="field required form-group">
                                <label class="control-label">Loại phiếu thu</label>
                                    <div class="receipt_group" style="display: inline-block;">                                      
                                            <select id="group_id" bind="group_id" name="type_voucher" class="product-input form-control" datatype="dropdown" styledropdown="left: 160px;width: 200px">
                                                        <option value="">Thu nợ khách hàng</option>
                                                        <option value="">Nhượng bán, thanh lý tài sản</option>
                                                        <option value="">Cho thuê tài sản</option>
                                                        <option value="">Tiền bồi thường</option>
                                                        <option value="">Tiền thưởng</option>
                                                        <option value="">Thu nhập khác</option>
                                            </select>
                                        
                                    </div>
                               <span class="help-block with-errors">{{ $errors->first('type_voucher') }}</span>
                            </div>
                            
                            <div class="field form-group">
                                <label class="control-label">Ngày ghi nhận</label>
                                <div class="order-menu-screen__right_detailorder-issued order-item-right-text datepicker-input pull-left" style="border-color: #CBD5DE;width:200px;">
                                    
                                    <div class="form-datepicker-common">
                                        <input type="text" class="format-date form-control inline filter-input no-margin invoiced_on" id="transdate" value="{{ old('created_at') }}" name="IssuedOn">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <span class="help-block with-errors">{{ $errors->first('created_at') }}</span>
                            </div>


                            <div class="form-group field required" style="clear:both;padding-top:10px;">
                                <label class="control-label font-normal">Chi nhánh</label>
                                <div class="controls">
                                    <select class="form-control product-input" data-val="true" data-val-number="The field LocationId must be a number." datatype="dropdown" id="location_id" name="branch_id" styledropdown="width: 200px;left: 160px;"><option value="17465">Chi nhánh mặc định</option>
</select>
                                </div>
                                <span class="help-block with-errors">{{ $errors->first('branch_id') }}</span>
                            </div>

                            <div class="product-descriptions">
                                <div class="form-group field">
                                    <label class="control-label">Mô tả</label>
                                    <table class="product-description" style="border:none">
                                        <tr>
                                            <td style="border:none; padding-top:5px">
                                                <textarea class="form-control product-input" cols="20" id="Note" name="description" placeholder="" rows="3" type="text">
{{ old('description') }}</textarea>
<span class="help-block with-errors">{{ $errors->first('description') }}</span>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-12 .col-lg-12 .col-xl-12" style="margin-bottom:10%;">
                    <div class="h_Box">
                        <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                            <h3>Chi tiết phiếu thu</h3>
                            <span class="colorNote">
                                Lựa chọn phương thức thanh toán, tiền tệ và điền giá trị cho phiếu thu.
                            </span>
                        </div>
                        <div class="col-md-9 .col-lg-9 .col-xl-9 marginContent">
                            <div class="tour-white">
                                <div class="form-group field required ">
                                    <label class="control-label font-normal">Phương thức</label>
                                    <div class="controls">
                                        <select class="form-control product-input" data-val="true" data-val-number="The field PaymentMethodId must be a number." datatype="dropdown" id="payment_method_id" name="method" styledropdown="width: 200px;left: 160px;z-index:9999"><option value="60736">COD</option>
<option value="60735">Chuyển khoản</option>
<option selected="selected" value="60734">Tiền mặt</option>
</select>
                                    </div>
                                    <span class="help-block with-errors">{{ $errors->first('method') }}</span>
                                </div>
                                
                                

                                <div class="form-group field required">
                                    <label class="control-label font-normal">Giá trị</label>
                                    <div class="controls">
                                        <div class="input-group" style="width: 200px; height: 33px;">
                                            <input bind="amount" name="cash" class="form-control  intConvert form-control-input-popup valid" style="height: 33px;" type-number="number" value="{{ old('cash') }}">

                                            
                                            <span class="input-group-addon border-default" id="iso"> VND</span>
                                        </div>

                                    </div>
                                    <span class="help-block with-errors">{{ $errors->first('cash') }}</span>
                                </div>

                                <div class="form-group field">
                                    <label class="control-label font-normal"></label>
                                    <div class="controls">
                                        <div class="input-group" style="width:200px;height:33px">
                                            <input bind="counted" id="Counted" name="Counted" type="checkbox" value="true" /><input name="Counted" type="hidden" value="false" />  <span style="vertical-align: middle;"> Hạch toán kết quả kinh doanh</span>
                                        </div>

                                    </div>
                                </div>

                           

                            </div>
                        </div>
                    </div>

                </div>




            </div>
         
               <div class="fixed-btm-padding">
                        <div class="fixed-btm-actions products form-group" style="margin-bottom: 0;">
                           <div class="actions">
                              <div class="right">
                                 <button type="reset" class="buttonfooter button-cancel">
                                 Hủy bỏ
                                 </button>
                                 <button class="buttonfooter button_primary website-submit" type="submit">
                                 Thêm
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>

                     
                  </form>
               </div>
            </div>
         </div>
@endsection

