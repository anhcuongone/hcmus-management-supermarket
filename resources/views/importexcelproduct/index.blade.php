@extends('layouts.app')

@section('content')
    <div class="colRight">
        <div id="load-view-right-content" class="fix-height-right-content">
            <div id="product-detail-form">
				
                <form method="post" name='importproduct' action="{{ route('import_product') }}" enctype="multipart/form-data" id="fatherFormFilterAndSavedSearch">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="header-common-right">
                        <div class="page-heading page-heading-md page-heading-border-bottom">
                            <h2 class="header__main"><span class="title">Import Product</span></h2>
                            <div class="header-right">
                                <div>
                                    <div class="header-fr">
										&nbsp;&nbsp;
										<button type='submit' class='btn btn-default pull-right btn-a-active'>Cập nhật</button>
                                        
                                    </div>
									
                                    <div class="header-fr">
										<label for="file-upload" class="custom-file-upload">
										<i class="fa fa-file-excel-o" aria-hidden="true"></i> Chọn file
									</label>
									<input id="file-upload" name='file' type="file"/>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
						
						

                        <div class="filter-search-nav">
							@if(session('status'))
								<div class='alert alert-{!! session()->get("status") !!}'>
									{!! session()->get("messages") !!}
								</div>
							@endif
                            <ul class="filter-tab-list overflow-hidden-x ps-container ps-theme-default"
                                id="filter-tab-list" style="position: relative;">
                                <li class="filter-tab-item filter-tab-active" data-tab-index="1" style="">
                                    <a href="javascript:void(0)" class="filter-tab filter-tab-active">
                                        Danh sách phiếu nhập
                                    </a>
                                </li>

                                
                                <li class="hidden filter-tab-item main-filter" id="remove-filter-common">
                                    <a id="deletes" href="#" class="filter-tab"><i class="fa fa-times"
                                                                                   aria-hidden="true"></i> Xóa</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="boder-table">
						
                        <div id="table-height" class="bulk-action-context">
                            <table class="table defaul-table" id="parent-variant">
                                <thead>
                                <tr>
                                   
                                    <th class='col-xs-1'><?php echo App\Helpers::render_sort('id', 'ID'); ?></th>
									<th class='col-xs-3'><?php echo App\Helpers::render_sort('users_id', 'Người nhập'); ?></th>
                                    <th class='col-xs-3'><?php echo App\Helpers::render_sort('total', 'Tổng tiền'); ?></th>
									<th class='col-xs-2'>File Excel</th>
                                    <th class='col-xs-3 text-center'><?php echo App\Helpers::render_sort('created_at', 'Ngày ghi nhận'); ?></th>
                                    
                                </tr>
                                </thead>
                                <tbody class="tbody-scoler">
                                @foreach ($data as $key => $item)
                                    <tr>
                                        

                                        <td class="left-td col-xs-1"><span>{{ $item->id }}</span></td>
										<td class="left-td col-xs-3"><span>
										<?php
											$user = DB::table('users')->where('id', $item->users_id)->first();
											echo $user->name;
										?>
										</span></td>
                                        <td class="left-td col-xs-3"><span>{{ $item->total }}</span></td>
                                        <td class="left-td col-xs-2"><a href="{!! asset('../resources/upload/excels/'.$item->file) !!}"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Download</a></td>
                                        <td class="left-td col-xs-3 text-center"><span>{{ $item->created_at }}</span></td>
                                        
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="t-grid-pager-boder row">
                            <div class="t-pager t-reset">
                                <div class="col-xs-12">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
