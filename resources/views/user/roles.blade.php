@extends('layouts.app')

@section('content')
    <div class="colRight">
        <div id="load-view-right-content" class="fix-height-right-content">
            <div id="product-detail-form">
                <form method="get" action="" id="fatherFormFilterAndSavedSearch">
                    <div class="header-common-right">
                        <div class="page-heading page-heading-md page-heading-border-bottom">
                            <h2 class="header__main"><span class="title">Quản lý Role</span></h2>
                            <div class="header-right">
                                <div>
                                    @if(\App\Facade\Permission::check('ROLE_ADD'))
                                        <div class="header-fr">
                                            <a class="btn btn-default pull-right btn-a-active" href="#"
                                               style="margin-right:0px;">
                                            <span data-target='#editRoleModal' style='cursor: pointer;'
                                                  data-toggle='modal' class="addUser">
                                                <i class='fa fa-edit'></i>
                                                Thêm mới
                                            </span>
                                            </a>
                                        </div>
                                    @endif
                                </div>
                                <div class="search-layout-common header-fr header-input-search"
                                     style="margin-right: 8px; background: white; text-align: left;">
                                    <i class="fa fa-search"></i>
                                    <input type="text" class="form-control search-input ui-autocomplete-input"
                                           name="search" placeholder="Tìm kiếm Role ..." value="">
                                </div>
                            </div>
                        </div>
                        <div class="filter-search-nav">
                            <ul class="filter-tab-list overflow-hidden-x ps-container ps-theme-default"
                                id="filter-tab-list" style="position: relative;">
                                <li class="filter-tab-item filter-tab-active" data-tab-index="1" style="">
                                    <a href="javascript:void(0)" class="filter-tab filter-tab-active">
                                        Danh sách Roles
                                    </a>
                                </li>

                                @if(\App\Facade\Permission::check('ROLE_DELETE'))
                                    <li class="filter-tab-item main-filter" id="remove-filter-common">
                                        <a href="#" class="filter-tab" data-toggle="modal"
                                           data-target="#confirm-delete-all"><i class="fa fa-times" aria-hidden="true"></i>
                                            Xóa</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>

                    <div class="" style="padding: 5px" id="errors">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li> {{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="" style="padding: 5px" id="success">
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul>
                                    <li> {{ Session::get('success') }}</li>
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="boder-table">
                        <div id="table-height" class="bulk-action-context">
                            <table class="table defaul-table" id="parent-variant">
                                <thead>
                                <tr>
                                    <th class="center-align checkbox-default">
                                        <span><input type="checkbox" class='js-no-dirty all-bulk-action'></span>
                                    </th>
                                    <th class='col-xs-2'>Mã</th>
                                    <th class='col-xs-2'>Tên</th>
                                    <th class='col-xs-3'>Ngày tạo</th>
                                    <th class='col-xs-3'>Ngày sửa</th>
                                    <th class='col-xs-2 text-center fix-col-edit'>Action</th>
                                </tr>
                                </thead>
                                <tbody class="tbody-scoler">
                                @foreach ($roles as $role)
                                    <tr>
                                        <td class="left-td checkbox-default"><input type="checkbox" name="roles_id[]"
                                                                                    value="{{ $role->id }}"
                                                                                    class="bulk-action-item"></td>
                                        <td class="left-td col-xs-2"><span>{{ $role->id }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ $role->name }}</span></td>
                                        <td class="left-td col-xs-3"><span>{{ $role->created_at }}</span></td>
                                        <td class="left-td col-xs-3"><span>{{ $role->updated_at }}</span></td>
                                        <td class="left-td text-center col-xs-2 fix-col-edit">
                                            @if(\App\Facade\Permission::check('ROLE_EDIT'))
                                                <a href="#"><span
                                                            class="editRole"
                                                            data-target='#editRoleModal'
                                                            data-id="{{ $role->id }}"
                                                            data-name="{{ $role->name }}"
                                                            style='cursor: pointer;'
                                                            data-toggle='modal'>
                                                        <i class='fa fa-edit'></i> Edit</span></a>
                                            @endif

                                            @if(\App\Facade\Permission::check('ROLE_EDIT') || \App\Facade\Permission::check('ROLE_DELETE'))
                                                &nbsp;&nbsp;|&nbsp;
                                            @endif

                                            @if(\App\Facade\Permission::check('ROLE_DELETE'))
                                                <a data-target="#confirm-delete"
                                                   data-toggle="modal"
                                                   data-href="{{ route('delete_role', [$role->id]) }}">
                                                    <i class='fa fa-trash-o'></i>
                                                    Delete
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="t-grid-pager-boder row">
                            <div class="t-pager t-reset">
                                <div class="col-xs-4">
                                    <div class="t-status-text dataTables_info">{{  $roles->links() }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Xóa Role
                </div>
                <div class="modal-body">
                    Bạn thật sự muốn xóa role này ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-delete-all" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Xóa role
                </div>
                <div class="modal-body">
                    Bạn thật sự muốn xóa những role này ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="delete_all">Delete</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editRoleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <span class="ui-dialog-title" id="ui-dialog-title-1">
					Roles
					</span>
                    <a href="#" class="ui-dialog-titlebar-close ui-corner-all">
                        <span class="ui-icon ui-icon-closethick"></span>
                    </a>
                </div>
                <form role="form" name="editRoleForm" id="editRoleForm" method="post" action="{{ route('edit_role') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <div class='col-xs-12'>
                                <input type="hidden" id="role_id" name="role_id">

                                <div class="tab">
                                    <button class="tablinks" onclick="openTabs(event, 'Role')" id="defaultOpen">Role
                                    </button>
                                    <button class="tablinks" onclick="openTabs(event, 'Permission')">Permission</button>
                                </div>

                                <div id="Role" class="tabcontent">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" id="name" required class="form-control"
                                               placeholder="Name">
                                    </div>
                                </div>

                                <div id="Permission" class="tabcontent">
                                    <div class="boder-table">
                                        <div id="table-height" class="bulk-action-context pre-scrollable"
                                             style="max-height: 400px;">
                                            <table class="table defaul-table" id="parent-variant">
                                                <thead>
                                                <tr>
                                                    <th class="center-align">
                                                        <span><input type="checkbox"
                                                                     class='js-no-dirty all-bulk-action'></span>
                                                    </th>
                                                    <th>Tên</th>
                                                </tr>
                                                </thead>
                                                <tbody class="tbody-scoler">
                                                @foreach ($permissions as $permission)
                                                    <tr>
                                                        <td class="left-td"><input type="checkbox"
                                                                                   name="permission_id[]"
                                                                                   value="{{ $permission->id }}"
                                                                                   class="bulk-action-item permissionId">
                                                        </td>
                                                        <td class="left-td"><span>{{ $permission->name }}</span></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="modal-footer">
                        <div class="action-btn clearfix">
                            <button type="submit" class="btn btn-default">Save</button>
                            <button type="button" class="btn btn-closes" id="close-popup" data-dismiss="modal">Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <script type="application/javascript">
        document.getElementById("defaultOpen").click();

        $(document).ready(function () {
            $(document).on("click", ".editRole", function () {
                var edit_id = $(this).data('id');
                var edit_name = $(this).data('name');

                $("#role_id").val(edit_id);
                $("#name").val(edit_name);

                $("input[type=checkbox]").each(function () {
                    $(this).prop('checked', false);
                });

                $.ajax({
                    url: '/getPermission',
                    method: "POST",
                    data: {
                        id: edit_id,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (data) {
                        $('input[type=checkbox]').each(function () {
                            var value = $(this).val();
                            var permissions = data.permissions;

                            for (var i in permissions) {
                                if (permissions[i].permission_id === parseInt(value)) {
                                    $(this).prop('checked', true);
                                    break;
                                }
                            }
                        });
                    }
                });
            });


            $(document).on("click", ".addUser", function () {
                $("input[type=checkbox]").each(function () {
                    $(this).prop('checked', false);
                });
            });

            $('#confirm-delete').on('show.bs.modal', function (e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });

            $('#delete_all').click(function () {
                var ids = [];

                $("input[name='roles_id[]']:checked").each(function () {
                    ids.push($(this).val());
                });

                $.ajax({
                    url: "/roles/delete_all",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        ids: ids
                    },
                    type: "POST",
                    success: function (response) {
                        if (response.success) {
                            var string = '<div class="alert alert-success">' +
                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                                '<ul><li>' + response.error + '</li></ul></div>';
                            $('#errors').append(string);

                            setTimeout(function () {
                                location.reload();
                            }, 2000);

                        } else {
                            var string = '<div class="alert alert-danger">' +
                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                                '<ul><li>' + response.error + '</li></ul></div>';
                            $('#errors').append(string);

                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }
                    }
                });

                $("[data-dismiss=modal]").trigger({type: "click"});
            });
        });


    </script>
@endsection

