@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @if (isset($success))
                @if ($success)
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> Save your profile successfully
                    </div>
                @else
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Danger!</strong> Save your profile failed
                    </div>
                @endif
            @endif

            <h1>Your Profile</h1>
            <form method="post">
                <input type="hidden" name="id" value="{{$user['id']}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{$user['name']}}">
                </div>
                <div class="form-group ">
                    <label for="email">Email address:</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{$user['email']}}">
                </div>
                <div class="form-group ">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pwd" name="pwd">
                </div>

                <div class="form-group">
                    <label for="pwd">Re-Password:</label>
                    <input type="password" class="form-control" id="pwd2">
                </div>

                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
@endsection
