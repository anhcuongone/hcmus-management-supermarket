@extends('layouts.app')

@section('content')
    <div class="colRight">
        <div id="load-view-right-content" class="fix-height-right-content">
            <div id="product-detail-form">
                <form method="get" action="" id="fatherFormFilterAndSavedSearch">
                    <div class="header-common-right">
                        <div class="page-heading page-heading-md page-heading-border-bottom">
                            <h2 class="header__main"><span class="title">Quản lý tài khoản</span></h2>
                            <div class="header-right">
                                <div>
                                    @if(\App\Facade\Permission::check('USER_ADD'))
                                        <div class="header-fr">
                                            <a class="btn btn-default pull-right btn-a-active" href="#"
                                               style="margin-right:0px;">
                                            <span data-target='#editUserModal' style='cursor: pointer;'
                                                  data-toggle='modal' class="addUser">
                                                <i class='fa fa-edit'></i>
                                                Thêm mới
                                            </span>
                                            </a>
                                        </div>
                                    @endif
                                </div>
                                <div class="search-layout-common header-fr header-input-search"
                                     style="margin-right: 8px; background: white; text-align: left;">
                                    <i class="fa fa-search"></i>
                                    <input type="text" class="form-control search-input ui-autocomplete-input"
                                           name="search" placeholder="Tìm kiếm tài khoản ..." value="">
                                </div>
                            </div>
                        </div>
                        <div class="filter-search-nav">
                            <ul class="filter-tab-list overflow-hidden-x ps-container ps-theme-default"
                                id="filter-tab-list" style="position: relative;">
                                <li class="filter-tab-item filter-tab-active" data-tab-index="1" style="">
                                    <a href="javascript:void(0)" class="filter-tab filter-tab-active">
                                        Danh sách tài khoản
                                    </a>
                                </li>

                                @if(\App\Facade\Permission::check('USER_DELETE'))
                                    <li class="filter-tab-item main-filter" id="remove-filter-common">
                                        <a href="#" class="filter-tab" data-toggle="modal"
                                           data-target="#confirm-delete-all"><i class="fa fa-times" aria-hidden="true"></i>
                                            Xóa</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>

                    <div class="" style="padding: 5px" id="errors">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li> {{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="" style="padding: 5px" id="success">
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul>
                                    <li> {{ Session::get('success') }}</li>
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="boder-table">
                        <div id="table-height" class="bulk-action-context">
                            <table class="table defaul-table" id="parent-variant">
                                <thead>
                                <tr>
                                    <th class="checkbox-default center-align">
                                        <span><input type="checkbox" class='js-no-dirty all-bulk-action'></span>
                                    </th>
                                    <th class='col-xs-2'>Mã</th>
                                    <th class='col-xs-2'>Tên</th>
                                    <th class='col-xs-2'>Email</th>
                                    <th class='col-xs-2'>Ngày tạo</th>
                                    <th class='col-xs-2'>Ngày sửa</th>
                                    <th class='col-xs-2 fix-col-edit text-center'>Action</th>
                                </tr>
                                </thead>
                                <tbody class="tbody-scoler">
                                @foreach ($users as $user)
                                    <tr>
                                        <td class="left-td checkbox-default">
                                            <input type="checkbox" name="users_id[]" value="{{ $user->id }}" class="bulk-action-item"></td>
                                        <td class="left-td col-xs-2"><span>{{ $user->id }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ $user->name }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ $user->email }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ $user->created_at }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ $user->updated_at }}</span></td>
                                        <td class="left-td fix-col-edit col-xs-2">

                                            @if(\App\Facade\Permission::check('USER_UPDATE'))
                                                <a href="#"><span
                                                            class="editUser"
                                                            data-target='#editUserModal'
                                                            data-id="{{ $user->id }}"
                                                            data-name="{{ $user->name }}"
                                                            data-email="{{ $user->email }}"
                                                            style='cursor: pointer;'
                                                            data-toggle='modal'>
                                                        <i class='fa fa-edit'></i> Edit</span></a>
                                            @endif

                                            @if(\App\Facade\Permission::check('USER_UPDATE') && \App\Facade\Permission::check('USER_DELETE'))
                                                &nbsp;&nbsp;|&nbsp;
                                            @endif

                                            @if(\App\Facade\Permission::check('USER_DELETE'))
                                                    <a data-target="#confirm-delete"
                                                       data-toggle="modal"
                                                       data-href="{{ route('delete_user', [$user->id]) }}">
                                                        <i class='fa fa-trash-o'></i>
                                                        Delete
                                                    </a>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="t-grid-pager-boder row">
                            <div class="t-pager t-reset">
                                <div class="col-xs-4">
                                    {{--<div class="t-status-text dataTables_info">Hiển thị kết quả từ 1 - 1 trên tổng 1 </div>--}}
                                    {{ $users->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Xóa user
                </div>
                <div class="modal-body">
                    Bạn thật sự muốn xóa user này ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-delete-all" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Xóa users
                </div>
                <div class="modal-body">
                    Bạn thật sự muốn xóa những users này ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="delete_all">Delete</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-user">
                <div class="modal-header">
                    <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <span class="ui-dialog-title" id="ui-dialog-title-1">
					User Info
					</span>
                    <a href="#" class="ui-dialog-titlebar-close ui-corner-all">
                        <span class="ui-icon ui-icon-closethick"></span>
                    </a>
                </div>
                <form role="form" name="editUserForm" id="editUserForm" method="post" action="{{ route('edit_user') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <div class='col-xs-12'>
                                <input type="hidden" id="user_id" name="user_id">
                                <div class="field required form-group">
                                    <label class="control-label text-right ">Name</label>
                                    <div class="controls">
                                        <input type="text" name="name" id="name" class="form-control product-input"
                                               placeholder="Name">
                                    </div>
                                </div>

                                <div class="field required form-group">
                                    <label class="control-label text-right ">Email</label>
                                    <div class="controls">
                                        <input type="text" name="email" id="email" class="form-control product-input"
                                               placeholder="E-mail">
                                    </div>
                                </div>

                                <div class="field required form-group">
                                    <label class="control-label text-right ">Password</label>
                                    <div class="controls">
                                        <input type="password" name="password" id="password"
                                               class="form-control product-input" placeholder="Password">
                                    </div>
                                </div>


                                <div class="field required form-group">
                                    <label class="control-label text-right ">Re-Password</label>
                                    <div class="controls">
                                        <input type="password" name="repassword" id="repassword"
                                               class="form-control product-input" placeholder="Re Password">
                                    </div>
                                </div>


                                <div class="field required form-group">
                                    <label class="control-label text-right" for="roles">Roles</label>
                                    <div class="controls">
                                        <select multiple id="roles" class="form-control product-input" name="roles[]">
                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="modal-footer">
                        <div class="action-btn clearfix">
                            <button type="submit" class="btn btn-default">Save</button>
                            <button type="button" class="btn btn-closes" id="close-popup" data-dismiss="modal">Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <script type="application/javascript">
        $(document).on("click", ".editUser", function () {
            var edit_id = $(this).data('id');
            var edit_name = $(this).data('name');
            var edit_email = $(this).data('email');

            $("#user_id").val(edit_id);
            $("#name").val(edit_name);
            $("#email").val(edit_email);

            $.ajax({
                method: "POST",
                url: "{{ route('getUserRoles') }}",
                data: {
                    id: edit_id
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                },
                success: function (result, status, xhr) {
                    if (result) {
                        var options = document.getElementById("roles").options;
                        var roles = result.roles;


                        for (var i in roles) {
                            for (var j in options) {
                                if (options[j].value && roles[i].role_id == options[j].value) {
                                    $(options[j]).attr("selected", "selected");
                                    console.log(options[j]);
                                }
                            }
                        }
                    }
                },
                error: function (xhr, status, error) {

                }
            });
        });

        $(document).on("click", ".addUser", function () {

            $("#user_id").val('');
            $("#name").val('');
            $("#email").val('');

        });

        $(document).ready(function () {
            $('#confirm-delete').on('show.bs.modal', function (e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });

            $('#delete_all').click(function () {
                var ids = [];

                $("input[name='users_id[]']:checked").each(function () {
                    ids.push($(this).val());
                });

                $.ajax({
                    url: "/users/delete_all",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        ids: ids
                    },
                    type: "POST",
                    success: function (response) {
                        if (response.success) {
                            var string = '<div class="alert alert-success">' +
                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                                '<ul><li>' + response.error + '</li></ul></div>';
                            $('#errors').append(string);

                            setTimeout(function () {
                                location.reload();
                            }, 2000);

                        } else {
                            var string = '<div class="alert alert-danger">' +
                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                                '<ul><li>' + response.error + '</li></ul></div>';
                            $('#errors').append(string);

                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }
                    }
                });

                $("[data-dismiss=modal]").trigger({type: "click"});
            });
        });
    </script>
@endsection

