@extends('layouts.app')

@section('content') 
    <div class="colRight">
            <div class="page-body website-menu-expand" id="load-view-right-content">
               <div context="editOrder">
                  <form action="{{ route('importproduct_store') }}" enctype="multipart/form-data" id="formOrder" method="post">
                  {{ csrf_field() }}
                     <div class='page-heading page-heading-md'>
                        <h2 class='header__main'><span class='breadcrumb'><a href=''>Đơn hàng</a> /</span><span class='title'>Tạo đơn hàng nhập</span></h2>
                        <div class="header-right">
                           <a class="btn btn-default pull-right btn-a-active" target="_blank" href="">Tạo đơn hàng</a>
                        </div>
                     </div>
                     
                     
                     <div class="order-menu-screen">
                        <div class="menu-left-new">
                           <div class="order-menu-screen__left order-menu-screen__left-order " id="order-menu-screen__left" style="position: relative;">
                              
                              <div class="order-menu-screen__left-scroll w100">
                                 <div style="padding: 0 7.5px" class="clearfix">
									<div class="col-sm-6 no-padding">
										<button data-target='#addProductModal' data-toggle='modal'  style="width: auto; margin-left: 0px; height: 28px;" class="buttonfooter button_primary website-submit disabled" type="button">
											Thêm sản phẩm
										</button>
										<div style="width: calc(100% - 115px); float: left;" class="form-group col-sm-12 search-layout-common no-padding no-margin" style="width: 100%">
                                          <i class="fa fa-search"></i>
                                          <input id="tim_sp" style="height: 29px; font-size: 12px;" id="search-input-prd" type="text" class="form-control search-input ui-autocomplete-input" placeholder="Tìm kiếm sản phẩm">
                                       </div> 
									</div>

									<div class="col-sm-3 no-padding">
                                       <div class="form-group col-sm-12 no-padding no-margin" style="width: 100%">
                                          <input id="OrderName" name='name' style="height: 29px; font-size: 12px;"  type="text" class="form-control" placeholder="Tên đơn hàng">
                                       </div>
                                    </div>
									
									<div class="col-sm-3 no-padding">
                                       <div class="form-group col-sm-12 no-padding no-margin" style="width: 100%">
                                          <input id="ProviderName" name='provider' style="height: 29px; font-size: 12px;"  type="text" class="form-control" placeholder="Tên nhà cung cấp">
                                       </div>
                                    </div>
									
									<div class="col-sm-6 no-padding">
									   <div style='    padding-left: 5px;float: left; width: 110px'>Tình trạng: </div>
                                       <div style="width: calc(100% - 115px);" class="form-group col-sm-12 no-padding no-margin" style="width: 100%">
                                          <select name='status'  class='form-control'>
											<option value="0">Chưa thanh toán</option>
											<option value="1">Đã thanh toán</option>
										  </select>
                                       </div>
                                    </div>
									
									<div class="col-sm-3 no-padding">
                                       <div class="form-group col-sm-12 no-padding no-margin" style="width: 100%">
                                          <input id="Discount" name='discount_order' style="height: 29px; font-size: 12px;"  type="text" class="form-control" placeholder="Giảm giá">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="order-menu-screen__left-variant clearfix">
                                    <div class="order-menu-screen__left-variant-scroll w100 table-order-new">
                                       <table class="table defaul-table popup-house table-order-edit" id="table-order">
                                          <thead>
                                             <tr>
                                                <th style="width: 12%" class="left-th">Mã SKU</th>
                                                <th style="width: 28%" class="left-th">Tên hàng hóa</th>
                                                <th style="width: 15%" class="right-th text-center">Số lượng</th>
                                                <th style="width: 12%" class="right-th hidden">Đơn giá</th>
												<th style="width: 10%" class="right-th hidden">Thuế (%)</th>
                                                <th style="width: 20%" class="right-th">Giảm giá</th>
                                                <th style="width: 25%" class="right-th">Thành tiền</th>
                                                <th style="width: 2%"></th>
                                             </tr>
                                          </thead>
                                          <tbody id="line_item_rows" style="display: inline-table; width: 100%">
                                          </tbody>
                                       </table>
                                       <div id="no-prd">
                                       </div>
                                    </div>
                                 </div>
                               <div class="order-menu-screen__left-totalamount" style="bottom: 46px;display:block">
                                 <div class='clearfix'>
                           <div class='col-md-6 col-xs-12 pull-right'>
                                 <div class="order-menu-screen__left-totalamount_total order-payment-total__head pull-left">
                                    <ul>
                                      <li>
                                         <span style="color: #1cb467;" class="order-payment-total__head_text discount-order">
                                         Giảm giá <i class="fa fa-caret-down"></i>
                                         </span>
                                         <span id="total_discount" class="order-payment-total__head_value">0đ</span>
                                      </li>
                                      <li>
                                        <span class="order-payment-total__head_text">Tổng tiền</span>
                                        <span id="order_total" class="order-payment-total__head_value">0đ</span>
                                      </li>
                                        <input id="_total" type="hidden" name="total" value="">
                                        <input id="_list_product" type="hidden" name="list_product" value="">
                                        <input id="_id_customer" type="hidden" name="id_customer" value="">
                                        <input id="_total_discount" type="hidden" name="discount" value="">
                                    </ul>
                                 
                                 </div>
                               </div>
                               
                           
                           
                           <div class="col-md-6 col-xs-12">
                              <div class="fixed-noteTag">
                                  <textarea class="form-control" cols="20" id="Note" name="description" placeholder="Ghi chú" rows="2" style="margin-bottom:7px;padding:7px 10px">
                                  </textarea>
                                  
                                </div>
                           </div>
                           </div>
                         </div>
                              </div>
                              
                              <div class="order-menu-screen__left-payment" style="background: #fff;">
                                 <div class="actions">
                                    <div class="left" style="width: 38%; display: inline-block">
                                    </div>
                                    <div class="right">
                                       <a href="{{ route('importproduct') }}" class="buttonfooter button-cancel" style="text-align: center;">
                                       Hủy
                                       </a>
                                       <button class="buttonfooter button_primary website-submit" type="submit">
                                 Đặt hàng
                                 </button>
                                    </div>
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                       
                        <div class="order-menu-screen__right pull-left menu-right-new" id="order-menu-screen__right">
                           <div class="order-menu-screen__right-scoll pull-left w100 ">
                              <div class="order-menu-screen__right-scoll_invoice pull-left w100">
                                 <div class="order-menu-screen__right-scoll_invoice-header pull-left w100">
                                    <div class="order-menu-screen__right-scoll_invoice-header-left pull-left" style="width: 100%;">
                                       <i class="fa fa-file-text"></i> Đơn hàng<span class='pull-right'></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                    
                  </form>
               </div>
            </div>
         </div>
		 
		 
		 
		 
		 <div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-user">
                <div class="modal-header">
					<button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">&times;</button>
					<span class="ui-dialog-title" id="ui-dialog-title-1">
					Thêm mới sản phẩm
					</span>
					<a href="#" class="ui-dialog-titlebar-close ui-corner-all">
					<span class="ui-icon ui-icon-closethick"></span>
					</a>
				 </div>
                <form action="{{ route('importproduct_productaddquick') }}" data-toggle="validator" role="form" enctype="multipart/form-data" id="createProduct" method="post">
                    {{ csrf_field() }}
					<input type='hidden' name='current_id' value="0" />
                    <div class="modal-body">
                        <div class="tour-white">	
								
                                <div class="field required form-group">
                                    <label class="control-label">Tên hàng</label>
                                    <div class="controls">
                                        <input data-error="Vui lòng nhập hàng." class="form-control product-input"
                                               id="Name" name="name" placeholder="Nhập tên hàng" type="text"
                                               value="{{ old('name') }}"/>
                                        <span class="help-block with-errors">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
								
								<div class="field required form-group" >
                                    <label class="control-label">Barcode</label>
                                    <div class="controls">
                                        <input data-error="Vui lòng nhập nhà barcode." type="text" required
                                               id="barcode" name="barcode"
                                               class="form-control product-input"  placeholder="Nhập barcode cung cấp" />
                                        <span class="help-block with-errors">{{ $errors->first('barcode') }}</span>
                                    </div>
                                </div>
								
                                <div class="field required form-group">
                                    <label class="control-label">Giá tiền</label>
                                    <div class="controls">
                                        <input data-error="Vui lòng nhập giá tiền." required
                                               class="form-control product-input" id="cost" name="cost"
                                               placeholder="Nhập giá tiền" type="text" value="{{ old('cost') }}"/>
                                        <span class="help-block with-errors">{{ $errors->first('cost') }}</span>
                                    </div>
                                </div>

                                <div class="field required form-group">
                                    <label class="control-label">Số lượng</label>
                                    <div class="controls">
                                        <input data-error="Vui lòng nhập số lượng." readonly
                                               class="form-control product-input" id="quantity" value="0" name="quantity"
                                               placeholder="Nhập số lượng" type="number" value="{{ old('quantity') }}"/>
                                        <span class="help-block with-errors">{{ $errors->first('quantity') }}</span>
                                    </div>
                                </div>

                                <div class="field required form-group" >
                                    <label class="control-label">Nhà cung cấp</label>
                                    <div class="controls" style="position:relative;  display: inline-block;">
									
										<select id="roles" class="form-control product-input"  name="suppliers_id">
											@foreach($supplier as $supplier)
												<option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
											@endforeach
										</select>
                                        <!--<input data-error="Vui lòng nhập nhà cung cấp." type="text" required
                                               id="provider"
                                               class="form-control product-input"  placeholder="Nhập nhà cung cấp" />-->
                                        <span class="help-block with-errors">{{ $errors->first('provider') }}</span>
                                        <!--<input type="hidden" id="provider-id" name="suppliers_id" value="{{ old('suppliers_id') }}">-->
                                    </div>
                                </div>
								
                                <div class="field required form-group" >
                                    <label class="control-label">Khuyến mãi</label>
                                    <div class="controls">
                                        <input data-error="Vui lòng nhập khuyến mãi" type="text"
                                               id="promotions"
                                               class="form-control product-input"  placeholder="Nhập khuyến mãi" />
                                        <span class="help-block with-errors">{{ $errors->first('promotions') }}</span>
                                        <input type="hidden" id="promotions-id" name="promotions_id" value="{{ old('promotions_id') }}">
                                    </div>
                                </div>


                                

                                <div class="field required form-group">
                                    <label class="control-label">Ngày hết hạn</label>
                                    <div class="controls">
                                        <div class="input-group date " data-provide="datepicker">
                                            <input data-error="Vui lòng nhập ngày hết hạn." type="text"
                                                   name="expired_date" id="expired_date" class="form-control">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                        <span class="help-block with-errors">{{ $errors->first('expired_date') }}</span>
                                    </div>
                                </div>

                                <div class="field required form-group">
                                    <label class="control-label">Mô tả</label>
                                    <div class="controls">
                                    <textarea
                                            class="form-control product-input"
                                            rows="4"
                                            cols="4"
                                            name="description"
                                            id="description"></textarea>
                                        <span class="help-block with-errors">{{ $errors->first('description') }}</span>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <br>
					
					<div class="modal-footer">
						<div class="action-btn clearfix">
                        <button type="submit" class="btn btn-default">Save</button>
                        <button type="button" class="btn btn-closes" id="close-popup" data-dismiss="modal">Cancel</button>
						</div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
	
	
	
	
	
	
@endsection
@section('js_script')
<script type="text/javascript">
   var options = {

      url: function(phrase) {
        return "{{ route('customer_searchByKeyWord') }}";
      },

      getValue: function(element) {
        return element.name;
      },

      list: {
        maxNumberOfElements: 14,
        onClickEvent: function() {
           $("#delivery_address").val($("#ma_kh").getSelectedItemData().address);
           $("#phone_work").val($("#ma_kh").getSelectedItemData().phone);
           $("#_id_customer").val($("#ma_kh").getSelectedItemData().id);
        } 
      },

      ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
          dataType: "json",
        }
      },

      preparePostData: function(data) {
        data.keyword = $("#ma_kh").val();
        data._token = "{{ csrf_token() }}";
        return data;
      },

      requestDelay: 400
    };

    $("#ma_kh").easyAutocomplete(options);

    var options = {

      url: function(phrase) {
        return "{{ route('product_searchByKeyword') }}";
      },

      getValue: function(element) {
        return element.name;
      },

      list: {
        maxNumberOfElements: 14,
        onClickEvent: function() {
          var sl = 1;
          var dongia = $("#tim_sp").getSelectedItemData().cost;
          var vat = 10;
          var discount = $("#tim_sp").getSelectedItemData().discount;
          discount = (discount === null) ? 0 : discount;
           $("#line_item_rows").append('<tr>'
              +'<td width="12%" class="id_product">'+$("#tim_sp").getSelectedItemData().id+'</td>'
              +'<td width="28%" class="name_product">'+$("#tim_sp").getSelectedItemData().name+'</td>'
              +'<td width="15%" ><input class="product_quantity form-control" value="'+sl+'"/></td>'
              +'<td width="20%"><span class="product_dongia">' + dongia + '</span></td>'
              +'<td class="hidden"><input class="product_vat" value="'+vat+'"/></td>'
              +'<td width="20%" class="hidden"><span class="product_discount">' + discount +'</span></td>'
              +'<td width="25%"><span class="product_total">'+parseFloat(sl*dongia + (sl*(dongia*(vat/100))) - discount) + '</span></td>'
              +'<td class="product_remove"><i class="fa fa-remove"></i></td>'
              +'</tr>');
           calc_order_total();
        } 
      },

      ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
          dataType: "json",
        }
      },

      preparePostData: function(data) {
        data.keyword = $("#tim_sp").val();
        data._token = "{{ csrf_token() }}";
        return data;
      },

      requestDelay: 400
    };

    $("#tim_sp").easyAutocomplete(options);

    $(document).on('keyup', '.product_quantity', function(e) {
      var sl = $(this).val();
      var dongia = $(this).parent().parent().find('.product_dongia').html();
      var discount = $(this).parent().parent().find('.product_discount').html();
      var vat = $(this).parent().parent().find('.product_vat').val();
     $(this).parent().parent().find('.product_total').html(parseFloat(sl*dongia + (sl*(dongia*(vat/100))) - discount));
     calc_order_total();
    });

    $(document).on('keyup', '.product_vat', function(e) {
      var vat = $(this).val();
      var dongia = $(this).parent().parent().find('.product_dongia').html();
      var sl = $(this).parent().parent().find('.product_quantity').val();
      var discount = $(this).parent().parent().find('.product_discount').html();
     $(this).parent().parent().find('.product_total').html(parseFloat(sl*dongia + (sl*(dongia*(vat/100))) - discount));
     calc_order_total();
    });

    $(document).on('click', '.product_remove', function(e) {
      $(this).parent().remove();
     calc_order_total();
    });

    function calc_order_total()
    {
      var total = 0;
      var total_discount = 0;
      
      $('#line_item_rows tr').each(function(){
        total += parseFloat($(this).find('.product_total').html());
        total_discount += parseFloat($(this).find('.product_discount').html());
      });

      $('#order_total').html(total);
      $('#_total').val(total);

      $('#total_discount').html(total_discount);
      $('#_total_discount').val(total_discount);

      var list_product = [];
      $("#line_item_rows tr").each(function(){

        var item = {
          'products_id' : $(this).find('.id_product').html(),
          'name_product' : $(this).find('.name_product').html(),
          'quantity_product' : $(this).find('.product_quantity').val(),
          'cost_product' : $(this).find('.product_dongia').html(),
          'vat_product' : $(this).find('.product_vat').val(),
          'discount_product' : $(this).find('.product_discount').html(),
          'total' : $(this).find('.product_total').html(),
        }
        list_product.push(item);
      });

      $('#_list_product').val(JSON.stringify(list_product));
    }
</script>
@endsection
