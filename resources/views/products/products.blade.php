@extends('layouts.app')

@section('content')
    <div class="colRight">
        <div id="load-view-right-content" class="fix-height-right-content">
            <div id="product-detail-form">
                <form method="get" action="" id="fatherFormFilterAndSavedSearch">
                    <div class="header-common-right">
                        <div class="page-heading page-heading-md page-heading-border-bottom">
                            <h2 class="header__main"><span class="title">Quản lý hàng hóa</span></h2>
                            <div class="header-right">
                                <div>
                                    @if(\App\Facade\Permission::check('PRODUCT_ADD'))
                                        <div class="header-fr">
                                            <a class="btn btn-default pull-right btn-a-active"
                                               href="{{  route('product_add') }}" style="margin-right:0px;">
                                        <span>
                                            <i class='fa fa-edit'></i>
                                            Thêm mới
                                        </span>
                                            </a>
                                        </div>
                                    @endif
                                </div>
                                <div class="search-layout-common header-fr header-input-search"
                                     style="margin-right: 8px; background: white; text-align: left;">
                                    <i class="fa fa-search"></i>
                                    <input type="text" class="form-control search-input ui-autocomplete-input"
                                           name="search" placeholder="Tìm kiếm ..." value="">
                                </div>
                            </div>
                        </div>
                        <div class="filter-search-nav">
                            <ul class="filter-tab-list overflow-hidden-x ps-container ps-theme-default"
                                id="filter-tab-list" style="position: relative;">
                                <li class="filter-tab-item filter-tab-active" data-tab-index="1" style="">
                                    <a href="javascript:void(0)" class="filter-tab filter-tab-active">
                                        Danh sách hàng hóa
                                    </a>
                                </li>
                                @if(\App\Facade\Permission::check('PRODUCT_DELETE'))
                                    <li class="filter-tab-item main-filter" id="remove-filter-common">
                                        <a href="#" class="filter-tab" data-toggle="modal"
                                           data-target="#confirm-delete-all"><i class="fa fa-times"
                                                                                aria-hidden="true"></i>
                                            Xóa</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>

                    <div class="" style="padding: 5px" id="errors">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li> {{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="" style="padding: 5px" id="success">
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul>
                                    <li> {{ Session::get('success') }}</li>
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="boder-table">
                        <div id="table-height" class="bulk-action-context">
                            <table class="table defaul-table" id="parent-variant">
                                <thead>
                                <tr>
                                    <th class='checkbox-default'>
                                        <span><input type="checkbox" class='js-no-dirty all-bulk-action'></span>
                                    </th>
                                    <th class='col-xs-1'>Tên</th>
                                    <th class='col-xs-2'>Mô tả</th>
                                    <th class='col-xs-1'>Số lượng</th>
                                    <th class='col-xs-2'>Giá tiền</th>
                                    <th class='col-xs-2'>Nhà cung cấp</th>
                                    <th class='col-xs-2'>Ngày hết hạn</th>
                                    <th class='col-xs-2 text-center fix-col-edit'>Action</th>
                                </tr>
                                </thead>
                                <tbody class="tbody-scoler">
                                @foreach ($products as $product)
                                    <tr>
                                        <td class="left-td checkbox-default"><input type="checkbox" name="product_id[]"
                                                                                    value="{{ $product->id }}"
                                                                                    class="bulk-action-item"></td>
                                        <td class="left-td col-xs-1"><span>{{ $product->name }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ $product->description }}</span></td>
                                        <td class="left-td col-xs-1"><span>{{ $product->quantity }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ number_format($product->cost) }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ $product->supplier_name }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ $product->expired_date }}</span></td>
                                        <td class="col-xs-2 fix-col-edit text-center">
                                            @if(\App\Facade\Permission::check('PRODUCT_UPDATE'))
                                                <a href="{{ route('product_edit', [$product->id]) }}"><i
                                                            class='fa fa-edit'></i> Edit</a>
                                            @endif

                                            @if(\App\Facade\Permission::check('PRODUCT_UPDATE') && \App\Facade\Permission::check('PRODUCT_DELETE'))
                                                |
                                            @endif

                                            @if(\App\Facade\Permission::check('PRODUCT_DELETE'))
                                                <a data-href="{{ route('product_delete', [$product->id]) }}"
                                                   data-toggle="modal" data-target="#confirm-delete"><i
                                                            class='fa fa-trash-o'></i> Delete</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="t-grid-pager-boder row">
                            <div class="t-pager t-reset">
                                <div class="col-xs-4">
                                    {{--<div class="t-status-text dataTables_info">Hiển thị kết quả từ 1 - 1 trên tổng 1 </div>--}}
                                    <div class="t-status-text dataTables_info">{{ $products->links() }} </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Xóa hàng hóa
                </div>
                <div class="modal-body">
                    Bạn thật sự muốn xóa hàng hóa này ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-delete-all" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Xóa hàng hóa
                </div>
                <div class="modal-body">
                    Bạn thật sự muốn xóa những hàng hóa này ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="product_delete_all">Delete</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#confirm-delete').on('show.bs.modal', function (e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });

            $('#product_delete_all').click(function () {
                var ids = [];

                $("input[name='product_id[]']:checked").each(function () {
                    ids.push($(this).val());
                });

                $.ajax({
                    url: "/products/delete_all",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        ids: ids
                    },
                    type: "POST",
                    success: function (response) {
                        if (response.success) {
                            var string = '<div class="alert alert-success">' +
                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                                '<ul><li>' + response.error + '</li></ul></div>';
                            $('#errors').append(string);

                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        } else {
                            var string = '<div class="alert alert-danger">' +
                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                                '<ul><li>' + response.error + '</li></ul></div>';
                            $('#errors').append(string);

                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }
                    }
                });

                $("[data-dismiss=modal]").trigger({type: "click"});
            });
        });
    </script>
@endsection