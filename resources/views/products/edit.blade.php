
@extends('layouts.app')

@section('content')
    <div class="colRight">
        <div class="page-body">
            <div class="container-right">
                <div class='page-heading page-heading-md page-heading-border-bottom'>
                    <h2 class='header__main'><span class='breadcrumb'><a href=''>khách hàng</a> /</span><span
                                class='title'>Sửa mới hàng hóa</span></h2>
                    <div class="header-right">
                    </div>
                </div>

                <div class="" style="padding: 5px" id="errors">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li> {{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>

                <form action="{{ route('product_edit', [$product->id]) }}" data-toggle="validator" role="form"
                      enctype="multipart/form-data" id="createProduct" method="post">
                    {{ csrf_field() }}
                    <div class="col-md-12 .col-lg-12 .col-xl-12">
                        <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                            <h3>Chi tiết hàng hóa</h3>
                        </div>
                        <div class="col-md-9 .col-lg-9 .col-xl-9 marginContent">
                            <div class="tour-white">

                                <div class="field required form-group">
                                    <input id="product_id" name="product_id" type="hidden" value="{{ $product->id }}"/>
                                </div>

                                <div class="field required form-group">
                                    <label class="control-label">Tên hàng</label>
                                    <div class="controls">
                                        <input data-error="Vui lòng nhập hàng." required
                                               class="form-control product-input" id="Name" name="name"
                                               placeholder="Nhập tên hàng" type="text" value="{{ $product->name }}"/>
                                        <span class="help-block with-errors">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
								
								<div class="field required form-group" >
                                    <label class="control-label">Barcode</label>
                                    <div class="controls">
                                        <input data-error="Vui lòng nhập nhà barcode." type="text" required
                                               id="barcode" name="barcode" value="{{ $product->barcode }}"
                                               class="form-control product-input"  placeholder="Nhập barcode cung cấp" />
                                        <span class="help-block with-errors">{{ $errors->first('barcode') }}</span>
                                    </div>
                                </div>

                                <div class="field required form-group">
                                    <label class="control-label">Giá tiền</label>
                                    <div class="controls">
                                        <input data-error="Vui lòng nhập giá tiền." required
                                               class="form-control product-input" id="cost" name="cost"
                                               placeholder="Nhập giá tiền" type="text" value="{{ $product->cost }}"/>
                                        <span class="help-block with-errors">{{ $errors->first('cost') }}</span>
                                    </div>
                                </div>

                                <div class="field required form-group">
                                    <label class="control-label">Số lượng</label>
                                    <div class="controls">
                                        <input data-error="Vui lòng nhập số lượng." required
                                               class="form-control product-input" id="quantity" name="quantity"
                                               placeholder="Nhập số lượng" type="number"
                                               value="{{ $product->quantity }}"/>
                                        <span class="help-block with-errors">{{ $errors->first('quantity') }}</span>
                                    </div>
                                </div>

                                <div class="field required form-group" >
                                    <label class="control-label">Nhà cung cấp</label>
                                    <div class="controls" style="position:relative; width: 400px; display: inline-block;">
                                        <input data-error="Vui lòng nhập nhà cung cấp." type="text" required
                                               id="provider"
                                               class="form-control product-input"  placeholder="Nhập nhà cung cấp" />
                                        <span class="help-block with-errors">{{ $errors->first('provider') }}</span>
                                        <input type="hidden" id="provider-id" name="suppliers_id" value="{{ $product->suppliers_id }}">
                                    </div>
                                </div>

                                 <div class="field form-group" >
                                    <label class="control-label">Khuyến mãi</label>
                                    <div class="controls" style="position:relative; width: 400px; display: inline-block;">
                                        <input data-error="Vui lòng nhập khuyến mãi" type="text" 
                                               id="promotions"
                                               class="form-control product-input"  placeholder="Nhập khuyến mãi" />
                                        <span class="help-block with-errors">{{ $errors->first('promotions') }}</span>
                                        <input type="hidden" id="promotions-id" name="promotions_id" value="{{ $product->promotions_id }}">
                                    </div>
                                </div>

                                

                                <div class="field required form-group">
                                    <label class="control-label">Ngày hết hạn</label>
                                    <div class="controls">
                                        <div class="input-group date " data-provide="datepicker" style="width: 200px">
                                            <input data-error="Vui lòng nhập ngày hết hạn." type="text"
                                                   name="expired_date" id="expired_date" class="form-control"
                                                   value="{{$product->expired_date}}">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                        <span class="help-block with-errors">{{ $errors->first('expired_date') }}</span>
                                    </div>
                                </div>

                                <div class="field required form-group">
                                    <label class="control-label">Mô tả</label>
                                    <div class="controls">
                                    <textarea
                                            class="form-control product-input"
                                            rows="4"
                                            cols="4"
                                            name="description"
                                            required
                                            id="description">{{ $product->description }}</textarea>
                                        <span class="help-block with-errors">{{ $errors->first('description') }}</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 .col-lg-12 .col-xl-12 ">
                    </div>
                    <div class="fixed-btm-padding">
                        <div class="fixed-btm-actions products form-group" style="margin-bottom: 0;">
                            <div class="actions">
                                <div class="right">
                                    <button type="reset" class="buttonfooter button-cancel">
                                        Hủy bỏ
                                    </button>
                                    <button class="buttonfooter button_primary website-submit" type="submit">
                                        Sửa
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js_script')
    <script type="text/javascript">
        $(function () {
            $(".datepicker").datepicker({
                format: 'dd/mm/yyyy',
            });
        });

        $(document).ready(function () {
            var options = {
                url: "/supplier/searchByName",
                getValue: function (element) {
                    return element.name;
                },
                placeholder: "Nhập tên nhà cung cấp",
                ajaxSettings: {
                    dataType: "json",
                    method: "POST",
                    data: {
                        dataType: "json"
                    }
                },
                list: {

                    onClickEvent: function (e) {
                        var data = $("#provider").getSelectedItemData();
                        $("#provider-id").val(data.id);
                    },
                    maxNumberOfElements: 15,
                    match: {
                        enabled: false
                    },
                    sort: {
                        enabled: true
                    }
                },
                preparePostData: function (data) {
                    data.keyword = $("#provider").val();
                    data._token = "{{ csrf_token() }}";
                    return data;
                },
                requestDelay: 400
            };

            $("#provider").easyAutocomplete(options);

            var provider = $('#provider-id').val();
            $.ajax({
                url : '/supplier/show/' + provider,
                method: "GET",
                success: function (data) {
                    $("#provider").val(data.name);
                }
            });

            var options = {
                url: "/promotion/searchByKeyWord",
                getValue: function (element) {
                    return element.name;
                },
                placeholder: "Nhập tên khuyến mãi",
                ajaxSettings: {
                    dataType: "json",
                    method: "POST",
                    data: {
                        dataType: "json"
                    }
                },
                list: {

                    onClickEvent: function (e) {
                        var data = $("#promotions").getSelectedItemData();
                        $("#promotions-id").val(data.id);
                    },
                    maxNumberOfElements: 15,
                    match: {
                        enabled: false
                    },
                    sort: {
                        enabled: true
                    }
                },
                preparePostData: function (data) {
                    data.keyword = $("#promotions").val();
                    data._token = "{{ csrf_token() }}";
                    return data;
                },
                requestDelay: 400
            };

            $("#promotions").easyAutocomplete(options);

            $.ajax({
                url : '/promotion/show/' + provider,
                method: "GET",
                success: function (data) {
                    $("#promotions").val(data.name);
                }
            });
        })
    </script>
@endsection
<style type="text/css">
    .easy-autocomplete input {
        padding-left: 5px !important;
    }

    .easy-autocomplete-container {
        z-index: 10000 !important
    }
</style>
