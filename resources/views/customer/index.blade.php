@extends('layouts.app')

@section('content')
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
    <div class="colRight">
        <div id="load-view-right-content" class="fix-height-right-content">
            <div id="product-detail-form">
                <form method="get" action="{{ route('customers') }}" id="fatherFormFilterAndSavedSearch">
                    <div class="header-common-right">
                        <div class="page-heading page-heading-md page-heading-border-bottom">
                            <h2 class="header__main"><span class="title">Khách hàng</span></h2>
                            <div class="header-right">
                                <div>
                                    @if(\App\Facade\Permission::check('CUSTOMER_ADD'))
                                        <div class="header-fr">
                                            <a class="btn btn-default pull-right btn-a-active"
                                               href="<?php echo action('CustomerController@add');?>"
                                               style="margin-right:0px;">
                                                Thêm mới
                                            </a>
                                        </div>
                                @endif
                                </div>
                                <div class="search-layout-common header-fr header-input-search"
                                     style="margin-right: 8px; background: white; text-align: left;">
                                    <i class="fa fa-search"></i>
                                    <input type="text" name="keywords"
                                           class="form-control search-input ui-autocomplete-input"
                                           placeholder="Tìm kiếm khách hàng ..."
                                           value="{{ isset($_GET['keywords']) ? $_GET['keywords'] : NULL }}">
                                </div>
                            </div>
                        </div>
                        <div class="filter-search-nav">
                            <ul class="filter-tab-list overflow-hidden-x ps-container ps-theme-default"
                                id="filter-tab-list" style="position: relative;">
                                <li class="filter-tab-item filter-tab-active" data-tab-index="1" style="">
                                    <a href="javascript:void(0)" class="filter-tab filter-tab-active">
                                        Danh sách khách hàng
                                    </a>
                                </li>
                                @if(\App\Facade\Permission::check('CUSTOMER_DELETE'))
                                    <li class="filter-tab-item main-filter" id="remove-filter-common">
                                        <a id="deletes" href="#" class="filter-tab"><i class="fa fa-times"
                                                                                       aria-hidden="true"></i> Xóa</a>
                                    </li>
                                @endif

                            </ul>
                        </div>
                    </div>
                    <div class="boder-table">
                        <div id="table-height" class="bulk-action-context">
                            <table class="table defaul-table" id="parent-variant">
                                <thead>
                                <tr>
                                    <th class="text-center checkbox-default">
                                        <span><input type="checkbox" class='js-no-dirty all-bulk-action'></span>
                                    </th>
                                    <th class='col-xs-1'><?php echo App\Helpers::render_sort('id', 'Mã'); ?></th>
                                    <th class='col-xs-2'><?php echo App\Helpers::render_sort('name', 'Tên'); ?></th>
                                    <th class='col-xs-2'><?php echo App\Helpers::render_sort('group_id', 'Nhóm khách hàng'); ?></th>
                                    <th class='col-xs-3'><?php echo App\Helpers::render_sort('email_work', 'Email liên hệ'); ?></th>
                                    <th class='col-xs-2 text-center'><?php echo App\Helpers::render_sort('phone_work', 'Số điện thoại'); ?></th>
                                    <th class='col-xs-2 fix-col-edit text-center'>Action</th>
                                </tr>
                                </thead>
                                <tbody class="tbody-scoler">
                                @foreach ($customers as $key => $customer)
                                    <tr>
                                        <td class="left-td checkbox-default"><input data-id="{{ $customer->id }}"
                                                                                    type="checkbox" value=""
                                                                                    class="bulk-action-item"></td>

                                        <td class="left-td col-xs-1"><span>{{ $customer->id }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ $customer->name }}</span></td>
                                        <td class="left-td col-xs-2"><span>
                                       @if($customer->group_id == 1)
                                                    Bán lẻ
                                                @else
                                                    Bán sỉ
                                                @endif
                                    </span></td>
                                        <td class="left-td col-xs-3"><span>{{ $customer->email }}</span></td>
                                        <td class="left-td col-xs-2 text-center">
                                            <span>{{ $customer->phone_work }}</span></td>
                                        <td class="left-td col-xs-2 fix-col-edit text-center">
                                            @if(\App\Facade\Permission::check('CUSTOMER_UPDATE'))
                                                <a href="{{ route('customer_edit',['id'=>$customer->id]) }}"><i
                                                            class="fa fa-edit"></i> Edit</a>
                                            @endif

                                            @if(\App\Facade\Permission::check('CUSTOMER_UPDATE') && \App\Facade\Permission::check('CUSTOMER_DELETE'))
                                                &nbsp;|&nbsp;
                                            @endif

                                            @if(\App\Facade\Permission::check('CUSTOMER_DELETE'))
                                                <a class="delete" data-id="{{ $customer->id }}"><i
                                                            class="fa fa-trash-o"></i> Delete</a>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="t-grid-pager-boder row">
                            <div class="t-pager t-reset">
                                <div class="col-xs-12">
                                    <div class="t-status-text dataTables_info">
                                        <?php
                                        if (isset($_GET['keywords'])) {
                                            $customers->appends(['keywords' => $_GET['keywords']]);
                                        }
                                        if (isset($_GET['order']) && isset($_GET['order_by'])) {
                                            $customers->appends(['order' => $_GET['order'], 'order_by' => $_GET['order_by']]);
                                        }
                                        echo $customers->links();
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js_script')
    <script type="text/javascript">
        $('.sort').on('click', function (e) {
            e.preventDefault();

            var sort_id = $(this).attr('data-name');
            var sort_value = $(this).attr('data-value');

            $("#fatherFormFilterAndSavedSearch").append("<input name='order' value='" + sort_value + "' />");
            $("#fatherFormFilterAndSavedSearch").append("<input name='order_by' value='" + sort_id + "' />");

            $('#fatherFormFilterAndSavedSearch').submit();
        });

        $('.delete').on('click', function (e) {
            e.preventDefault();

            var id = $(this).data('id');
            var list_id = [];
            list_id.push(id);

            var element_remove = $(this).parent().parent();

             $.confirm({
                title: 'Bạn có muốn xóa?',
                content: 'Bạn có chắc chắn muốn xóa? Những đơn hàng của khách hàng này cũng sẽ được xóa. Những liệu đã xóa sẽ không thể phục hồi!',
                buttons: {
                    'Có, tôi muốn xóa': function () {
                        $.ajax({
                            url: "{{ route('customer_deletes') }}",
                            method: "POST",
                            data: {
                                list_id: list_id,
                                _token: '{{ csrf_token() }}'
                            },
                            success: function (data) {
                                if (data.success) {
                                    element_remove.remove();
                                }
                                $.alert(data.message);
                            },
                            error: function(jqXHR, textStatus, errorThrown) { 
                                $.alert("Error: " + jqXHR.status + " - " + errorThrown);
                            } 
                        });
                    },
                    'Hủy bỏ': function () {
                    },
                }
            });
        });

        $('#deletes').on('click', function () {
            var list_id = [];
            $('#parent-variant input:checked').each(function () {
                list_id.push($(this).attr('data-id'));
            });
            if(list_id.length === 0)
            {
                $.alert('Vui lòng chọn dữ liệu cần xóa');
            }else{
                $.confirm({
                    title: 'Bạn có muốn xóa?',
                    content: 'Bạn có chắc chắn muốn xóa? Những đơn hàng của khách hàng này cũng sẽ được xóa. Những liệu đã xóa sẽ không thể phục hồi!',
                    buttons: {
                        'Có, tôi muốn xóa': function () {
                            $.ajax({
                                url: "{{ route('customer_deletes') }}",
                                method: "POST",
                                data: {
                                    list_id: list_id,
                                    _token: '{{ csrf_token() }}'
                                },
                                success: function (data) {
                                    if (data.success) {
                                        $('#parent-variant input:checked').each(function () {
                                            $(this).parent().parent().remove();
                                        });
                                    }
                                    $.alert(data.message);
                                },
                                error: function(jqXHR, textStatus, errorThrown) { 
                                    $.alert("Error: " + jqXHR.status + " - " + errorThrown);
                                } 
                            });
                        },
                        'Hủy bỏ': function () {
                        },
                    }
                });
            }
        })
    </script>
@endsection