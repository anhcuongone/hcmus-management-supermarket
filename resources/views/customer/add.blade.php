@extends('layouts.app')

@section('content')
    <div class="colRight">
            <div class="page-body">
               <div class="container-right">
                  <div class='page-heading page-heading-md page-heading-border-bottom'>
                     <h2 class='header__main'><span class='breadcrumb'><a href=''>khách hàng</a> /</span><span class='title'>Thêm mới khách hàng</span></h2>
                     <div class="header-right">
                     </div>
                  </div>
                  <form action="{{ route('customer_store') }}" data-toggle="validator" role="form" enctype="multipart/form-data" id="createProduct" method="post">
                  {{ csrf_field() }}   
                     <div class="col-md-12 .col-lg-12 .col-xl-12">
                        <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                           <h3>Chi tiết khách hàng</h3>
                           <span class="colorNote">
                           Điền tên khách hàng và thông tin chi tiết. Khách hàng có thể tạo đơn hàng.
                           </span>
                        </div>
                        <div class="col-md-9 .col-lg-9 .col-xl-9 marginContent">
                           <div class="tour-white">
                              <div class="field required form-group">
                                 <label class="control-label">Tên khách hàng</label>
                                 <div class="controls">
                                    <input data-error="{{ $errors->first('name') }}" required class="form-control product-input" id="Name" name="name" placeholder="Nhập tên khách hàng" type="text" value="{{ old('name') }}" />
                                    <span class="help-block with-errors">{{ $errors->first('name') }}</span>
                                 </div>
                              </div>
                              <div class="field required form-group">
                                 <label class="control-label">Nhóm khách hàng</label>
                                 <div id="selectSupplier" style="display: inline-block;">
                                    <select required data-error="{{ $errors->first('group_id') }}" class="product-input form-control" id="supplierid" name="group_id">
                                       <option value="">Nhóm khách hàng</option>
                                       <option value="1">Bán sỉ</option>
                                       <option value="2">Bán lẻ</option>
                                    </select>
                                 </div>
                                 <span class="help-block with-errors">{{ $errors->first('group_id') }}</span>
                              </div>
                              <div class="form-group required field">
                                 <label class="control-label font-normal">Số điện thoại</label>
                                 <div class="controls">
                                    <input required data-error="{{ $errors->first('phone') }}" class="form-control product-input" id="PhoneNumber" name="phone" placeholder="Nhập số điện thoại" type="text" value="{{ old('phone') }}" />
                                    <span class="help-block with-errors">{{ $errors->first('phone') }}</span>
                                 </div>
                              </div>
                              <div class="form-group required field">
                                 <label class="control-label font-normal"> Email</label>
                                 <div class="controls">
                                    <input required data-error="{{ $errors->first('email') }}" class="form-control product-input" id="Email" name="email" placeholder="Nhập địa chỉ email" type="email" value="{{ old('email') }}" />
                                    <span class="help-block with-errors">{{ $errors->first('email') }}</span>
                                 </div>
                              </div>
                              <div class="field form-group" id="sex">
                                 <label class="control-label">Giới tính</label>
                                 <div class="controls">
                                    <select class="product-input form-control" name="gender">
                                       <option value="male">Nam</option>
                                       <option value="female">Nữ</option>
                                       <option value="other">Khác</option>
                                    </select>
                                 </div>
                                 <span class="help-block help-block">{{ $errors->first('gender') }}</span>
                              </div>
                              <div class="field form-group clearfix">
                                 <label class="control-label">Ngày sinh</label>
                                 <div class="controls">
                                    <div class="dateofbirth order-menu-screen__right_detailorder-issued order-item-right-text datepicker-input fl controls" style="border-color: #CBD5DE;">
                                       <input type="text" class="form-control format-date inline filter-input no-margin" id="birth_transdate" name="birthday" placeholder="Nhập ngày sinh" value="{{ old('birthday') }}">
                                       <span class="input-group-addon padding-lg-right"><i class="fa fa-calendar"></i></span>
                                    </div>
                                 </div>
                                 <span class="help-block help-block">{{ $errors->first('birthday') }}</span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 .col-lg-12 .col-xl-12 ">
                        <div class="h_Box">
                           <div class="col-md-3 .col-lg-3 .col-xl-3 col-sm-3 box-description">
                              <h3>Địa chỉ khách hàng</h3>
                              <span class="colorNote">
                              Thêm địa chỉ khách hàng phục vụ  cho việc tạo đơn hàng, xuất hóa đơn và chuyển hàng.
                              </span>
                           </div>
                           <div class="col-md-9 .col-lg-9 .col-xl-9 col-sm-9 marginContent">
                              <div class="tour-white">
                        <div class='row'>
                                 <div class="col-xs-6">
                                    <div class="required form-group field">
                                       <label class="control-label mar-bottom5">Địa chỉ khách hàng</label>
                                       <input required="required" class="form-control product-input" data-error="{{ $errors->first('address') }}" id="Address_Label" name="address" placeholder="Địa chỉ khách hàng" type="text" value="{{ old('address') }}" />
                              <span class="help-block with-errors">{{ $errors->first('address') }}</span>
                                    </div>
                           
                           
                                    <div class="field form-group">
                                       <label class="control-label mar-bottom5">Email</label>
                                       
                                          <input bind="address.email" class="form-control product-input" data-val="true" data-val-email="Email không đúng định dạng" id="Address_Email" name="email_work" placeholder="Nhập địa chỉ email" type="text" value="{{ old('email_work') }}" />
                                          <span class="help-block help-block-margin"><span class="field-validation-valid" data-valmsg-for="Address.Email" data-valmsg-replace="true"></span></span>
                                       <span class="help-block with-errors">{{ $errors->first('email_work') }}</span>
                                    </div>
                                    <div class="field form-group">
                                       <label class="control-label">Số điện thoại</label>
                                       
                                          <input bind="address.phone_number" class="form-control product-input"  id="Address_PhoneNumber" name="phone_work" placeholder="Nhập số điện thoại" type="text" value="{{ old('phone_work') }}" />
                                         <span class="help-block with-errors">{{ $errors->first('phone_work') }}</span>
                                       
                                    </div>
                                    <div class="form-group field">
                                       <label class="control-label">Mã vùng</label>
                                       <input bind="zip_code" class="product-input form-control" id="Address_ZipCode" name="zipcode" placeholder="Nhập mã vùng" type="text" value="{{ old('zipcode') }}" />
                                       <span class="help-block with-errors">{{ $errors->first('zipcode') }}</span>
                                    </div>
                                 </div>
                                 <div class="col-xs-6">
                                    <div class="field form-group">
                                       <label class="control-label mar-bottom5">Địa chỉ giao hàng</label>
                                       <input class="form-control product-input" id="Address_Address1" name="delivery_address" placeholder="Nơi thanh toán, giao hàng" type="text" value="{{ old('delivery_address') }}" />
                                       <span class="help-block with-errors">{{ $errors->first('delivery_address') }}</span>
                                    </div>
                                    
                                    <div class="field required form-group">
                                       <label class="control-label required  mar-bottom5">Tỉnh/Thành phố</label>
                                       <div id="div-city">
                                          <select required data-error="{{ $errors->first('city') }}" Id="city" class="product-input form-control" id="CityId" name="city">
                                             <option selected="selected" value="">Chọn Tỉnh/Thành phố</option>
                                             @foreach ($province as $key => $value)
                                                 <option value="{{ $value->provinceid }}"
                                                 @if ($value->provinceid == old('city'))
                                                     selected="selected"
                                                 @endif
                                                 >{{ $value->name }}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                       <span class="help-block with-errors">{{ $errors->first('city') }}</span>
                                    </div>
                                    <div class="field required form-group">
                                       <label class="control-label required  mar-bottom5">Quận/Huyện</label>
                                       <div id="div-district">
                                          <select required data-error="{{ $errors->first('district') }}" Id="district" class="product-input form-control" id="DistrictId" name="district" styledropdown="width: 200px;left: 160px;">
                                             <option selected="selected" value="">Chọn Quận/Huyện</option>
                                          </select>
                                       </div>
                                       <span class="help-block with-errors">{{ $errors->first('district') }}</span>
                                    </div>
                                 </div>
                         
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="fixed-btm-padding">
                        <div class="fixed-btm-actions products form-group" style="margin-bottom: 0;">
                           <div class="actions">
                              <div class="right">
                                 <button type="reset" class="buttonfooter button-cancel">
                                 Hủy bỏ
                                 </button>
                                 <button class="buttonfooter button_primary website-submit" type="submit">
                                 Thêm
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
@endsection
@section('js_script')
<script type="text/javascript">
   $('#city').on('change',function(){
      $.ajax({
        url: "{{ route('get_district') }}",
        method: "GET",
        data: { provinceid : $("#city option:selected").val()},
        success:function(data){
         $('#district').html('<option value="">Chọn Quận/Huyện</option>');
         $.each(data, function (i, item) {
             $('#district').append($('<option>', { 
                 value: item.districtid,
                 text : item.type + ' ' + item.name 
             }));
         });
        }
      });
   })
</script>
@endsection

