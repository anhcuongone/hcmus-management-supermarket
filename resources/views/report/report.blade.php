@extends('layouts.app')

<style>
    #chart, #chart-1, #chart-2, #chart-3 {
        min-width: 400px;
        max-width: 800px;
        height: 400px;
        margin: 0 auto
    }
</style>
@section('content')
    <div class="colRight">
        <div id="load-view-right-content" class="fix-height-right-content">
            <div id="product-detail-form">
                <div class="header-common-right">
                    <div class="page-heading page-heading-md page-heading-border-bottom">
                        <h2 class="header__main"><span class="title">Báo cáo & Thông kê</span></h2>
                        <div class="header-right">
                            <div>
                                <div class="header-fr">
                                    <a class="btn btn-default pull-right btn-a-active" href="#" id="statistics"
                                       style="margin-right:5px;">
                                        <span>
                                            <i class='fa fa-edit'></i>
                                            Thống kê
                                        </span>
                                    </a>
                                </div>
                            </div>

                            <div class="header-fr" style="margin-right: 8px; background: white; text-align: left;">
                                <div class="input-group input-daterange">
                                    <input type="text" class="form-control" value="" id="from">
                                    <div class="input-group-addon">To</div>
                                    <input type="text" class="form-control" value="" id="to">
                                </div>
                            </div>

                            <div class="header-fr" style="margin-right: 8px; background: white; text-align: left;">
                                <select class="form-control ui-autocomplete-input" name="option-date" id="option-date"
                                        placeholder="">
                                    <option value="1">Ngày</option>
                                    <option value="2">Tháng</option>
                                    <option value="3">Năm</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="filter-search-nav">
                        <ul class="filter-tab-list overflow-hidden-x ps-container ps-theme-default" id="filter-tab-list"
                            style="position: relative;">
                            <li class="filter-tab-item filter-tab-active" data-tab-index="1" style="">
                                <a href="javascript:void(0)" class="filter-tab filter-tab-active">
                                    Báo cáo & Thông kê
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="" style="padding: 5px" id="errors">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li> {{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="boder-table">
                    <table class="table">
                        <tr>
                            <td style="width: 50%">
                                <div id="order-chart" style="float: left"></div>
                                @if(\App\Facade\Permission::check('REPORT_EXPORT'))
                                    <div style="float: right">
                                        <button type="button" class="btn btn-success" id="export-orders">
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i> &nbsp; Export
                                        </button>
                                    </div>
                                @endif
                            </td>

                            <td style="width: 50%">
                                <div id="products-chart" style="float: left"></div>
                                @if(\App\Facade\Permission::check('REPORT_EXPORT'))
                                    <div style="float: right">
                                        <button type="button" class="btn btn-success" id="export-detail-orders">
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i> &nbsp; Export
                                        </button>
                                    </div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%">
                                <div id="import-chart" style="float: left"></div>
                                @if(\App\Facade\Permission::check('REPORT_EXPORT'))
                                    <div style="float: right">
                                        <button type="button" class="btn btn-success" id="export-import-product">
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i> &nbsp; Export
                                        </button>
                                    </div>
                                @endif
                            </td>

                            <td style="width: 50%">
                                <div id="import-products-chart" style="float: left"></div>
                                @if(\App\Facade\Permission::check('REPORT_EXPORT'))
                                    <div style="float: right">
                                        <button type="button" class="btn btn-success" id="export-import-detail-product">
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i> &nbsp; Export
                                        </button>
                                    </div>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js_script')
    <script type="text/javascript">
        $(document).ready(function () {

            $('.input-daterange input').each(function () {
                $(this).datepicker({
                    format: "dd-mm-yyyy"
                });
            });

            var type = 1;
            $('#option-date').change(function (e) {
                var val = $(this).val();
                type = val;

                switch (val) {
                    case '1':
                        $('.input-daterange input').each(function () {
                            $(this).datepicker("remove");
                            $(this).datepicker({
                                format: "dd-mm-yyyy"
                            });
                        });
                        break;
                    case '2':

                        $('.input-daterange input').each(function () {
                            $(this).datepicker("remove");
                            $(this).datepicker({
                                format: "mm-yyyy",
                                startView: 'months',
                                minViewMode: 'months'
                            });
                        });
                        break;
                    case '3':
                        $('.input-daterange input').each(function () {
                            $(this).datepicker("remove");
                            $(this).datepicker({
                                format: "yyyy",
                                startView: 'years',
                                minViewMode: 'years'
                            });
                        });
                        break;
                }

                $('.input-daterange input').each(function () {
                    $(this).datepicker("update", new Date());
                });
            });

            var date = new Date();
            date.setDate(date.getDate() - 30);

            var toDate = new Date();
            toDate.setDate(toDate.getDate() + 1);

            $('#from').datepicker('setDate', date);
            $('#to').datepicker('setDate', toDate);

            $('#statistics').click(function () {
                var from = $('#from').val();
                var to = $('#to').val();

                $.ajax({
                    url: '/report/getReportOrders',
                    method: "GET",
                    data: {
                        from: from,
                        to: to,
                        type: type
                    },
                    success: function (data) {
                        Highcharts.chart('order-chart', {
                            chart: {
                                type: 'column',
                            },
                            title: {
                                text: 'Thông kê số lượng hóa đơn xuất'
                            },
                            subtitle: {
                                text: 'Số lương hóa đơn theo ngày/tháng/năm'
                            },
                            xAxis: {
                                categories: data.categories,
                                crosshair: true
                            },
                            yAxis: {
                                min: 0,
//                                tickInterval: 1,
                                title: {
                                    text: 'Số lượng'
                                }
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                            series: data.data
                        });
                    }
                });


                $.ajax({
                    url: '/report/getReportProducts',
                    method: "GET",
                    data: {
                        from: from,
                        to: to,
                        type: type
                    },
                    success: function (data) {
                        Highcharts.chart('products-chart', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Thông kê số lượng sản phẩm bán ra'
                            },
                            subtitle: {
                                text: 'Số lương sản phẩm theo ngày/tháng/năm'
                            },
                            xAxis: {
                                categories: data.categories,
                                crosshair: true
                            },
                            colors: [
                                '#DB843D'
                            ],
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Số lượng'
                                }
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                            series: data.data
                        });
                    }
                });

                $.ajax({
                    url: '/report/getReportImports',
                    method: "GET",
                    data: {
                        from: from,
                        to: to,
                        type: type
                    },
                    success: function (data) {
                        Highcharts.chart('import-chart', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Thông kê hóa đơn nhập'
                            },
                            subtitle: {
                                text: 'Số lương hóa đơn nhập theo ngày/tháng/năm'
                            },
                            xAxis: {
                                categories: data.categories,
                                crosshair: true
                            },
                            colors: [
                                '#3D96AE'
                            ],
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Số lượng'
                                }
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                            series: data.data
                        });
                    }
                });

                $.ajax({
                    url: '/report/getReportImportProducts',
                    method: "GET",
                    data: {
                        from: from,
                        to: to,
                        type: type
                    },
                    success: function (data) {
                        Highcharts.chart('import-products-chart', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Thông kê sản phẩm nhập'
                            },
                            subtitle: {
                                text: 'Số lương sản phẩm theo ngày/tháng/năm'
                            },
                            xAxis: {
                                categories: data.categories,
                                crosshair: true
                            },
                            colors: [
                                '#A47D7C'
                            ],
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Số lượng'
                                }
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                            series: data.data
                        });
                    }
                });
            });

            $("#statistics").trigger("click");

            $('#export-orders').click(function () {
                var from = $('#from').val();
                var to = $('#to').val();

                var url = '/report/exportOrders?from=' + from + '&to=' + to + '&type=' + type;
                window.open(url, '_blank');
            });

            $('#export-detail-orders').click(function () {
                var from = $('#from').val();
                var to = $('#to').val();

                var url = '/report/exportOrderDetail?from=' + from + '&to=' + to + '&type=' + type;
                window.open(url, '_blank');
            });

            $('#export-import-product').click(function () {
                var from = $('#from').val();
                var to = $('#to').val();

                var url = '/report/exportImportProducts?from=' + from + '&to=' + to + '&type=' + type;
                window.open(url, '_blank');
            });

            $('#export-import-detail-product').click(function () {
                var from = $('#from').val();
                var to = $('#to').val();

                var url = '/report/exportImportDetailProduct?from=' + from + '&to=' + to + '&type=' + type;
                window.open(url, '_blank');
            });
        });

    </script>
@endsection