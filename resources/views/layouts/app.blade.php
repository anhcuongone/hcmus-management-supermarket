<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('font/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700" rel="stylesheet">
<!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="{{ asset('js/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/nprogress.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('js/EasyAutocomplete/easy-autocomplete.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery-3.2.0.min.js') }}"></script>
    <script src="{{ asset('js/highcharts.src.js') }}"></script>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>


    <script>
        function openTabs(evt, cityName) {
            evt.preventDefault();
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the button that opened the tab
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

    </script>
</head>
<body>
@if (Auth::check())
    <div id="container"
         class="{{ (Request::is('users*') || Request::is('roles*') || Request::is('importproduct*') || Request::is('products*') || Request::is('suppliers*')) ? 'nav-active': null }}">
        <div class="colLeft">
            <nav id="left-nav" class="website-menu">
                <div class="website-menu-top-header">
                    <div class="menu-top-logo">
                        <img src="{{ asset('img/logo.png') }}" class="website-menu-top-logo--short"/>
                    </div>
                </div>

                <div class="website-menu-inner-wrapper clearfix">
                    <div class="website-menu-primary pull-left">
                        <div class="website-menu-primary-inner">
                            <ul class="website-menu-list website-menu-list--primary">

                                <li class="website-menu-list-item website-menu-list-item--primary {{ (Request::is('') || Request::is('home*'))?'website-menu-list-item--active':'' }}">
                                    <a href="/"
                                       class="website-menu-list-item-link website-menu-list-item-link--primary">
                                        <i class="website-menu-list-item-icon fa fa-home"></i>
                                        <span class="website-menu-list-item-label">Tổng quan</span>
                                    </a>
                                </li>

                                @if(\App\Facade\Permission::check('ORDER_VIEW'))
                                    <li class="website-menu-list-item website-menu-list-item--primary {{ (Request::is('order*'))?'website-menu-list-item--active':'' }}">
                                        <a href="{{ route('order') }}"
                                           class="website-menu-list-item-link website-menu-list-item-link--primary">
                                            <i class="website-menu-list-item-icon fa fa-file-text"></i>
                                            <span class="website-menu-list-item-label">Đơn hàng</span>
                                        </a>
                                    </li>
                                @endif

                                @if(\App\Facade\Permission::check('CUSTOMER_VIEW'))
                                    <li class="website-menu-list-item website-menu-list-item--primary {{ (Request::is('customers*'))?'website-menu-list-item--active':'' }}">
                                        <a href="<?php echo action('CustomerController@index'); ?>"
                                           class="website-menu-list-item-link website-menu-list-item-link--primary">
                                            <i class="website-menu-list-item-icon fa fa-users"></i>
                                            <span class="website-menu-list-item-label">Khách hàng</span>
                                        </a>
                                    </li>
                                @endif

                                @if(\App\Facade\Permission::check('PRODUCT_VIEW'))
                                    <li class="website-menu-list-item website-menu-list-item--primary {{ (Request::is('products*'))?'website-menu-list-item--active':'' }}">
                                        <a href="{{ route('products') }}"
                                           class="website-menu-list-item-link website-menu-list-item-link--primary">
                                            <i class="website-menu-list-item-icon fa fa-cubes"></i>
                                            <span class="website-menu-list-item-label">Hàng hóa</span>
                                        </a>
                                    </li>
                                    @elseif(\App\Facade\Permission::check('PRODUCT_IMPORT_VIEW'))
                                    <li class="website-menu-list-item website-menu-list-item--primary {{ (Request::is('products*'))?'website-menu-list-item--active':'' }}">
                                        <a href="{{ route('importproduct') }}"
                                           class="website-menu-list-item-link website-menu-list-item-link--primary">
                                            <i class="website-menu-list-item-icon fa fa-cubes"></i>
                                            <span class="website-menu-list-item-label">Hàng hóa</span>
                                        </a>
                                    </li>
                                    @elseif(\App\Facade\Permission::check('SUPPLIER_VIEW'))
                                    <li class="website-menu-list-item website-menu-list-item--primary {{ (Request::is('products*'))?'website-menu-list-item--active':'' }}">
                                        <a href="{{ route('suppliers') }}"
                                           class="website-menu-list-item-link website-menu-list-item-link--primary">
                                            <i class="website-menu-list-item-icon fa fa-cubes"></i>
                                            <span class="website-menu-list-item-label">Hàng hóa</span>
                                        </a>
                                    </li>
                                @endif

                                @if(\App\Facade\Permission::check('REPORT_VIEW'))
                                    <li class="website-menu-list-item website-menu-list-item--primary {{ (Request::is('report*'))?'website-menu-list-item--active':'' }}">
                                        <a href="{{ route('report') }}"
                                           class="website-menu-list-item-link website-menu-list-item-link--primary">
                                            <i class="website-menu-list-item-icon fa fa-bar-chart"></i>
                                            <span class="website-menu-list-item-label">Báo cáo</span>
                                        </a>
                                    </li>
                                @endif

                                @if(\App\Facade\Permission::check('PROMOTION_VIEW'))
                                    <li class="website-menu-list-item website-menu-list-item--primary ">
                                        <a href="{{ route('promotions') }}"
                                           class="website-menu-list-item-link website-menu-list-item-link--primary">
                                            <i class="website-menu-list-item-icon fa fa-bitcoin"></i>
                                            <span class="website-menu-list-item-label">Khuyến mãi</span>
                                        </a>
                                    </li>
                                @endif

                                {{--<li class="website-menu-list-item website-menu-list-item--primary ">--}}
                                {{--<a href="" class="website-menu-list-item-link website-menu-list-item-link--primary">--}}
                                {{--<i class="website-menu-list-item-icon fa fa-cog"></i>--}}
                                {{--<span class="website-menu-list-item-label">Cài đặt</span>--}}
                                {{--</a>--}}
                                {{--</li>--}}

                                @if(\App\Facade\Permission::check('USER_VIEW') ||
                                    \App\Facade\Permission::check('ROLE_VIEW'))
                                    <li class="website-menu-list-item website-menu-list-item--primary ">
                                        @if (\App\Facade\Permission::check('USER_VIEW'))
                                            <a href="{{ route('users') }}"
                                               class="website-menu-list-item-link website-menu-list-item-link--primary">
                                                <i class="website-menu-list-item-icon fa fa-user-circle"></i>
                                                <span class="website-menu-list-item-label">Tài khoản</span>
                                            </a>
                                        @elseif(\App\Facade\Permission::check('ROLE_VIEW'))
                                            <a href="{{ route('roles') }}"
                                               class="website-menu-list-item-link website-menu-list-item-link--primary">
                                                <i class="website-menu-list-item-icon fa fa-user-circle"></i>
                                                <span class="website-menu-list-item-label">Tài khoản</span>
                                            </a>
                                        @endif
                                    </li>
                                @endif
                            </ul>
                        </div>

                        <div class="website-menu-primary-inner--fix-bottom">
                            <ul class="website-menu-list website-menu-list--primary">
                                <li class="website-menu-list-item website-menu-list-item--primary website-menu-list-item__user-info dropup">
                                    <a href="javascript:void(0)" class="website-menu-list-item-link"
                                       data-toggle="dropdown">
							 <span class="website-menu-list-item-icon-wrapper text-center">
							 <i class="website-menu-list-item-icon fa fa-user website-menu-list-item-icon--user-info"></i>
							 </span>
                                    </a>
                                    <div class="dropdown-menu">
                                        <div class="dropdown-menu-arrow"></div>
                                        <ul class="dropdown-menu-list">
                                            <li>
                                                <a href="">
                                                    <i class="fa fa-user"></i>
                                                    <span class="nav-label">Hồ sơ cá nhân</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">

                                                    <i class="fa fa-sign-out"></i>
                                                    <span class="nav-label">Thoát</span>
                                                </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                      style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    @if(Request::is('users*') || Request::is('roles*'))
                        @include('layouts.navigation.navUser')
                    @endif

                    @if(Request::is('importproduct*') || Request::is('products*') || Request::is('suppliers*'))
                        @include('layouts.navigation.navProduct')
                    @endif

                    {{--@if(Request::is('orders*'))--}}
                    {{--@include('layouts.navigation.navOrder')--}}
                    {{--@endif--}}


                </div>
            </nav>
        </div>

        @yield('content')

    </div>
@else
    @yield('content')
@endif
<?php /*<div id="app">
        <div class="container-fluid">
            <div class="row">
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">

                            <!-- Collapsed Hamburger -->
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                                <span class="sr-only">Toggle Navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <!-- Branding Image -->
                            <a class="navbar-brand" href="{{ url('/') }}">
                                {{ config('app.name', 'Laravel') }}
                            </a>
                        </div>

                        <div class="collapse navbar-collapse" id="app-navbar-collapse">
                            <!-- Left Side Of Navbar -->
                            <ul class="nav navbar-nav">
                                &nbsp;
                            </ul>

                            <!-- Right Side Of Navbar -->
                            <ul class="nav navbar-nav navbar-right">
                                <!-- Authentication Links -->
                                @if (Auth::guest())
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <div class="row">
                @if (Auth::check())
                    <div class="col-md-3">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="{{ url('/home') }}"><i class="fa fa-home fa-fw"></i>Home</a></li>
                            <li><a href="{{ route('profile') }}"><i class="fa fa-list-alt fa-fw"></i>Profile</a></li>
                            <li>
                                <a href="#"><i class="fa fa-list-alt fa-fw"></i>Management</a>
                                <ul class="nav-pills nav-stacked">
                                    <li><a href="{{ route('users') }}"><i class="fa fa-list-alt fa-fw"></i>Management User</a></li>
                                    <li><a href="#"><i class="fa fa-list-alt fa-fw"></i>Management Role</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                @endif
                <div class="col-md-9">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>*/?>

<!-- Scripts -->
<!--<script src="{{ asset('js/app.js') }}"></script>-->
<script src="{{ asset('js/jquery-1.11.2.min.js') }}"></script>
<script src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/validator.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/nprogress.js') }}"></script>
<script src="{{ asset('js/icheck.min.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
<script src="{{ asset('js/EasyAutocomplete/jquery.easy-autocomplete.min.js') }}"></script>
<script src="{{ asset('js/jquery-confirm-master/jquery-confirm.min.js') }}"></script>
<link href="{{ asset('js/jquery-confirm-master/jquery-confirm.min.css') }}" rel="stylesheet">
<style type="text/css">
    .easy-autocomplete {
        position: inherit;
    }

    .easy-autocomplete input {
        padding-left: 30px;
    }
</style>
@yield('js_script')
</body>
</html>
