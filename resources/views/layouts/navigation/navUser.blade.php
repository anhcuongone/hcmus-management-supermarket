<div class="website-menu-secondary pull-left">
    <ul class="website-menu-list website-menu-list--secondary">
        @if(\App\Facade\Permission::check('USER_VIEW'))
            <li class="website-menu-list-item website-menu-list-item--secondary ">
                <a href="{{ route('users') }}"
                   class="website-menu-list-item-link website-menu-list-item-link--secondary {{ (Request::is('users*'))?'website-menu-list-item-link--secondary-active':'' }}">Users</a>
            </li>
        @endif

        @if(\App\Facade\Permission::check('ROLE_VIEW'))
            <li class="website-menu-list-item website-menu-list-item--secondary ">
                <a href="{{ route('roles') }}"
                   class="website-menu-list-item-link website-menu-list-item-link--secondary {{ (Request::is('roles*'))?'website-menu-list-item-link--secondary-active':'' }}">Roles</a>
            </li>
        @endif
    </ul>
</div>