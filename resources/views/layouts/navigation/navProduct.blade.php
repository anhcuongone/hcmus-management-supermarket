<div class="website-menu-secondary pull-left">
    <ul class="website-menu-list website-menu-list--secondary">
        @if(\App\Facade\Permission::check('PRODUCT_VIEW'))
            <li class="website-menu-list-item website-menu-list-item--secondary ">
                <a href="{{ route('products') }}"
                   class="website-menu-list-item-link website-menu-list-item-link--secondary {{ (Request::is('products*'))?'website-menu-list-item-link--secondary-active':'' }}">Hàng
                    hóa</a>
            </li>
        @endif

        @if(\App\Facade\Permission::check('PRODUCT_IMPORT_VIEW'))
            <li class="website-menu-list-item website-menu-list-item--secondary ">
                <a href="{{ route('importproduct') }}"
                   class="website-menu-list-item-link website-menu-list-item-link--secondary {{ (Request::is('importproduct*'))?'website-menu-list-item-link--secondary-active':'' }} ">Nhập
                    Hàng hóa</a>
            </li>
        @endif
        {{--<li class="website-menu-list-item website-menu-list-item--secondary ">--}}
        {{--<a href="#" class="website-menu-list-item-link website-menu-list-item-link--secondary ">Hóa đơn</a>--}}
        {{--</li>--}}

        @if(\App\Facade\Permission::check('SUPPLIER_VIEW'))
            <li class="website-menu-list-item website-menu-list-item--secondary ">
                <a href="{{ route('suppliers') }}"
                   class="website-menu-list-item-link website-menu-list-item-link--secondary {{ (Request::is('suppliers*'))?'website-menu-list-item-link--secondary-active':'' }} ">Nhà
                    cung cấp</a>
            </li>
        @endif
    </ul>
</div>