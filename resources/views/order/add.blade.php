@extends('layouts.app')

@section('content')
    <div class="colRight">
            <div class="page-body website-menu-expand" id="load-view-right-content">
               <div context="editOrder">
                  <form action="{{ route('order_store') }}" enctype="multipart/form-data" id="formOrder" method="post">
                  {{ csrf_field() }}
                     <div class='page-heading page-heading-md'>
                        <h2 class='header__main'><span class='breadcrumb'><a href=''>Đơn hàng</a> /</span><span class='title'>Tạo đơn hàng</span></h2>
                     </div>
                     
                     
                     <div class="order-menu-screen">
                        <div class="menu-left-new">
                           <div class="order-menu-screen__left order-menu-screen__left-order " id="order-menu-screen__left" style="position: relative;">
                              
                              <div class="order-menu-screen__left-scroll w100">
                                 <div style="padding: 0 7.5px" class="clearfix">
									<div class="col-sm-3 no-padding">
                                      
                                       <div class="form-group col-sm-12 search-layout-common no-padding no-margin" style="width: 100%">
                                          <i class="fa fa-search"></i>
                                          <input id="tim_sp" style="height: 29px; font-size: 12px;" id="search-input-prd" type="text" class="form-control search-input ui-autocomplete-input" placeholder="Tìm kiếm sản phẩm">
                                       </div>
                                    </div>
									
                                    <div class="col-sm-3 no-padding">
                                       <div class="form-group col-sm-12 no-padding left-inner-addon disabled" style="width: 100%; height: 35px;">
                                          <div id="input-customer">
                                             <i class="fa fa-user"></i>
                                             <input id="ma_kh" type="text" style="height: 29px;" class="form-control search-customer" placeholder="Chọn khách hàng" />
                                          </div>
                                          
                                       </div>
                                  
                                    </div>
									
									
									
                                    <div class="col-sm-6 no-padding">
                                       <div class="col-sm-6 no-padding">
                                          <div class="form-group col-sm-12 no-padding ui-autocomplete-group " id="address-shipto-select">
                                             <input disabled="disabled" id="delivery_address" class="form-control shipto-select" placeholder="Địa chỉ khách hàng" value="" />
                                          </div>
                                       </div>
                                       <div class="col-sm-6 no-padding">
                                          <div class="form-group col-sm-12 no-padding">
                                             <input disabled="disabled" id="phone_work" class="form-control" id="PhoneNumber" name="PhoneNumber" placeholder="Điện thoại" type="text" value="" />
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="order-menu-screen__left-variant clearfix">
                                    <div class="order-menu-screen__left-variant-scroll w100 table-order-new">
                                       <table class="table defaul-table popup-house table-order-edit" id="table-order">
                                          <thead>
                                             <tr>
                                                <th style="width: 12%" class="left-th">Mã SKU</th>
                                                <th style="width: 28%" class="left-th">Tên hàng hóa</th>
                                                <th style="width: 12%" class="right-th">Số lượng</th>
                                                <th style="width: 12%" class="right-th">Đơn giá</th>
                                                <th style="width: 10%" class="right-th">Thuế (%)</th>
                                                <th style="width: 10%" class="right-th">Giảm giá</th>
                                                <th style="width: 12%" class="right-th">Thành tiền</th>
                                                <th style="width: 2%"></th>
                                             </tr>
                                          </thead>
                                          <tbody id="line_item_rows" style="display: inline-table; width: 100%">
                                          </tbody>
                                       </table>
                                       <div id="no-prd">
                                       </div>
                                    </div>
                                 </div>
                               <div class="order-menu-screen__left-totalamount" style="bottom: 46px;display:block">
                                 <div class='clearfix'>
                           <div class='col-md-6 col-xs-12 pull-right'>
                                 <div class="order-menu-screen__left-totalamount_total order-payment-total__head pull-left">
                                    <ul>
                                      <li>
                                         <span style="color: #1cb467;" class="order-payment-total__head_text discount-order">
                                         Giảm giá <i class="fa fa-caret-down"></i>
                                         </span>
                                         <span id="total_discount" class="order-payment-total__head_value">0đ</span>
                                      </li>
                                      <li>
                                        <span class="order-payment-total__head_text">Tổng tiền</span>
                                        <span id="order_total" class="order-payment-total__head_value">0đ</span>
                                      </li>
                                        <input id="_total" type="hidden" name="total" value="">
                                        <input id="_list_product" type="hidden" name="list_product" value="">
                                        <input id="_id_customer" type="hidden" name="id_customer" value="">
                                        <input id="_total_discount" type="hidden" name="discount" value="">
                                    </ul>
                                 
                                 </div>
                               </div>
                               
                           
                           
                           <div class="col-md-6 col-xs-12">
                              <div class="fixed-noteTag">
                                  <textarea class="form-control" cols="20" id="Note" name="Note" placeholder="Ghi chú" rows="2" style="margin-bottom:7px;padding:7px 10px">{{ old('Note') }}</textarea>
                                  
                                </div>
                           </div>
                           </div>
                         </div>
                              </div>
                              
                              <div class="order-menu-screen__left-payment" style="background: #fff;">
                                 <div class="actions">
                                    <div class="left" style="width: 38%; display: inline-block">
                                    </div>
                                    <div class="right">
                                       <a href="{{ route('orders') }}" class="buttonfooter button-cancel" style="text-align: center;">
                                       Hủy
                                       </a>
                                       <button id="btn_submit" class="buttonfooter button_primary website-submit" type="submit">
                                 Đặt hàng
                                 </button>
                                    </div>
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                       
                        <div class="order-menu-screen__right pull-left menu-right-new" id="order-menu-screen__right">
                           <div class="order-menu-screen__right-scoll pull-left w100 ">
                              <div class="order-menu-screen__right-scoll_invoice pull-left w100">
                                 <div class="order-menu-screen__right-scoll_invoice-header pull-left w100">
                                    <div class="order-menu-screen__right-scoll_invoice-header-left pull-left" style="width: 100%;">
                                       <i class="fa fa-file-text"></i> Thông báo<span class='pull-right'></span>
                                        <p>{{ $errors->first('list_product') }}</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                    
                  </form>
               </div>
            </div>
         </div>
@endsection
@section('js_script')
<script type="text/javascript">
   var options = {

      url: function(phrase) {
        return "{{ route('customer_searchByKeyWord') }}";
      },

      getValue: function(element) {
        return element.name;
      },

      list: {
        maxNumberOfElements: 14,
        onClickEvent: function() {
           $("#delivery_address").val($("#ma_kh").getSelectedItemData().address);
           $("#phone_work").val($("#ma_kh").getSelectedItemData().phone);
           $("#_id_customer").val($("#ma_kh").getSelectedItemData().id);
        } 
      },

      ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
          dataType: "json",
        }
      },

      preparePostData: function(data) {
        data.keyword = $("#ma_kh").val();
        data._token = "{{ csrf_token() }}";
        return data;
      },

      requestDelay: 400
    };

    $("#ma_kh").easyAutocomplete(options);

    var options = {

      url: function(phrase) {
        return "{{ route('search_product') }}";
      },

      getValue: function(element) {
        return element.name + " - Số Lượng: "+element.quantity;
      },

      list: {
        maxNumberOfElements: 14,
        onClickEvent: function() {
          var flag_duplicated = false;
          // check item exist
          $("#line_item_rows tr").each(function(){
              var id_product = $(this).find('.id_product').html();
              var quantity_product = parseInt($(this).find('.product_quantity').val());
              if(id_product == $("#tim_sp").getSelectedItemData().id)
              {
                  $(this).find('.product_quantity').val(quantity_product+1);
                  flag_duplicated = true;
              }
          });
          if(flag_duplicated)
              return true;
          var sl = 1;
          var dongia = $("#tim_sp").getSelectedItemData().cost;
          var vat = 10;
          var discount = $("#tim_sp").getSelectedItemData().discount;
          discount = (discount === null) ? 0 : discount;
           $("#line_item_rows").append('<tr>'
              +'<td stlye="width: 12%" class="id_product">'+$("#tim_sp").getSelectedItemData().id+'</td>'
              +'<td style="width: 28%" class="name_product">'+$("#tim_sp").getSelectedItemData().name+'</td>'
              +'<td style="width: 12%"><input class="form-control product_quantity" data-warehouse="'+$("#tim_sp").getSelectedItemData().quantity+'" value="'+sl+'"/></td>'
              +'<td style="width: 12%"><span class="product_dongia">' + dongia + '</span></td>'
              +'<td style="width: 10%" ><input class="form-control product_vat" value="'+vat+'"/></td>'
              +'<td style="width: 10%; text-align: center;"><span class="product_discount">' + discount +'</span></td>'
              +'<td style="width: 12%"><span class="product_total">'+parseFloat(sl*dongia + (sl*(dongia*(vat/100))) - discount) + '</span></td>'
              +'<td style="width: 2%" class="product_remove"><i class="fa fa-remove"></i></td>'
              +'</tr>');
           calc_order_total();
        } 
      },

      ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
          dataType: "json",
        }
      },

      preparePostData: function(data) {
        data.keyword = $("#tim_sp").val();
        data._token = "{{ csrf_token() }}";
        return data;
      },

      requestDelay: 400
    };

    $("#tim_sp").easyAutocomplete(options);

    $(document).on('keyup', '.product_quantity', function(e) {
      var sl = $(this).val();
      var dongia = $(this).parent().parent().find('.product_dongia').html();
      var discount = $(this).parent().parent().find('.product_discount').html();
      var vat = $(this).parent().parent().find('.product_vat').val();
     $(this).parent().parent().find('.product_total').html(parseFloat(sl*dongia + (sl*(dongia*(vat/100))) - discount));
     calc_order_total();
    });

    $(document).on('keyup', '.product_vat', function(e) {
      var vat = $(this).val();
      var dongia = $(this).parent().parent().find('.product_dongia').html();
      var sl = $(this).parent().parent().find('.product_quantity').val();
      var discount = $(this).parent().parent().find('.product_discount').html();
     $(this).parent().parent().find('.product_total').html(parseFloat(sl*dongia + (sl*(dongia*(vat/100))) - discount));
     calc_order_total();
    });

    $(document).on('click', '.product_remove', function(e) {
      $(this).parent().remove();
     calc_order_total();
    });

    function calc_order_total()
    {
      var total = 0;
      var total_discount = 0;
      
      $('#line_item_rows tr').each(function(){
        total += parseFloat($(this).find('.product_total').html());
        total_discount += parseFloat($(this).find('.product_discount').html());
      });

      $('#order_total').html(total);
      $('#_total').val(total);

      $('#total_discount').html(total_discount);
      $('#_total_discount').val(total_discount);

      var list_product = [];
      $("#line_item_rows tr").each(function(){

        var item = {
          'products_id' : $(this).find('.id_product').html(),
          'name_product' : $(this).find('.name_product').html(),
          'quantity_product' : $(this).find('.product_quantity').val(),
          'cost_product' : $(this).find('.product_dongia').html(),
          'vat_product' : $(this).find('.product_vat').val(),
          'discount_product' : $(this).find('.product_discount').html(),
          'total' : $(this).find('.product_total').html(),
        }
        list_product.push(item);
      });

      $('#_list_product').val(JSON.stringify(list_product));
    }

    //Check submit
    $('#btn_submit').on('click', function(e){
      e.preventDefault();
      var message = '';
      
      if($("#_id_customer").val().length == 0)
        message += '<p>Thông tin khách hàng không được để trống</p>';

      if($("#line_item_rows tr").text().length == 0)
        message += '<p>Vui lòng chọn sản phẩm</p>';

      $("#line_item_rows tr").each(function(){
        var quantity = $(this).find('.product_quantity').val();
        var name_product = $(this).find('.name_product').html();
        var warehouse = $(this).find('.product_quantity').data('warehouse');
        if(quantity <= 0)
        {
            message += '<p>Số lượng sản phẩm phải > 0.</p>';
        }else{
            if(quantity > warehouse)
                message += '<p><i>'+name_product+' số lượng '+quantity+' > số lượng trong kho: '+warehouse+'</i></p>';
        }
      });

      if(message !== '')
      {
        $.alert(message);
      }else{
        $('#formOrder').submit();
      }
    })
</script>
@endsection
