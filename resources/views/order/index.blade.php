@extends('layouts.app')

@section('content')
    <div class="colRight">
        <div id="load-view-right-content" class="fix-height-right-content">
            <div id="product-detail-form">
                <form method="get" action="{{ route('orders') }}" id="fatherFormFilterAndSavedSearch">
                    <div class="header-common-right">
                        <div class="page-heading page-heading-md page-heading-border-bottom">
                            <h2 class="header__main"><span class="title">Đơn hàng</span></h2>
                            <div class="header-right">
                                <div>
                                    @if(\App\Facade\Permission::check('ORDER_ADD'))
                                        <div class="header-fr">
                                            <a class="btn btn-default pull-right btn-a-active"
                                               href="<?php echo action('OrderController@add');?>" style="margin-right:0px;">
                                                Thêm mới
                                            </a>
                                        </div>
                                    @endif

                                    {{--<div class="header-fr header-button-margin">--}}
                                        {{--<a href="javascript:void(0)"--}}
                                           {{--class="btn btn-default pull-right header-button-border-default header-button-gl export_supplier">--}}
                                            {{--Xuất file--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="header-fr">--}}
                                        {{--<a href="javascript:void(0)"--}}
                                           {{--class="btn btn-default pull-right header-button-border-default header-button-gr import_supplier">--}}
                                            {{--Nhập file--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                </div>
                                <div class="search-layout-common header-fr header-input-search"
                                     style="margin-right: 8px; background: white; text-align: left;">
                                    <i class="fa fa-search"></i>
                                    <input type="text" name="keywords"
                                           class="form-control search-input ui-autocomplete-input"
                                           placeholder="Tìm kiếm đơn hàng ..."
                                           value="{{ isset($_GET['keywords']) ? $_GET['keywords'] : NULL }}">
                                </div>
                            </div>
                        </div>
                        <div class="filter-search-nav">
                            <ul class="filter-tab-list overflow-hidden-x ps-container ps-theme-default"
                                id="filter-tab-list" style="position: relative;">
                                <li class="filter-tab-item filter-tab-active" data-tab-index="1" style="">
                                    <a href="javascript:void(0)" class="filter-tab filter-tab-active">
                                        Danh sách Đơn hàng
                                    </a>
                                </li>

                                @if(\App\Facade\Permission::check('ORDER_DELETE'))
                                    <li class="filter-tab-item main-filter" id="remove-filter-common">
                                        <a id="deletes" href="#" class="filter-tab"><i class="fa fa-times"
                                                                                       aria-hidden="true"></i> Xóa</a>
                                    </li>
                                @endif

                            </ul>
                        </div>
                    </div>

                    <div class="" style="padding: 5px" id="errors">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li> {{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="boder-table">
                        <div id="table-height" class="bulk-action-context">
                            <table class="table defaul-table" id="parent-variant">
                                <thead>
                                <tr>
                                    <th style="width: 3%;" class="checkbox-default text-center">
                                        <span><input type="checkbox" class='js-no-dirty all-bulk-action'></span>
                                    </th>
                                    <th class='col-xs-1'><?php echo App\Helpers::render_sort('orders.id', 'Mã đơn hàng'); ?></th>
                                    <th class='col-xs-1'><?php echo App\Helpers::render_sort('customers.name', 'Khách hàng'); ?></th>
                                    <th class='col-xs-2'><?php echo App\Helpers::render_sort('customers.address', 'Địa chỉ'); ?></th>
                                    <th class='col-xs-1'><?php echo App\Helpers::render_sort('orders.status', 'Trạng thái'); ?></th>
                                    <th class='col-xs-1'><?php echo App\Helpers::render_sort('customers.phone_work', 'Số điện thoại'); ?></th>
                                    <th 
                                        class='text-center col-xs-2'><?php echo App\Helpers::render_sort('orders.total', 'Tổng tiền'); ?></th>
                                    <th 
                                        class='text-center col-xs-2'><?php echo App\Helpers::render_sort('orders.created_at', 'Ngày ghi nhận'); ?></th>
                                    <th style="" class='text-center col-xs-2 fix-col-edit'>Action</th>
                                </tr>
                                </thead>
                                <tbody class="tbody-scoler">
                                @foreach ($orders as $key => $item)
                                    <tr>
                                        <td class="left-td checkbox-default"><input data-id="{{ $item->id }}" type="checkbox" value=""
                                                                   class="bulk-action-item"></td>

                                        <td class="left-td col-xs-1"><span>{{ $item->id }}</span></td>
                                        <td class="left-td col-xs-1"><span>{{ $item->name }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ $item->address }}</span></td>
                                        <td class="left-td text-center col-xs-1"><span>{{ ($item->status == 1) ? "Đã thanh toán" : "Chưa thanh toán" }}</span></td>
                                        <td class="left-td col-xs-1 text-center"><span>{{ $item->phone_work }}</span></td>
                                        <td class="left-td text-center col-xs-2"><span>{{ number_format($item->total) }}</span></td>
                                        <td class="left-td text-center col-xs-2"><span>{{ $item->created_at }}</span></td>

                                        <td class="left-td text-center col-xs-2 fix-col-edit">
                                            @if(\App\Facade\Permission::check('ORDER_UPDATE'))
                                                <a href="{{ route('order_edit',['id'=>$item->id]) }}"><i class="fa fa-edit"></i> Edit</a>
                                            @endif

                                            @if(\App\Facade\Permission::check('ORDER_UPDATE') && \App\Facade\Permission::check('ORDER_DELETE'))
                                                  |
                                            @endif
                                            &nbsp;
                                            @if(\App\Facade\Permission::check('ORDER_DELETE'))
                                                <a class="delete" data-id="{{ $item->id }}"><i class="fa fa-trash-o"></i> Delete</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="t-grid-pager-boder row">
                            <div class="t-pager t-reset">
                                <div class="col-xs-12">
                                    <div class="t-status-text dataTables_info">
                                        <?php
                                        if (isset($_GET['keywords'])) {
                                            $orders->appends(['keywords' => $_GET['keywords']]);
                                        }
                                        if (isset($_GET['order']) && isset($_GET['order_by'])) {
                                            $orders->appends(['order' => $_GET['order'], 'order_by' => $_GET['order_by']]);
                                        }
                                        echo $orders->links();
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js_script')
    <script type="text/javascript">
        $('.sort').on('click', function (e) {
            e.preventDefault();

            var sort_id = $(this).attr('data-name');
            var sort_value = $(this).attr('data-value');

            $("#fatherFormFilterAndSavedSearch").append("<input name='order' value='" + sort_value + "' />");
            $("#fatherFormFilterAndSavedSearch").append("<input name='order_by' value='" + sort_id + "' />");

            $('#fatherFormFilterAndSavedSearch').submit();
        });

        $('.delete').on('click', function (e) {
            e.preventDefault();

            var id = $(this).data('id');
            var list_id = [];
            list_id.push(id);

            var element_remove = $(this).parent().parent();

             $.confirm({
                title: 'Bạn có muốn xóa?',
                content: 'Bạn có chắc chắn muốn xóa? Những liệu đã xóa sẽ không thể phục hồi!',
                buttons: {
                    'Có, tôi muốn xóa': function () {
                        $.ajax({
                            url: "{{ route('order_deletes') }}",
                            method: "POST",
                            data: {
                                list_id: list_id,
                                _token: '{{ csrf_token() }}'
                            },
                            success: function (data) {
                                if (data.success) {
                                    element_remove.remove();
                                }
                                $.alert(data.message);
                            },
                            error: function(jqXHR, textStatus, errorThrown) { 
                                $.alert("Error: " + jqXHR.status + " - " + errorThrown);
                            } 
                        });
                    },
                    'Hủy bỏ': function () {
                    },
                }
            });
        });

        $('#deletes').on('click', function () {
            var list_id = [];
            $('#parent-variant input:checked').each(function () {
                list_id.push($(this).attr('data-id'));
            });
            if(list_id.length === 0)
            {
                $.alert('Vui lòng chọn dữ liệu cần xóa');
            }else{
                $.confirm({
                    title: 'Bạn có muốn xóa?',
                    content: 'Bạn có chắc chắn muốn xóa? Những liệu đã xóa sẽ không thể phục hồi!',
                    buttons: {
                        'Có, tôi muốn xóa': function () {
                            $.ajax({
                                url: "{{ route('order_deletes') }}",
                                method: "POST",
                                data: {
                                    list_id: list_id,
                                    _token: '{{ csrf_token() }}'
                                },
                                success: function (data) {
                                    if (data.success) {
                                        $('#parent-variant input:checked').each(function () {
                                            $(this).parent().parent().remove();
                                        });
                                    }
                                    $.alert(data.message);
                                },
                                error: function(jqXHR, textStatus, errorThrown) { 
                                    $.alert("Error: " + jqXHR.status + " - " + errorThrown);
                                } 
                            });
                        },
                        'Hủy bỏ': function () {
                        },
                    }
                });
            }
        })
    </script>
@endsection