@extends('layouts.app')

@section('content')
<div class="colRight">
	<div class="background-help">
	   <div class="">
		  <div class="help">
			 <h2>Chào mừng bạn đến với Website!</h2>
			 <p>Hãy hoàn thành 3 bước để bắt đầu bán hàng ngay bây giờ</p>
			 <div class="list-btn" style="height:180px">
				<a class="btn-help" href="{{ route('product_add') }}">
				   <div class="btn-img-help">
					  <img alt="website" src="{{ asset('img/hanghoa.png') }}"/>
				   </div>
				   <p>Thêm hàng hóa</p>
				</a>
				<div class="ico-arrow">
				   <img alt="website" src="{{ asset('img/arrow.png') }}"/>
				</div>
				<a class="btn-help" href="{{ route('customer_add') }}">
				   <div class="btn-img-help" style="padding-top: 25px;">
					  <img alt="website" src="{{ asset('img/khachhang.png') }}"/>
				   </div>
				   <p>Thêm khách hàng</p>
				</a>
				<div class="ico-arrow">
				   <img alt="website" src="{{ asset('img/arrow.png') }}"/>
				</div>
				<a class="btn-help" href="{{ route('order_add') }}">
				   <div class="btn-img-help"  style="padding-top: 30px;">
					  <img alt="website" src="{{ asset('img/banhang.png') }}"/>
				   </div>
				   <p>Bắt đầu bán hàng</p>
				</a>
			 </div>
			 <div class="col-sm-12" >
				<a href="" class="btn-skip btn">
				Về trang tổng quan
				</a>
			 </div>
		  </div>
	   </div>
	</div>
</div>
@endsection
