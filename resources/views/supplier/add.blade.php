@extends('layouts.app')

@section('content')
    <div class="colRight">
            <div class="page-body">
               <div class="container-right">
                  <div class='page-heading page-heading-md page-heading-border-bottom'>
                     <h2 class='header__main'><span class='breadcrumb'><a href=''>nhà cung cấp</a> /</span><span class='title'>Thêm mới nhà cung cấp</span></h2>
                     <div class="header-right">
                     </div>
                  </div>
                  <form action="{{ route('supplier_store') }}" data-toggle="validator" role="form" enctype="multipart/form-data" id="createProduct" method="post">
                  {{ csrf_field() }}   
                     <div class="col-md-12 .col-lg-12 .col-xl-12">
                        <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                           <h3>Chi tiết nhà cung cấp</h3>
                           <span class="colorNote">
                           Điền tên nhà cung cấp và thông tin chi tiết. Nhà cung cấp có thể tạo đơn nhập hàng.
                           </span>
                        </div>
                        <div class="col-md-9 .col-lg-9 .col-xl-9 marginContent">
                           <div class="tour-white">
                              <div class="field required form-group">
                                 <label class="control-label">Tên nhà cung cấp</label>
                                 <div class="controls">
                                    <input data-error="Vui lòng nhập tên nhà cung cấp." required class="form-control product-input" id="Name" name="name" placeholder="Nhập tên nhà cung cấp" type="text" value="{{ old('name') }}" />
                                    <span class="help-block with-errors">{{ $errors->first('name') }}</span>
                                 </div>
                              </div>
                              <div class="field form-group">
                                 <label class="control-label">Mã nhà cung cấp</label>
                                 <div class="controls">
                                    <input disabled="disabled" class="form-control product-input" id="id" name="id" placeholder="Mã nhà cung cấp" type="text" value="{{ old('id') }}" />
                                    <span class="help-block with-errors">{{ $errors->first('id') }}</span>
                                 </div>
                              </div>
                              <div class="form-group required field">
                                 <label class="control-label font-normal">Số điện thoại</label>
                                 <div class="controls">
                                    <input required data-error="{{ $errors->first('phone') }}" class="form-control product-input" id="PhoneNumber" name="phone" placeholder="Nhập số điện thoại" type="text" value="{{ old('phone') }}" />
                                    <span class="help-block with-errors">{{ $errors->first('phone') }}</span>
                                 </div>
                              </div>
                              <div class="form-group field">
                                 <label class="control-label font-normal"> Địa chỉ nhà cung cấp</label>
                                 <div class="controls">
                                    <input class="form-control product-input" id="Email" name="address" placeholder="Nhập địa chỉ" type="text" value="{{ old('address') }}" />
                                    <span class="help-block help-block-margin">{{ $errors->first('address') }}</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="fixed-btm-padding">
                        <div class="fixed-btm-actions products form-group" style="margin-bottom: 0;">
                           <div class="actions">
                              <div class="right">
                                 <button type="reset" class="buttonfooter button-cancel">
                                 Hủy bỏ
                                 </button>
                                 <button class="buttonfooter button_primary website-submit" type="submit">
                                 Thêm
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
@endsection

