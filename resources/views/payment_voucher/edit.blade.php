@extends('layouts.app')

@section('content')
    <div class="colRight">
            
            <div class="page-body">
               <div class="container-right">
                  <div class='page-heading page-heading-md page-heading-border-bottom'>
                     <h2 class='header__main'><span class='breadcrumb'><a href=''>Phiếu chi</a> /</span><span class='title'>Thêm mới phiếu chi</span></h2>
                     <div class="header-right">
                     </div>
                  </div>
                  <form action="{{ route('payment_vouchers_update') }}" data-toggle="validator" role="form" enctype="multipart/form-data" id="createProduct" method="post">
                     {{ csrf_field() }}
                     <input type="hidden" name="_method" value="PUT">
                  <input type="hidden" name="id" value="{{$payment_voucher->id}}">
                <div class="row">
                <div class="col-md-12 .col-lg-12 .col-xl-12">
                    <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                        <h3>Thông tin phiếu</h3>
                        <span class="colorNote">                         
                            Phiếu chi khi nhận tiền thanh toán đơn hàng. công nợ, thanh toán hàng
                            trả lại nhà cung cấp.
                         
                        </span>
                    </div>
                    <div class="col-md-9 .col-lg-9 .col-xl-9 marginContent">
                        <div class="tour-white">

                            <div class="form-group field">
                                <label class="control-label">Đối tượng</label>
                                <div class="controls">
                                    <select Id="use-object-id" class="form-control product-input" datatype="dropdown" id="ObjectType" name="type_customer">
                                    <?php 
                                          $op_customer = [
                                             ["name"=>"Khách hàng","value"=>"customer"],
                                             ["name"=>"Nhà cung cấp","value"=>"supplier"],
                                             ["name"=>"Nhân viên","value"=>"employee"],
                                             ["name"=>"Đối tượng khác","value"=>"other"],
                                          ];
                                       ?>
                                       @foreach ($op_customer as $key => $item)
                                          <option value="{{ $item['value']}}"
                                          @if ($item['value'] == old('type_customer',$payment_voucher->type_customer))
                                                     selected="selected"
                                          @endif
                                          >{{ $item['name'] }}</option>
                                       @endforeach
                                    </select>
                                </div>
                                <span class="help-block with-errors">{{ $errors->first('type_customer') }}</span>
                            </div>
                       
                            <div class="field required form-group" id="customer">
                                <label class="control-label">Khách hàng</label>
                                <div class="controls">
                                    <input name="name_customer" type="text" data-error="Nhập tên khách hàng" required placeholder="Nhập tên khách hàng" class="form-control product-input search-customer" value="{{ old('name_customer',$payment_voucher->name_customer) }}" />
                           <span class="help-block with-errors">{{ $errors->first('name_customer') }}</span>
                                </div>
                            </div>

                            <div class="field required form-group">
                                <label class="control-label">Loại phiếu chi</label>
                                    <div class="payment_group" style="display: inline-block;">                                      
                                            <select id="group_id" bind="group_id" name="type_voucher" class="product-input form-control" datatype="dropdown" styledropdown="left: 160px;width: 200px">
                                                <?php 
                                                  $op_voucher = [
                                                     ["name"=>"Thu nợ khách hàng","value"=>1],
                                                     ["name"=>"Nhượng bán, thanh lý tài sản","value"=>2],
                                                     ["name"=>"Cho thuê tài sản","value"=>3],
                                                     ["name"=>"Tiền bồi thường","value"=>4],
                                                     ["name"=>"Tiền thưởng","value"=>5],
                                                     ["name"=>"Thu nhập khác","value"=>6],
                                                  ];
                                               ?>
                                               @foreach ($op_voucher as $key => $item)
                                                  <option value="{{ $item['value']}}"
                                                  @if ($item['value'] == old('type_voucher',$payment_voucher->type_voucher))
                                                             selected="selected"
                                                  @endif
                                                  >{{ $item['name'] }}</option>
                                               @endforeach
                                            </select>
                                        
                                    </div>
                               <span class="help-block with-errors">{{ $errors->first('type_voucher') }}</span>
                            </div>
                            
                            <div class="field form-group">
                                <label class="control-label">Ngày ghi nhận</label>
                                <div class="order-menu-screen__right_detailorder-issued order-item-right-text datepicker-input pull-left" style="border-color: #CBD5DE;width:200px;">
                                    
                                    <div class="form-datepicker-common">
                                        <input type="text" class="format-date form-control inline filter-input no-margin invoiced_on" id="transdate" value="05-04-2017" name="IssuedOn">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <span class="help-block with-errors">{{ $errors->first('created_at') }}</span>
                            </div>


                            <div class="form-group field required" style="clear:both;padding-top:10px;">
                                <label class="control-label font-normal">Chi nhánh</label>
                                <div class="controls">
                                    <select class="form-control product-input" data-val="true" data-val-number="The field LocationId must be a number." datatype="dropdown" id="location_id" name="branch_id" styledropdown="width: 200px;left: 160px;"><option value="17465">Chi nhánh mặc định</option>
</select>
                                </div>
                                <span class="help-block with-errors">{{ $errors->first('branch_id') }}</span>
                            </div>

                            <div class="product-descriptions">
                                <div class="form-group field">
                                    <label class="control-label">Mô tả</label>
                                    <table class="product-description" style="border:none">
                                        <tr>
                                            <td style="border:none; padding-top:5px">
                                                <textarea class="form-control product-input" cols="20" id="Note" name="description" placeholder="" rows="3" type="text">
{{ old('description', $payment_voucher->description) }}</textarea>
<span class="help-block with-errors">{{ $errors->first('description') }}</span>

                                            </td>
                                        </tr>
                                    </table>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-12 .col-lg-12 .col-xl-12" style="margin-bottom:10%;">
                    <div class="h_Box">
                        <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                            <h3>Chi tiết phiếu chi</h3>
                            <span class="colorNote">
                                Lựa chọn phương thức thanh toán, tiền tệ và điền giá trị cho phiếu chi.
                            </span>
                        </div>
                        <div class="col-md-9 .col-lg-9 .col-xl-9 marginContent">
                            <div class="tour-white">
                                <div class="form-group field required ">
                                    <label class="control-label font-normal">Phương thức</label>
                                    <div class="controls">
                                        <select class="form-control product-input" data-val="true" data-val-number="The field PaymentMethodId must be a number." datatype="dropdown" id="payment_method_id" name="method" styledropdown="width: 200px;left: 160px;z-index:9999">
                                        <?php 
                                          $op_method = [
                                             ["name"=>"COD","value"=>"60736"],
                                             ["name"=>"Chuyển khoản","value"=>"60735"],
                                             ["name"=>"Tiền mặt","value"=>"60734"],
                                          ];
                                       ?>
                                       @foreach ($op_method as $key => $item)
                                          <option value="{{ $item['value']}}"
                                          @if ($item['value'] == old('method',$payment_voucher->method))
                                                     selected="selected"
                                          @endif
                                          >{{ $item['name'] }}</option>
                                       @endforeach
                                        </select>
                                    </div>
                                    <span class="help-block with-errors">{{ $errors->first('method') }}</span>
                                </div>
                                
                                

                                <div class="form-group field required">
                                    <label class="control-label font-normal">Giá trị</label>
                                    <div class="controls">
                                        <div class="input-group" style="width: 200px; height: 33px;">
                                            <input bind="amount" name="cash" class="form-control  intConvert form-control-input-popup valid" style="height: 33px;" type-number="number" value="{{ old('cash',$payment_voucher->cash) }}">

                                            
                                            <span class="input-group-addon border-default" id="iso"> VND</span>
                                        </div>

                                    </div>
                                    <span class="help-block with-errors">{{ $errors->first('cash') }}</span>
                                </div>

                                <div class="form-group field">
                                    <label class="control-label font-normal"></label>
                                    <div class="controls">
                                        <div class="input-group" style="width:200px;height:33px">
                                            <input bind="counted" id="Counted" name="Counted" type="checkbox" value="true" /><input name="Counted" type="hidden" value="false" />  <span style="vertical-align: middle;"> Hạch toán kết quả kinh doanh</span>
                                        </div>

                                    </div>
                                </div>

                           

                            </div>
                        </div>
                    </div>

                </div>




            </div>
         
               <div class="fixed-btm-padding">
                        <div class="fixed-btm-actions products form-group" style="margin-bottom: 0;">
                           <div class="actions">
                              <div class="right">
                                 <button type="reset" class="buttonfooter button-cancel">
                                 Hủy bỏ
                                 </button>
                                 <button class="buttonfooter button_primary website-submit" type="submit">
                                 Thêm
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>

                     
                  </form>
               </div>
            </div>
         </div>
@endsection

