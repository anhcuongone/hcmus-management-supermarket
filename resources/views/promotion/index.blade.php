@extends('layouts.app')

@section('content')
    <div class="colRight">
        <div id="load-view-right-content" class="fix-height-right-content">
            <div id="product-detail-form">
                <form method="get" action="{{ route('promotions') }}" id="fatherFormFilterAndSavedSearch">
                    <div class="header-common-right">
                        <div class="page-heading page-heading-md page-heading-border-bottom">
                            <h2 class="header__main"><span class="title">Chương trình khuyến mãi</span></h2>
                            <div class="header-right">
                                <div>
                                    @if(\App\Facade\Permission::check('PROMOTION_ADD'))
                                        <div class="header-fr">
                                            <a class="btn btn-default pull-right btn-a-active"
                                               href="<?php echo action('PromotionController@add');?>"
                                               style="margin-right:0px;">
                                                Thêm mới
                                            </a>
                                        </div>
                                    @endif
                                    {{--<div class="header-fr header-button-margin">--}}
                                    {{--<a href="javascript:void(0)" class="btn btn-default pull-right header-button-border-default header-button-gl export_supplier">--}}
                                    {{--Xuất file--}}
                                    {{--</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="header-fr">--}}
                                    {{--<a href="javascript:void(0)" class="btn btn-default pull-right header-button-border-default header-button-gr import_supplier">--}}
                                    {{--Nhập file--}}
                                    {{--</a>--}}
                                    {{--</div>--}}
                                </div>
                                <div class="search-layout-common header-fr header-input-search"
                                     style="margin-right: 8px; background: white; text-align: left;">
                                    <i class="fa fa-search"></i>
                                    <input type="text" name="keywords"
                                           class="form-control search-input ui-autocomplete-input"
                                           placeholder="Tìm kiếm khuyến mãi ..."
                                           value="{{ isset($_GET['keywords']) ? $_GET['keywords'] : NULL }}">
                                </div>
                            </div>
                        </div>
                        <div class="filter-search-nav">
                            <ul class="filter-tab-list overflow-hidden-x ps-container ps-theme-default"
                                id="filter-tab-list" style="position: relative;">
                                <li class="filter-tab-item filter-tab-active" data-tab-index="1" style="">
                                    <a href="javascript:void(0)" class="filter-tab filter-tab-active">
                                        Danh sách khuyến mãi
                                    </a>
                                </li>
                                @if(\App\Facade\Permission::check('PROMOTION_DELETE'))
                                    <li class="filter-tab-item main-filter" id="remove-filter-common">
                                        <a id="deletes" href="#" class="filter-tab"><i class="fa fa-times"
                                                                                       aria-hidden="true"></i> Xóa</a>
                                    </li>
                                @endif

                            </ul>
                        </div>
                    </div>
                    <div class="boder-table">
                        <div id="table-height" class="bulk-action-context">
                            <table class="table defaul-table" id="parent-variant">
                                <thead>
                                <tr>
                                    <th style="width: 3%;" class="checkbox-default text-center">
                                        <span><input type="checkbox" class='js-no-dirty all-bulk-action'></span>
                                    </th>
                                    <th class='col-xs-2'><?php echo App\Helpers::render_sort('id', 'Mã'); ?></th>
                                    <th class='col-xs-3'><?php echo App\Helpers::render_sort('name', 'Tên khuyến mãi'); ?></th>
                                    <th class='col-xs-2'><?php echo App\Helpers::render_sort('cost', 'Giảm giá'); ?></th>
                                    <th class='col-xs-3'><?php echo App\Helpers::render_sort('created_at', 'Ngày tạo'); ?></th>
                                    <th class='text-center col-xs-2 fix-col-edit'>Edit</th>
                                </tr>
                                </thead>
                                <tbody class="tbody-scoler">
                                @foreach ($promotions as $key => $promotion)
                                    <tr>
                                        <td class="left-td checkbox-default"><input data-id="{{ $promotion->id }}"
                                                                                    type="checkbox" value=""
                                                                                    class="bulk-action-item"></td>

                                        <td class="left-td col-xs-2"><span>{{ $promotion->id }}</span></td>
                                        <td class="left-td col-xs-3"><span>{{ $promotion->name }}</span></td>
                                        <td class="left-td col-xs-2"><span>{{ number_format($promotion->cost) }}</span></td>
                                        <td class="left-td col-xs-3"><span>{{ $promotion->created_at }}</span></td>
                                        <td class="left-td text-center col-xs-2 fix-col-edit">
                                            @if(\App\Facade\Permission::check('PROMOTION_UPDATE'))
                                                <a href="{{ route('promotion_edit',['id'=>$promotion->id]) }}"><i
                                                            class="fa fa-edit"></i> Edit</a>
                                            @endif

                                            @if(\App\Facade\Permission::check('PROMOTION_DELETE') && \App\Facade\Permission::check('PROMOTION_UPDATE'))
                                                &nbsp;&nbsp;|&nbsp;
                                            @endif
                                            @if(\App\Facade\Permission::check('PROMOTION_DELETE'))
                                                <a class="delete" data-id="{{ $promotion->id }}"><i
                                                            class="fa fa-trash-o"></i> Delete</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="t-grid-pager-boder row">
                            <div class="t-pager t-reset">
                                <div class="col-xs-12">
                                    <div class="t-status-text dataTables_info">
                                        <?php
                                        if (isset($_GET['keywords'])) {
                                            $promotions->appends(['keywords' => $_GET['keywords']]);
                                        }
                                        if (isset($_GET['order']) && isset($_GET['order_by'])) {
                                            $promotions->appends(['order' => $_GET['order'], 'order_by' => $_GET['order_by']]);
                                        }
                                        echo $promotions->links();
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js_script')
    <script type="text/javascript">
        $('.sort').on('click', function (e) {
            e.preventDefault();

            var sort_id = $(this).attr('data-name');
            var sort_value = $(this).attr('data-value');

            $("#fatherFormFilterAndSavedSearch").append("<input name='order' value='" + sort_value + "' />");
            $("#fatherFormFilterAndSavedSearch").append("<input name='order_by' value='" + sort_id + "' />");

            $('#fatherFormFilterAndSavedSearch').submit();
        });

        $('.delete').on('click', function (e) {
            e.preventDefault();

            var id = $(this).data('id');
            var list_id = [];
            list_id.push(id);

            var element_remove = $(this).parent().parent();

             $.confirm({
                title: 'Bạn có muốn xóa?',
                content: 'Bạn có chắc chắn muốn xóa? Những liệu đã xóa sẽ không thể phục hồi!',
                buttons: {
                    'Có, tôi muốn xóa': function () {
                        $.ajax({
                            url: "{{ route('promotion_deletes') }}",
                            method: "POST",
                            data: {
                                list_id: list_id,
                                _token: '{{ csrf_token() }}'
                            },
                            success: function (data) {
                                if (data.success) {
                                    element_remove.remove();
                                }
                                $.alert(data.message);
                            },
                            error: function(jqXHR, textStatus, errorThrown) { 
                                $.alert("Error: " + jqXHR.status + " - " + errorThrown);
                            } 
                        });
                    },
                    'Hủy bỏ': function () {
                    },
                }
            });
        });

        $('#deletes').on('click', function () {
            var list_id = [];
            $('#parent-variant input:checked').each(function () {
                list_id.push($(this).attr('data-id'));
            });
            if(list_id.length === 0)
            {
                $.alert('Vui lòng chọn dữ liệu cần xóa');
            }else{
                $.confirm({
                    title: 'Bạn có muốn xóa?',
                    content: 'Bạn có chắc chắn muốn xóa? Những liệu đã xóa sẽ không thể phục hồi!',
                    buttons: {
                        'Có, tôi muốn xóa': function () {
                            $.ajax({
                                url: "{{ route('promotion_deletes') }}",
                                method: "POST",
                                data: {
                                    list_id: list_id,
                                    _token: '{{ csrf_token() }}'
                                },
                                success: function (data) {
                                    if (data.success) {
                                        $('#parent-variant input:checked').each(function () {
                                            $(this).parent().parent().remove();
                                        });
                                    }
                                    $.alert(data.message);
                                },
                                error: function(jqXHR, textStatus, errorThrown) { 
                                    $.alert("Error: " + jqXHR.status + " - " + errorThrown);
                                } 
                            });
                        },
                        'Hủy bỏ': function () {
                        },
                    }
                });
            }
        })
    </script>
@endsection