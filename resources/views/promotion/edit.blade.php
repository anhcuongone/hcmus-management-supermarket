@extends('layouts.app')

@section('content')
    <div class="colRight">
            <div class="page-body">
               <div class="container-right">
                  <div class='page-heading page-heading-md page-heading-border-bottom'>
                     <h2 class='header__main'><span class='breadcrumb'><a href=''>khuyến mãi</a> /</span><span class='title'>Thêm mới khuyến mãi</span></h2>
                     <div class="header-right">
                     </div>
                  </div>
                  <form action="{{ route('promotion_update') }}" data-toggle="validator" role="form" enctype="multipart/form-data" id="createProduct" method="POST">
                  <input type="hidden" name="_method" value="PUT">
                  <input type="hidden" name="id" value="{{$promotion->id}}">
                  {{ csrf_field() }}   
                     <div class="col-md-12 .col-lg-12 .col-xl-12">
                        <div class="col-md-3 .col-lg-3 .col-xl-3 box-description">
                           <h3>Chi tiết khuyến mãi</h3>
                           <span class="colorNote">
                           Điền thông tin chương trình khuyến mãi
                           </span>
                        </div>
                        <div class="col-md-9 .col-lg-9 .col-xl-9 marginContent">
                           <div class="tour-white">
                              <div class="field required form-group">
                                 <label class="control-label">Tên khuyến mãi</label>
                                 <div class="controls">
                                    <input data-error="{{ $errors->first('name') }}" required class="form-control product-input" id="Name" name="name" placeholder="Nhập tên chương trình khuyến mãi" type="text" value="{{ old('name',$promotion->name) }}" />
                                    <span class="help-block with-errors">{{ $errors->first('name') }}</span>
                                 </div>
                              </div>
                              <div class="field required form-group">
                                 <label class="control-label">Giá khuyến mãi</label>
                                 <div class="controls">
                                    <input data-error="{{ $errors->first('cost') }}" required class="form-control product-input" id="Name" name="cost" placeholder="Nhập mức giá giảm" type="text" value="{{ old('cost',$promotion->cost) }}" />
                                    <span class="help-block with-errors">{{ $errors->first('cost') }}</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 .col-lg-12 .col-xl-12 ">
                     <div class="fixed-btm-padding">
                        <div class="fixed-btm-actions products form-group" style="margin-bottom: 0;">
                           <div class="actions">
                              <div class="right">
                                 <a href="{{ route('promotions') }}" class="buttonfooter button-cancel">
                                 Hủy bỏ
                                 </a>
                                 <button class="buttonfooter button_primary website-submit" type="submit">
                                 Lưu lại
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
@endsection