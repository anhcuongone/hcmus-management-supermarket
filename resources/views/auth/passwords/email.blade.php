@extends('layouts.app')

@section('content')
<style>
	body{
		min-width: auto;
		background-color: #242b39;
	}
</style>
<div id="login-form">
		<div class="container main-login">
<form data-toggle="validator" role="form" action="login.php" method="post">
{{ csrf_field() }}
     <img class='logo-login' src="{{ asset('img/logo.png')}}" />
    <p class='title'>Phần mềm quản lý bán hàng</p>
        <div class="inputText" style="text-align:center;">
            <h3 class="text-center margin-xl-bottom">Bạn quên mật khẩu?</h3>
            <p  style="text-align: center; color: #ff6060;">
                Vui lòng nhập địa chỉ Email tài khoản của bạn để lấy lại mật khẩu
            </p>
			<div class="field required form-group">
			<input id="email" placeholder="Địa chỉ Email" type="email" data-error="{{ $errors->first('email') }}" class="form-control login-input" name="email" value="{{ old('email') }}" required>          
			<div class="has-error">
				<span class="help-block with-errors"></span>			
			</div>
			</div>
            <a class="forget-pass" href="{{ route('login') }}">Đăng nhập trở lại</a>

            <input type="submit" value="Gửi" />
        </div>
</form> 
    <img class="line-login" src="{{ asset('img/line.png')}}" />
    <p class='title'>Tổng đài hỗ trợ khách hàng: 1900_ _ _ _ _</p>
    
</div>


   </div>
   
<!--
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
@endsection
