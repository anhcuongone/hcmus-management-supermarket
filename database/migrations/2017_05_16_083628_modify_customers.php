<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('customers', function (Blueprint $table) {
            $table->string('name',255)->change();
            $table->integer('group_id')->change();
            $table->string('gender',10)->change();
            $table->integer('status')->default(1)->change();
            $table->integer('city')->default(0)->change();
            $table->integer('district')->default(0)->change();
            $table->string('zipcode')->nullable()->change();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
