<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_voucher', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_customer');
            $table->string('name_customer');
            $table->integer('type_voucher');
            $table->integer('branch_id');
            $table->string('description');
            $table->integer('method');
            $table->integer('cash');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
