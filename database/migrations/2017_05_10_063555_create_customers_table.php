<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->decimal('group_id');
            $table->string('email_work');
            $table->string('phone_work');
            $table->string('phone')->nullable();
            $table->string('email',160)->nullable();
            $table->decimal('status');
            $table->string('gender',10);
            $table->dateTime('birthday')->nullable();
            $table->string('address');
            $table->string('delivery_address')->nullable();
            $table->string('zipcode');
            $table->integer('city');
            $table->integer('district');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
