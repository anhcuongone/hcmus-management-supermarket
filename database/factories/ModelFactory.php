<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Model\Customers::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName  . ' ' . $faker->lastName,
        'group_id' => random_int(1 ,10),
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'gender' => $faker->randomElement(['male', 'female']),
        'birthday' => $faker->dateTime,
        'address' =>$faker->address,
        'email_work' => $faker->companyEmail,
        'phone_work' => $faker->phoneNumber,
        'zipcode' => $faker->postcode
    ];
});



/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Model\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text(100),
        'cost' => $faker->randomFloat(100, 1000),
        'quantity' => random_int(1 ,10),
        'suppliers_id' => random_int(1 , 20),
        'expired_date' => (new \DateTime())->add(new \DateInterval('P1Y'))
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Model\Suppliers::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'address' =>$faker->address,
    ];
});