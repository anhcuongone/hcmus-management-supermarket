<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('DB_CONNECTION') == 'mysql') {
            DB::unprepared(file_get_contents(__DIR__ . '/../../doc/mysql-structure-db.sql'));
            DB::unprepared(file_get_contents(__DIR__ . '/../../doc/mysql-role-permission.sql'));
            $this->call(UsersTableSeeder::class);
        }

        if (env('DB_CONNECTION') == 'pgsql') {
            DB::unprepared(file_get_contents(__DIR__ . '/../../doc/pgsql-structure-db.sql'));
            DB::unprepared(file_get_contents(__DIR__ . '/../../doc/pgsql-role-permission.sql'));
        }

        $this->call(CustomersTableSeeder::class);
        $this->call(SuppliersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
    }
}
