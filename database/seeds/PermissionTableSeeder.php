<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'key' => 'ORDER_VIEW',
                'name' => 'Xem hóa đơn'
            ],
            [
                'key' => 'ORDER_DELETE',
                'name' => 'Xóa hóa đơn'
            ],
            [
                'key' => 'ORDER_UPDATE',
                'name' => 'Sửa hóa đơn'
            ],
            [
                'key' => 'ORDER_ADD',
                'name' => 'Tạo hóa đơn'
            ],
            [
                'key' => 'CUSTOMER_VIEW',
                'name' => 'Xem khách hàng'
            ],
            [
                'key' => 'CUSTOMER_DELETE',
                'name' => 'Xóa khách hàng'
            ],
            [
                'key' => 'CUSTOMER_UPDATE',
                'name' => 'Sủa khách hàng'
            ],
            [
                'key' => 'CUSTOMER_ADD',
                'name' => 'Thêm khách hàng'
            ],
            [
                'key' => 'PRODUCT_VIEW',
                'name' => 'Xem hàng hóa'
            ],
            [
                'key' => 'PRODUCT_DELETE',
                'name' => 'Xóa hàng hóa'
            ],
            [
                'key' => 'PRODUCT_UPDATE',
                'name' => 'Sửa hàng hóa'
            ],
            [
                'key' => 'PRODUCT_ADD',
                'name' => 'Thêm hàng hóa'
            ],
            [
                'key' => 'PRODUCT_IMPORT_VIEW',
                'name' => 'Xem nhập hàng hóa'
            ],
            [
                'key' => 'PRODUCT_IMPORT_DELETE',
                'name' => 'Xóa nhập hàng hóa'
            ],
            [
                'key' => 'PRODUCT_IMPORT_UPDATE',
                'name' => 'Sửa nhập hàng hóa'
            ],
            [
                'key' => 'PRODUCT_IMPORT_ADD',
                'name' => 'Thêm nhập hàng hóa'
            ],
            [
                'key' => 'REPORT_VIEW',
                'name' => 'Xem bao cáo'
            ],
            [
                'key' => 'REPORT_EXPORT',
                'name' => 'Export excel'
            ],
            [
                'key' => 'PROMOTION_VIEW',
                'name' => 'Xem khuyến mãi'
            ],
            [
                'key' => 'PROMOTION_DELETE',
                'name' => 'Xóa khuyến mãi'
            ],
            [
                'key' => 'PROMOTION_UPDATE',
                'name' => 'Sửa khuyến mãi'
            ],
            [
                'key' => 'PROMOTION_ADD',
                'name' => 'Thêm khuyến mãi'
            ],
            [
                'key' => 'SUPPLIER_VIEW',
                'name' => 'Xem nhà cung cấp'
            ],
            [
                'key' => 'SUPPLIER_DELETE',
                'name' => 'Xóa nhà cung cấp'
            ],
            [
                'key' => 'SUPPLIER_UPDATE',
                'name' => 'Sửa nhà cung cấp'
            ],
            [
                'key' => 'SUPPLIER_ADD',
                'name' => 'Thêm nhà cung cấp'
            ],
            [
                'key' => 'USER_VIEW',
                'name' => 'Xem người dùng'
            ],
            [
                'key' => 'USER_DELETE',
                'name' => 'Xóa người dùng'
            ],
            [
                'key' => 'USER_UPDATE',
                'name' => 'Sửa người dùng'
            ],
            [
                'key' => 'USER_ADD',
                'name' => 'Thêm người dùng'
            ],
            [
                'key' => 'ROLE_VIEW',
                'name' => 'Xem Role'
            ],
            [
                'key' => 'ROLE_DELETE',
                'name' => 'Xóa Role'
            ],
            [
                'key' => 'ROLE_EDIT',
                'name' => 'Sửa Role'
            ],
            [
                'key' => 'ROLE_ADD',
                'name' => 'Thêm Role'
            ]
        ];


        foreach ($permissions as $permission) {
            DB::table('permissions')->insert($permission);
        }
    }
}
