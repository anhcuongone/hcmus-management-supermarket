<?php
use Illuminate\Database\Seeder;

class UsersTableSeeder  extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Ly Anh Cuong',
            'email' => 'cuong.ly@gmail.com',
            'password' => bcrypt('123123'),
        ]);
    }
}