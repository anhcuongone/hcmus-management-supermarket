<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
//        factory(\App\Model\Product::class , 50)->create();
        $data = [
            [
                'suppliers_id' => random_int(1, 13),
                'name' => 'Sữa Bột Vinamilk Canxi Pro Hộp 400G (30T)',
                'cost' => '140000',
                'quantity' => '100',
                'description' => 'Sữa Bột Vinamilk Canxi Pro Hộp 400G (30T)',
                'expired_date' => '2018-05-27 12:09:56',
                'barcode' => null,
            ],
            [
                'suppliers_id' => random_int(1, 13),
                'name' => 'Mũ Bảo Hiểm 3/4 Napoli',
                'cost' => '200000',
                'quantity' => '200',
                'description' => 'Mũ Bảo Hiểm 3/4 Napoli - Hồng Đậm Nhám - Nâu',
                'expired_date' => '2020-05-27 12:09:56',
                'barcode' => null,
            ],
            [
                'suppliers_id' => random_int(1, 13),
                'name' => 'Giày Mọi Sledgers',
                'cost' => '235000',
                'quantity' => '200',
                'description' => 'Giày Mọi Sledgers Heath (Sm61Lf14L) Black 40 (Đen)',
                'expired_date' => '2020-05-27 12:09:56',
                'barcode' => null,
            ],
            [
                'suppliers_id' => random_int(1, 13),
                'name' => 'Cây nước nóng lạnh',
                'cost' => '70000',
                'quantity' => '200',
                'description' => 'Cây nước nóng lạnh SHD9698 xanh ngọc',
                'expired_date' => '2020-05-27 12:09:56',
                'barcode' => null,
            ],
            [
                'suppliers_id' => random_int(1, 13),
                'name' => 'Máy lọc nước R.O 9 lõi Sunhouse',
                'cost' => '600000',
                'quantity' => '52',
                'description' => 'Máy lọc nước R.O 9 lõi Sunhouse',
                'expired_date' => '2020-05-27 12:09:56',
                'barcode' => null,
            ],
            [
                'suppliers_id' => random_int(1, 13),
                'name' => 'Xiaomi Redmi 3S 16Gb Gold',
                'cost' => '5000000',
                'quantity' => '52',
                'description' => 'Xiaomi Redmi 3S 16Gb Gold',
                'expired_date' => '2020-05-27 12:09:56',
                'barcode' => null,
            ],
            [
                'suppliers_id' => random_int(1, 13),
                'name' => 'Apple iPhone 7 Plus 256GB (Vàng hồng) ',
                'cost' => '20000000',
                'quantity' => '10',
                'description' => 'Apple iPhone 7 Plus 256GB (Vàng hồng) ',
                'expired_date' => '2020-05-27 12:09:56',
                'barcode' => null,
            ],
            [
                'suppliers_id' => random_int(1, 13),
                'name' => 'Bánh Snack Corn',
                'cost' => '10000',
                'quantity' => '10',
                'description' => 'Bánh Snack Corn Chip Vị Bắp Nướng Bơ Orion Gói 38 G',
                'expired_date' => '2020-05-27 12:09:56',
                'barcode' => null,
            ],
            [
                'suppliers_id' => random_int(1, 13),
                'name' => 'Snack Hành Sachi',
                'cost' => '10000',
                'quantity' => '10',
                'description' => 'Snack Hành Sachi Gói 45G',
                'expired_date' => '2020-05-27 12:09:56',
                'barcode' => null,
            ],
            [
                'suppliers_id' => random_int(1, 13),
                'name' => 'Bàn Chải Đánh Răng Wellbeing',
                'cost' => '30000',
                'quantity' => '10',
                'description' => 'Bàn Chải Đánh Răng Wellbeing Nano Gold Vỉ 1 Cái',
                'expired_date' => '2020-05-27 12:09:56',
                'barcode' => null,
            ],

        ];

        foreach ($data as $datum) {
            $datum['barcode'] = rand(10000000, 999999999);
            DB::table('products')->insert($datum);
        }
    }
}
