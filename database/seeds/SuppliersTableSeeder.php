<?php

use Illuminate\Database\Seeder;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(\App\Model\Suppliers::class , 21)->create();
        $data = [
            [
                'name' => 'CÔNG TY SONG TẠO',
                'phone' => '(08) 3.821.6690',
                'address' => '23 Trịnh Văn Cấn, P.Cầu Ông Lãnh, Q.1, TP.HCM',
            ],
            [
                'name' => 'CÔNG TY TNHH IN & BAO BÌ DMB',
                'phone' => '08.39974330',
                'address' => '245 A Huỳnh Văn Bánh, P.12,Q.Phú Nhuận, HCM',
            ],
            [
                'name' => 'CÔNG TY IN VĂN HÓA SÀI GÒN',
                'phone' => '38550906',
                'address' => '754 Hàm Tử, Phường 10, Quận 5, HCM',
            ],
            [
                'name' => 'CÔNG TY LION PRINTING',
                'phone' => '39435425',
                'address' => '60/3a Tân Mỹ, P. tân thuận tây, Q.5, TP.HCM',
            ],
            [
                'name' => 'CÔNG TY ITAXA',
                'phone' => '39300917',
                'address' => '122-124-126 Nguyễn Thị Minh Khai, Phường 6, Quận 3, HCM',
            ],
            [
                'name' => 'CÔNG TY CP IN VIỆT PHÁT',
                'phone' => '055.2240999',
                'address' => '287/5 Phan Đình Phùng, TP.Quảng Ngãi, Quảng Ngãi',
            ],
            [
                'name' => 'Công ty Đức Trí',
                'phone' => '055.2240999',
                'address' => '287/5 Phan Đình Phùng, TP.Quảng Ngãi, Quảng Ngãi',
            ],
            [
                'name' => 'Công Ty TNHH Campina Việt Nam',
                'phone' => '(08) 35180861',
                'address' => 'Tầng8, Tòa Nhà HUD, 159 Điện Biên Phủ, P. 15, Q. Bình Thạnh, Tp. Hồ Chí Minh',
            ],
            [
                'name' => 'Công Ty TNHH Sản Xuất Thương Mại Đông Anh',
                'phone' => '(08) 38865610',
                'address' => 'Số 30 Đường 36, P. Bình Trị Đông, Q. Bình Tân Tp. Hồ Chí Minh ',
            ],
            [
                'name' => 'Công Ty TNHH Trung Long',
                'phone' => '(0236) 3712266',
                'address' => '526/6 Trần Cao Vân, Q. Thanh Khê, Tp. Đà Nẵng',
            ],
            [
                'name' => 'Công Ty Cổ Phần Tăng ích Mỹ',
                'phone' => '0902665308',
                'address' => 'Tầng 3 Tòa Nhà Lữ Gia, 70 Lữ Gia, Phường 15, Quận 11',
            ],
            [
                'name' => 'Công Ty TNHH Humana Việt Nam',
                'phone' => '(08) 39492529',
                'address' => '62/36 Trương Công Định, P. 14, Q. Tân Bình, Tp. Hồ Chí Minh',
            ],
            [
                'name' => 'Công Ty Cổ Phần Lothamilk',
                'phone' => '(061) 3511329',
                'address' => 'Quốc Lộ 51, Km 14, X. Tam Phước, TP. Biên Hòa, Đồng Nai',
            ],

        ];

        foreach ($data as $datum) {
            DB::table('suppliers')->insert($datum);
        }
    }
}
