$(document).ready(function() {
    NProgress.start()
});
$(window).load(function() {
    NProgress.done()
});

function formatDateTime() {
	$('.format-date').datepicker({
		format: 'mm-dd-yyyy',
			   todayBtn: 'linked'
	});
}

function resizeTable() {
	$("#table-height .tbody-scoler").height($(window).height() - 237); 
	$("#table-height").height($(window).height() - 107);
}

$(window).resize(function(){
	resizeTable();
})

$(document).ready(function () {
	resizeTable();
	formatDateTime();
	
    $(".all-bulk-action").click(function () {
        $(".bulk-action-item").prop('checked', $(this).prop('checked'));
    });
});