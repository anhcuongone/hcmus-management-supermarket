<?php
/**
 * Created by PhpStorm.
 * User: AnhCuong
 * Date: 09-Apr-17
 * Time: 4:15 PM
 */

namespace App;

/**
 * Class Application
 * @package App
 */
class Application extends \Illuminate\Foundation\Application
{
    /**
     * Application constructor.
     * @param null|string $basePath
     */
    public function __construct($basePath)
    {
        parent::__construct($basePath);
    }
}