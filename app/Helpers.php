<?php

namespace App;

/**
* 
*/
class Helpers
{
	public static function render_sort($data_name, $text)
	{
		$order_value = (isset($_GET['order'])) ? $_GET['order'] : NULL;
		$order_by = (isset($_GET['order_by'])) ? $_GET['order_by'] : NULL;

		if($order_by != $data_name)
		{
         //Chưa sắp xếp
			return '<a class="sort" data-name="'.$data_name.'" data-value="asc" href=""><span class="glyphicon glyphicon-sort" aria-hidden="true"></span> '.$text.'</a>';
		}else
		{
			if($order_value == 'desc' && $order_by == $data_name)
			{
            //Sắp xếp giảm dần
				return '<a class="sort" data-name="'.$data_name.'" data-value="asc" href=""><span class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true"></span> '.$text.'</a>';
			}
         //Sắp xếp tăng dần
			return '<a class="sort" data-name="'.$data_name.'" data-value="desc" href=""><span class="glyphicon glyphicon-sort-by-attributes" aria-hidden="true"></span> '.$text.'</a>';
		}
	}
}
