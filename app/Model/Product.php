<?php
/**
 * Created by PhpStorm.
 * User: cuong.ly
 * Date: 13/05/2017
 * Time: 12:16 SA
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /** @var string  */
    public $table = 'products';

    /** @var array  */
    public $fillable = ['name', 'description', 'cost', 'quantity', 'suppliers_id', 'expired_date', 'barcode', 'promotions_id'];
}