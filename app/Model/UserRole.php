<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    public $fillable = ['user_id', 'role_id'];
}
