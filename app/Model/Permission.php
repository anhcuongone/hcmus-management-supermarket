<?php
/**
 * Created by PhpStorm.
 * User: AnhCuong
 * Date: 09-Apr-17
 * Time: 3:17 PM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'key'
    ];
}