<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'province';
}
