<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Promotions extends Model
{
    /**
     * @var string
     */
    public $table = 'promotions';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'cost'
    ];
}
