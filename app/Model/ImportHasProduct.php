<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ImportHasProduct extends Model
{
    //
	protected $table = 'import_has_product';
}
