<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentVoucher extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payment_voucher';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_customer', 'name_customer', 'type_voucher', 'branch_id', 'description', 'method', 'cash'
    ];
}
