<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderHasProducts extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_has_products';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_order', 'id_product', 'name', 'quantity', 'vat', 'cost', 'total', 'created_at'
    ];
}
