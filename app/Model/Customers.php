<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    /**
     * @var string
     */
    public $table = 'customers';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'group_id', 'phone', 'email', 'gender', 'birthday', 'address', 'email_work', 'phone_work', 'city', 'district', 'zipcode', 'delivery_address'
    ];
}
