<?php

namespace App\Http\Controllers;

use App\Model\PaymentVoucher;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PaymentVoucherController extends Controller
{
    public $limit = 20;

    public function index(Request $request)
    {
        $query = PaymentVoucher::query();
        if(!empty($request->keywords))
        {
            $query = $query->where('name_customer','like','%'.$request->keywords.'%')->orWhere('id','like','%'.$request->keywords.'%');
        }
        if(!empty($request->order_by) && !empty($request->order))
        {
            $query = $query->orderBy($request->order_by, $request->order);
        }
        $data = $query->paginate($this->limit);

        return view('payment_voucher.index',['payment_vouchers'=>$data]);
    }

    public function add()
    {
    	return view('payment_voucher.add');
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
	        'type_customer' => 'required',
	        'name_customer' => 'required|alpha_num',
	        'type_voucher' => 'required|numeric',
	        'branch_id' => 'required|numeric',
	        'description' => 'alpha',
	        'method' => 'required|numeric',
	        'cash' => 'required|numeric',
	    ]);

	    $data = $request->all();
	    // $data['birthday'] = new \DateTime($request->birthday);
	    
	    $payment_voucher = new PaymentVoucher($data);
        $payment_voucher->save();

        return redirect('payment_vouchers');
    }

    public function edit($id)
    {
    	$payment_voucher = PaymentVoucher::find($id);

    	return view('payment_voucher.edit',['payment_voucher'=>$payment_voucher]);
    }

    public function update(Request $request)
    {
    	$this->validate($request, [
	        'type_customer' => 'required',
	        'name_customer' => 'required|alpha_num',
	        'type_voucher' => 'required|numeric',
	        'branch_id' => 'required|numeric',
	        'description' => 'alpha',
	        'method' => 'required|numeric',
	        'cash' => 'required|numeric',
	    ]);

	    $data = $request->except(['_token','_method','IssuedOn','Counted']);
	    // $data['birthday'] = new \DateTime($request->birthday);

        $payment_voucher = new PaymentVoucher($data);
        $payment_voucher->where('id',$request->id)->update($data);

        return redirect('payment_vouchers');
    }

    public function delete(Request $request)
    {
    	$this->validate($request, [
	        'id' => 'required|numeric',]);
    	$payment_voucher = PaymentVoucher::find($request->id)->delete();

    	return redirect('payment_vouchers');
    }

    public function deletes(Request $request)
    {
        $this->validate($request, [
            'list_id' => 'required',]);
        return new JsonResponse(['success' => TRUE, 'error' => PaymentVoucher::whereIn('id', $request->list_id)->delete()]);
        return new JsonResponse(['success' => FALSE, 'error' => "Có lỗi trong quá trình xóa"]);
    }
}
