<?php

namespace App\Http\Controllers;


use App\Facade\Permission;
use App\Model\Promotions;
use App\Model\Province;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    public $limit = 20;

    public function index(Request $request)
    {
        if(!Permission::check('PROMOTION_VIEW')) {
            throw new \Exception('You do not have permission');
        }

        $query = Promotions::query();
        if (!empty($request->keywords)) {
            if (!is_numeric($request->keywords)) {
                $query = $query->where('name', 'like', '%' . $request->keywords . '%');
            } else {
                $query = $query->where('id', '=', $request->keywords);
            }
        }

        if (!empty($request->order_by) && !empty($request->order)) {
            $query = $query->orderBy($request->order_by, $request->order);
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->paginate($this->limit);

        return view('promotion.index', ['promotions' => $data]);
    }

    public function add()
    {
        if(!Permission::check('PROMOTION_ADD')) {
            throw new \Exception('You do not have permission');
        }

        return view('promotion.add');
    }

    public function store(Request $request)
    {
        if(!Permission::check('PROMOTION_ADD')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'name' => 'required|max:255',
            'cost' => 'required|numeric',
        ]);

        $data = $request->all();
        $promotions = new Promotions($data);
        $promotions->save();

        return redirect('promotions');
    }

    public function edit($id)
    {
        if(!Permission::check('PROMOTION_UPDATE')) {
            throw new \Exception('You do not have permission');
        }

        $promotion = Promotions::find($id);
        $province = Province::all();

        return view('promotion.edit', ['promotion' => $promotion, 'province' => $province]);
    }

    public function update(Request $request)
    {
        if(!Permission::check('PROMOTION_UPDATE')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'name' => 'required|max:255',
            'cost' => 'required|numeric',
        ]);

        $data = $request->except(['_token', '_method', 'PromotionCode_code']);

        $promotions = new Promotions($data);
        $promotions->where('id', $request->id)->update($data);

        return redirect('promotions');
    }

    public function deletes(Request $request)
    {
        if(!Permission::check('PROMOTION_DELETE')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'list_id' => 'required',]);

        $status_delete = Promotions::whereIn('id', $request->list_id)->delete();

        if($status_delete)
            return new JsonResponse(['success' => TRUE, 'message' => 'Đã xóa thành công!']);
        return new JsonResponse(['success' => FALSE, 'message' => $status_delete]);
    }

    public function searchByKeyWord(Request $request)
    {
        $params = $request->all();

        $query = Promotions::query();
        if (is_numeric($params['keyword'])) {
            $query->where('id', '=', $params['keyword']);
        } else {
            $query->orWhere('name', 'like', "%" . $params['keyword'] . "%");
        }

        $promotions = $query->get();

        if (!empty($promotions)) {
            $promotion = current($promotions);
        } else {
            $promotion = [];
        }

        return new JsonResponse($promotion);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $supplier = Promotions::query()->find($id);

        return new JsonResponse($supplier);
    }
}