<?php
/**
 * Created by PhpStorm.
 * User: cuong.ly
 * Date: 15/05/2017
 * Time: 11:15 CH
 */

namespace App\Http\Controllers;


use App\Facade\Permission;
use App\Model\Orders;
use App\Model\Product;
use App\Model\OrderHasProducts;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public $limit = 20;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if(!Permission::check('ORDER_VIEW')) {
            throw new \Exception('You do not have permission');
        }

        $query = Orders::query();
        $query = $query->join('customers', 'customers.id', '=', 'orders.customers_id')
            ->select('orders.*', 'customers.name', 'customers.address', 'customers.phone_work');

        if (!empty($request->keywords)) {

            if (is_numeric($request->keywords )){
                $query = $query->where('orders.id', '=', $request->keywords);
            } else {
                $query = $query->where('customers.name', 'like', '%' . $request->keywords . '%');
            }
        }

        if (!empty($request->order_by) && !empty($request->order)) {
            $query = $query->orderBy($request->order_by, $request->order);
        } else{
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->paginate($this->limit);

        return view('order.index', ['orders' => $data]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function add()
    {
        if(!Permission::check('ORDER_ADD')) {
            throw new \Exception('You do not have permission');
        }

        return view('order.add');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(Request $request)
    {
        if(!Permission::check('ORDER_ADD')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'list_product' => 'required_list',
            'discount' => 'required|numeric',
            'total' => 'required|numeric',
            'id_customer' => 'required|numeric',
        ]);
        //Lập đơn hàng
        $orders = new Orders;
        $orders->total = $request->total;
        $orders->status = 1;
        $orders->customers_id = $request->id_customer;
        $orders->discount = $request->discount;
        $orders->description = $request->Note;
        $orders->users_id = Auth::id();
        $orders->setCreatedAt((new Carbon())->setTimezone(new \DateTimeZone('Asia/Ho_Chi_Minh'))->format('Y-m-d H:i:s'));
        $orders->save();

        //Lập danh sách những mặt hàng có trong đơn hàng
        $list_product = json_decode($request->list_product);
        foreach ($list_product as $key => $item) {
            $data[$key] = [
                'orders_id' => $orders->id,
                'products_id' => $item->products_id,
                'quantity_product' => $item->quantity_product,
                'vat_product' => $item->vat_product,
                'cost_product' => $item->cost_product,
                'total' => $item->total,
                'discount' => $item->discount_product,
                'created_at' => ((new Carbon())->setTimezone(new \DateTimeZone('Asia/Ho_Chi_Minh'))->format('Y-m-d H:i:s'))
            ];
        //Trừ kho
        Product::where('id','=',$item->products_id)->decrement('quantity', $item->quantity_product);
        }
        OrderHasProducts::insert($data);


        return redirect('orders');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        if(!Permission::check('ORDER_UPDATE')) {
            throw new \Exception('You do not have permission');
        }

        $order = Orders::join('customers', 'customers.id', '=', 'orders.customers_id')
            ->select('orders.*', 'customers.name', 'customers.address', 'customers.phone_work')->find($id);

        $list_product = OrderHasProducts::join('products', 'order_has_products.products_id', '=', 'products.id')
        ->select('order_has_products.*','products.name', 'products.quantity as warehouse')
        ->where('orders_id', '=', $id)->get();

        return view('order.edit', ['order' => $order, 'list_product' => $list_product]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update(Request $request)
    {
        if(!Permission::check('ORDER_UPDATE')) {
            throw new \Exception('You do not have permission');
        }


        $this->validate($request, [
            'list_product' => 'required_list',
            'discount' => 'required|numeric',
            'total' => 'required|numeric',
        ]);
        //Xóa danh sách sản phẩm trong đơn hàng
        //Cập nhật lại số lượng kho, + số lượng những thứ được xóa
        $ls_add = OrderHasProducts::where('orders_id', '=', $request->id)->get();
        foreach ($ls_add as $i => $item) {
            Product::where('id','=',$item->products_id)->increment('quantity', $item->quantity_product);
        }

        OrderHasProducts::where('orders_id', '=', $request->id)->delete();
        $list_product = json_decode($request->list_product);
//dd($list_product);
        if (!empty($list_product)) {
            foreach ($list_product as $key => $item) {
                $data[$key] = [
                    'orders_id' => $request->id,
                    'products_id' => $item->products_id,
                    'quantity_product' => $item->quantity_product,
                    'vat_product' => $item->vat_product,
                    'cost_product' => $item->cost_product,
                    'total' => $item->total,
                    'discount' => $item->discount_product
                ];
            //Cập nhật lại số lượng kho, - số lượng những thứ được thêm vào
            Product::where('id','=',$item->products_id)->decrement('quantity', $item->quantity_product);
            }
            //Cập nhật lại những sản phẩm có trong đơn hàng
            OrderHasProducts::insert($data);
        }
        //Cập nhật lại tổng giá trị đơn hàng.
        Orders::where('id', '=', $request->id)->update([
            'total' => $request->total, 
            'discount' => $request->discount,
            'description' => $request->Note
            ]);

        return redirect('orders');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deletes(Request $request)
    {
        if(!Permission::check('ORDER_DELETE')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'list_id' => 'required',]);
        //Cập nhật lại số lượng kho, + số lượng những thứ được xóa
        $ls_add = OrderHasProducts::whereIn('orders_id', $request->list_id)->get();
        foreach ($ls_add as $i => $item) {
            Product::where('id','=',$item->products_id)->increment('quantity', $item->quantity_product);
        }
        OrderHasProducts::whereIn('orders_id', $request->list_id)->delete();
        $status_delete = Orders::whereIn('id', $request->list_id)->delete();

        if($status_delete)
            return new JsonResponse(['success' => TRUE, 'message' => 'Đã xóa thành công!']);
        return new JsonResponse(['success' => TRUE, 'message' => $status_delete]);
    }

    public function search_product(Request $request)
    {
        $params = $request->all();
        $query = Product::query()
            ->select('products.*', 'promotions.cost as discount')
            ->leftJoin('promotions', 'products.promotions_id', '=', 'promotions.id');

        if (is_numeric($params['keyword'])) {
            $query->where('products.id', '=', $params['keyword']);
        } else {
            $query->where('products.name', 'like', "%". $params['keyword'] ."%");
        }
        $query->where('products.quantity','>',0);

        $products = $query->get()->toArray();
        return new JsonResponse($products);
    }

}