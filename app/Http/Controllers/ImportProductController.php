<?php
/**
 * Created by PhpStorm.
 * User: cuong.ly
 * Date: 15/05/2017
 * Time: 11:15 CH
 */

namespace App\Http\Controllers;


use App\Facade\Permission;
use App\Model\ImportHasProduct;
use App\Model\ImportProduct;
use App\Model\Product;
use App\Model\Suppliers;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ImportProductController extends Controller
{
    public $limit = 20;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if (!Permission::check('PRODUCT_IMPORT_VIEW')) {
            throw new \Exception('You do not have permission');
        }

        $query = ImportProduct::query();

        if (!empty($request->keywords)) {
            if (is_numeric($request->keywords)) {
                $query = $query->where('id', '=', $request->keywords);
            } else {
                $query = $query->where('name', 'like', '%' . $request->keywords . '%')
                    ->orWhere('provider', 'like', '%' . $request->keywords . '%');
            }
        }

        if (!empty($request->order_by) && !empty($request->order)) {
            $query = $query->orderBy($request->order_by, $request->order);
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->paginate($this->limit);

        return view('importproduct.index', ['importproduct' => $data]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function add()
    {
        if (!Permission::check('PRODUCT_IMPORT_ADD')) {
            throw new \Exception('You do not have permission');
        }

        $supplier = Suppliers::all();
        return view('importproduct.add', ['supplier' => $supplier]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function productaddquick(Request $request)
    {

        if (!Permission::check('PRODUCT_IMPORT_ADD')) {
            throw new \Exception('You do not have permission');
        }

        $params = $request->all();
        if ($request->getMethod() === 'POST') {
            $this->validate($request, [
                'name' => 'required|max:255',
                'description' => 'required',
                'cost' => 'required',
                'quantity' => 'required|integer',
                'suppliers_id' => 'required|max:160',
                //'promotions_id' => 'required|integer',
                //'expired_date' => 'required',
            ]);
            $product = new Product($params);
            $product->setAttribute('expired_date', new \DateTime($params['expired_date']));
            $product->save();
        }
        if ($params['current_id'] > 0) {
            return redirect('importproduct/edit/' . $params['current_id']);
        } else {
            return redirect('add');
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(Request $request)
    {
        if (!Permission::check('PRODUCT_IMPORT_ADD')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'list_product' => 'required_list',
            'total' => 'required|numeric',
        ]);
        //Lập đơn hàng
        $importproduct = new ImportProduct;
        $importproduct->total = $request->total;
        $importproduct->name = $request->name;
        $importproduct->status = $request->status;
        $importproduct->provider = $request->provider;
        $importproduct->users_id = Auth::id();
        $importproduct->setCreatedAt((new Carbon())->setTimezone(new \DateTimeZone('Asia/Ho_Chi_Minh'))->format('Y-m-d H:i:s'));
        $importproduct->save();

        //Lập danh sách những mặt hàng có trong đơn hàng
        $list_product = json_decode($request->list_product);
        foreach ($list_product as $key => $item) {
            $data[$key] = [
                'import_product_id' => $importproduct->id,
                'products_id' => $item->products_id,
                'quantity_product' => $item->quantity_product,
                'cost_product' => $item->cost_product,
                'total' => $item->total,
                'created_at' => ((new Carbon())->setTimezone(new \DateTimeZone('Asia/Ho_Chi_Minh'))->format('Y-m-d H:i:s'))
            ];
            //Cộng kho
            Product::where('id', '=', $item->products_id)->increment('quantity', $item->quantity_product);
        }
        ImportHasProduct::insert($data);


        return redirect('importproduct');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        if (!Permission::check('PRODUCT_IMPORT_UPDATE')) {
            throw new \Exception('You do not have permission');
        }

        $supplier = Suppliers::all();
        $importproduct = ImportProduct::query()->select()->find($id);
        $list_product = ImportHasProduct::join('products', 'import_has_product.products_id', '=', 'products.id')
            ->select('import_has_product.*', 'products.name')
            ->where('import_product_id', '=', $id)->get();
        return view('importproduct.edit', ['importproduct' => $importproduct, 'list_product' => $list_product, 'supplier' => $supplier]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update(Request $request)
    {
        if (!Permission::check('PRODUCT_IMPORT_UPDATE')) {
            throw new \Exception('You do not have permission');
        }

        /*$this->validate($request, [
            'list_product' => 'required_list',
            'total' => 'required|numeric',
        ]);*/

        //Xóa danh sách sản phẩm trong đơn hàng
        //Cập nhật lại số lượng kho, + số lượng những thứ được xóa
        $ls_add = ImportHasProduct::where('import_product_id', '=', $request->id)->get();
        foreach ($ls_add as $i => $item) {
            Product::where('id', '=', $item->products_id)->decrement('quantity', $item->quantity_product);
        }

        ImportHasProduct::where('import_product_id', '=', $request->id)->delete();
        $list_product = json_decode($request->list_product);

        if (!empty($list_product)) {
            foreach ($list_product as $key => $item) {
                $data[$key] = [
                    'import_product_id' => $request->id,
                    'products_id' => $item->products_id,
                    'quantity_product' => $item->quantity_product,
                    'cost_product' => $item->cost_product,
                    'total' => $item->total
                ];
                //Cập nhật lại số lượng kho, + số lượng những thứ được thêm vào
                Product::where('id', '=', $item->products_id)->increment('quantity', $item->quantity_product);
            }
            //Cập nhật lại những sản phẩm có trong đơn hàng
            ImportHasProduct::insert($data);
        }
        //Cập nhật lại tổng giá trị đơn hàng.		
        ImportProduct::where('id', '=', $request->id)->update([
            'name' => $request->name,
            'provider' => $request->provider,
            'discount' => $request->discount_order,
            'status' => $request->status,
            'description' => $request->description,
            'total' => $request->total
        ]);

        return redirect('importproduct');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deletes(Request $request)
    {
        if (!Permission::check('PRODUCT_IMPORT_DELETE')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'list_id' => 'required',]);
        //Cập nhật lại số lượng kho
        $ls_add = ImportHasProduct::whereIn('import_product_id', $request->list_id)->get();
        foreach ($ls_add as $i => $item) {
            Product::where('id', '=', $item->products_id)->decrement('quantity', $item->quantity_product);
        }
        ImportHasProduct::whereIn('import_product_id', $request->list_id)->delete();
        
        $status_delete = ImportProduct::whereIn('id', $request->list_id)->delete();

        if($status_delete)
            return new JsonResponse(['success' => TRUE, 'message' => 'Đã xóa thành công!']);
        return new JsonResponse(['success' => TRUE, 'message' => $status_delete]);
    }

}