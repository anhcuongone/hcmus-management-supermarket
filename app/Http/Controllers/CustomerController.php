<?php
namespace App\Http\Controllers;


use App\Facade\Permission;
use App\Model\Customers;
use App\Model\Province;
use App\Model\District;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public $limit = 20;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if(!Permission::check('CUSTOMER_VIEW')) {
            throw new \Exception('You do not have permission');
        }

        $query = Customers::query();

        if(!empty($request->keywords))
        {
            if (is_numeric($request->keywords)) {
                $query = $query->where('id','=', $request->keywords);
            } else {
                $query = $query->where('name','like','%'.$request->keywords.'%');
            }
        }

        if(!empty($request->order_by) && !empty($request->order))
        {
            $query = $query->orderBy($request->order_by, $request->order);
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->paginate($this->limit);

        return view('customer.index',['customers'=>$data]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function add()
    {
        if(!Permission::check('CUSTOMER_ADD')) {
            throw new \Exception('You do not have permission');
        }

        $province = Province::all();

        return view('customer.add',['province'=>$province]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(Request $request)
    {
        if(!Permission::check('CUSTOMER_ADD')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'name' => 'required|max:255',
            'group_id' => 'required|numeric',
            'phone' => 'required|min:8|max:20',
            'email' => 'required|max:160',
            'gender' => 'required',
            'address' => 'required|max:255',
            'city' => 'required|numeric',
            'district' => 'required|numeric',
            'delivery_address' => 'max:255',
            'email_work' => 'max:160',
            'phone_work' => 'max:20'
        ]);

        $data = $request->all();
        $data['birthday'] = new \DateTime($request->birthday);

        $customers = new Customers($data);
        $customers->save();

        return redirect('customers');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        if(!Permission::check('CUSTOMER_UPDATE')) {
            throw new \Exception('You do not have permission');
        }

        $customer = Customers::find($id);
        $province = Province::all();

        return view('customer.edit', ['customer'=>$customer,'province'=>$province]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update(Request $request)
    {
        if(!Permission::check('CUSTOMER_UPDATE')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'name' => 'required|max:255',
            'group_id' => 'required|numeric',
            'phone' => 'required|min:8|max:20',
            'email' => 'required|max:160',
            'gender' => 'required',
            'address' => 'required|max:225',
            'delivery_address' => 'max:255',
            'city' => 'required|numeric',
            'district' => 'required|numeric',
            'email_work' => 'max:160',
            'phone_work' => 'max:20'
        ]);

        $data = $request->except(['_token','_method','CustomerCode_code']);
        $data['birthday'] = new \DateTime($request->birthday);

        $customers = new Customers($data);
        $customers->where('customers.id',$request->id)->update($data);

        return redirect('customers');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deletes(Request $request)
    {
        if(!Permission::check('CUSTOMER_DELETE')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'list_id' => 'required',]);
        $list_order = \App\Model\Orders::whereIn('customers_id', $request->list_id)->pluck('id');
        //Cập nhật lại số lượng kho, + số lượng những thứ được xóa
        $ls_add = \App\Model\OrderHasProducts::whereIn('orders_id',$list_order)->get();
        foreach ($ls_add as $i => $item) {
            \App\Model\Product::where('id','=',$item->products_id)->increment('quantity', $item->quantity_product);
        }
        \App\Model\OrderHasProducts::whereIn('orders_id', $list_order)->delete();
        \App\Model\Orders::whereIn('customers_id', $request->list_id)->delete();


        $status_delete = Customers::whereIn('id', $request->list_id)->delete();
        
        if($status_delete)
            return new JsonResponse(['success' => TRUE, 'message' => 'Đã xóa thành công!']);
        return new JsonResponse(['success' => TRUE, 'message' => $status_delete]);
    }

    public function get_district(Request $request)
    {
        $district = District::where('provinceid', $request->provinceid)->get();

        return $district;
    }

    public function searchByKeyWord(Request $request)
    {
        $params = $request->all();

        $query = Customers::query();

        if (is_numeric($params['keyword'])) {
            $query->where('id', '=', intval($params['keyword']));
        } else {
            $query->where('name', 'like', "%". $params['keyword'] ."%");
        }

        $customers = $query->get();

        if (!empty($customers)) {
            $customer = current($customers);
        } else {
            $customer = [];
        }

        return new JsonResponse($customer);
    }
}