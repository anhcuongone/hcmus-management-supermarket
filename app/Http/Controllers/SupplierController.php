<?php

namespace App\Http\Controllers;


use App\Facade\Permission;
use App\Model\District;
use App\Model\Suppliers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public $limit = 20;

    public function index(Request $request)
    {
        if(!Permission::check('SUPPLIER_VIEW')) {
            throw new \Exception('You do not have permission');
        }

        $query = Suppliers::query();
        if (!empty($request->keywords)) {
            if (!is_numeric($request->keywords)) {
                $query = $query->where('name', 'like', '%' . $request->keywords . '%');
            } else {
                $query =  $query->where('id', '=', $request->keywords);
            }
        }
        if (!empty($request->order_by) && !empty($request->order)) {
            $query = $query->orderBy($request->order_by, $request->order);
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $data = $query->paginate($this->limit);

        return view('supplier.index', ['suppliers' => $data]);
    }

    public function add()
    {
        if(!Permission::check('SUPPLIER_ADD')) {
            throw new \Exception('You do not have permission');
        }

        return view('supplier.add');
    }

    public function store(Request $request)
    {
        if(!Permission::check('SUPPLIER_ADD')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'name' => 'required|max:255',
            'phone' => 'required|min:8|max:20',
            'address' => 'required|max:160',
        ]);

        $data = $request->all();

        $suppliers = new Suppliers($data);
        $suppliers->save();

        return redirect('suppliers');
    }

    public function edit($id)
    {
        if(!Permission::check('SUPPLIER_UPDATE')) {
            throw new \Exception('You do not have permission');
        }

        $supplier = Suppliers::find($id);

        return view('supplier.edit', ['supplier' => $supplier]);
    }

    public function update(Request $request)
    {
        if(!Permission::check('SUPPLIER_UPDATE')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'id' => 'required|numeric',
            'name' => 'required|max:255',
            'phone' => 'required|min:8|max:20',
            'address' => 'required|max:160',
        ]);

        $data = $request->except(['_token', '_method', 'SupplierCode_code']);

        $supplier = new Suppliers($data);
        $supplier->where('id', $request->id)->update($data);

        return redirect('suppliers');
    }

    public function deletes(Request $request)
    {
        if(!Permission::check('SUPPLIER_DELETE')) {
            throw new \Exception('You do not have permission');
        }

        $this->validate($request, [
            'list_id' => 'required',]);
        \App\Model\Product::whereIn('suppliers_id', $request->list_id)->delete();

        $status_delete = Suppliers::whereIn('id', $request->list_id)->delete();

        if($status_delete)
            return new JsonResponse(['success' => TRUE, 'message' => 'Đã xóa thành công!']);
        return new JsonResponse(['success' => FALSE, 'message' => $status_delete]);
    }

    public function get_district(Request $request)
    {
        $district = District::where('provinceid', $request->provinceid)->get();

        return $district;
    }

    public function searchByKeyWord(Request $request)
    {
        $params = $request->all();

        $query = Suppliers::query();

        if (is_numeric($params['keyword'])) {
            $query = $query->where('id', '=', $params['keyword']);
        } else {
            $query = $query->where('name', 'like', "%" . $params['keyword'] . "%");
        }

        $suppliers = $query->get();

        if (!empty($suppliers)) {
            $supplier = current($suppliers);
        } else {
            $supplier = [];
        }

        return new JsonResponse($supplier);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function searchByName(Request $request)
    {
        $params = $request->all();

        $query = Suppliers::query();

        if (is_numeric($params['keyword'])) {
            $query = $query->where('id', '=', $params['keyword']);
        } else {
            $query = $query->where('name', 'like', "%" . $params['keyword'] . "%");
        }

        $query->limit(15);
        $suppliers = $query->get();

        return new JsonResponse($suppliers);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $supplier = Suppliers::query()->find($id);

        return new JsonResponse($supplier);
    }
}