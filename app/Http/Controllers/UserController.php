<?php
namespace App\Http\Controllers;


use App\Facade\Permission;
use App\Model\Role;
use App\Model\UserRole;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile(Request $request)
    {
        $params = $request->all();
        $user = User::query()->find(Auth::user()->getAuthIdentifier());
        if ($request->getMethod() == 'POST' && !empty($user)) {
            $user->name = $params['name'];
            $user->email = $params['email'];
            $user->save();

            return view('user.profile', ['success' => true, 'user' => $user->toArray()]);
        }

        return view('user.profile', ['user' => $user->toArray()]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function getUsers(Request $request)
    {
        if(!Permission::check('USER_VIEW')) {
            throw new \Exception('You do not have permission');
        }

        $params = $request->all();
        $query = User::query();

        if (!empty($params['search'])) {
            $query->where('users.name', 'like', '%' . $params['search'] . '%')
                ->orWhere('users.email', '=' , $params['search']);
        }

        $users = $query->paginate(20);
        $roles = Role::all();

        return view('user.users', ['users' => $users, 'roles' => $roles]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function editUser(Request $request)
    {
        if(!Permission::check('USER_UPDATE')) {
            throw new \Exception('You do not have permission');
        }

        $rules = [
            'name' => 'required',
            'email' => 'required'
        ];

        $this->validate($request, $rules);
        $params = $request->all();

        if ($request->getMethod() == 'POST') {
            $user = User::query()->find($params['user_id']);
            if (empty($user)) {
                $user = new User();
            } else {
                DB::table('user_roles')->where('user_id', '=', $params['user_id'])->delete();
            }

            $user->name = $params['name'];
            $user->email = $params['email'];
            if (!empty($params['password'])) {
                $user->password = bcrypt($params['password']);
            }

            $user->save();

            if (!empty($params['roles'])) {
                foreach ($params['roles'] as $role) {
                    $data = [
                        'user_id' => $user->id,
                        'role_id' => $role
                    ];
                    $userRole = new UserRole($data);
                    $userRole->save();
                }
            }
        }


        return redirect('users');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserRoles(Request $request)
    {

        $rules = [
            'id' => 'required',
        ];

        $this->validate($request, $rules);

        $param = $request->all();
        $roles = UserRole::query()->where('user_id', '=', $param['id'])->get();

        return response()->json(['roles' => $roles]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPermission(Request $request)
    {
        $param = $request->all();
        $permissions = Role::query()
            ->join('role_permissions', 'role_permissions.role_id', '=', 'roles.id')
            ->where('role_permissions.role_id', '=', $param['id'])
            ->select('role_permissions.*')
            ->get();

        return response()->json(['permissions' => $permissions]);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        if(!\App\Facade\Permission::check('USER_DELETE')) {
            throw new \Exception('You do not have permission');
        }

        try {
            DB::beginTransaction();

            UserRole::query()->where('user_roles.user_id', '=', $id)->delete();
            User::query()->find($id)->delete();

            DB::commit();
        }
        catch (\Exception $exception) {
            DB::rollBack();
            $errors = ['message' => 'Xóa không thành công'];
            return redirect('users')->withErrors($errors, $this->errorBag());
        }

        $errors = ['success' => 'Xóa thành công'];
        return redirect('users')->with($errors);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteAll(Request $request)
    {
        if(!\App\Facade\Permission::check('USER_DELETE')) {
            return new JsonResponse(['success' => false, 'error' => 'Bạn không có quyền xóa']);
        }

        $params = $request->all();

        if (empty($params['ids'])) {
            return new JsonResponse(['success' => false, 'error' => 'Hạy chọn users để xóa']);
        }

        try {
            DB::beginTransaction();

            UserRole::query()->whereIn('user_roles.user_id', $params['ids'])->delete();
            User::query()->whereIn('users.id', $params['ids'])->delete();

            DB::commit();
        }
        catch (\Exception $exception) {
            DB::rollBack();
            return new JsonResponse(['success' => false, 'error' => 'Xóa không thành công']);
        }

        return new JsonResponse(['success' => true, 'error' => 'Xóa thành công']);
    }
}