<?php
/**
 * Created by PhpStorm.
 * User: cuong.ly
 * Date: 12/05/2017
 * Time: 11:28 CH
 */

namespace App\Http\Controllers;


use App\Facade\Permission;
use App\Model\ImportHasProduct;
use App\Model\OrderHasProducts;
use App\Model\Product;
use App\Model\Suppliers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class ProductsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if(!Permission::check('PRODUCT_VIEW')) {
            throw new \Exception('You do not have permission');
        }

        $params = $request->all();

        $query = DB::table('products')
            ->join('suppliers', 'suppliers.id', '=', 'products.suppliers_id');
        if (isset($params['search'])) {
            $search = $params['search'];
            $query->where('products.name', 'like', "%$search%");
        }

        $query->orderBy('products.updated_at', 'desc');
        $query->select('products.*', 'suppliers.name AS supplier_name');

        $products = $query->paginate(20);

        return view('products.products', ['products' => $products]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Exception
     */
    public function store(Request $request)
    {
        if(!Permission::check('PRODUCT_ADD')) {
            throw new \Exception('You do not have permission');
        }

        $params = $request->all();
        if ($request->getMethod() === 'POST') {

            $this->validate($request, [
                'name' => 'required|max:255',
                'cost' => 'required',
                'quantity' => 'required|integer',
                'suppliers_id' => 'required|max:160',
                'expired_date' => 'required',
            ]);

            $product = new Product($params);
            $product->setAttribute('expired_date', new \DateTime($params['expired_date']));
            $product->save();

            return redirect('products');
        }

        return view('products.add');
    }

    /**
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit(Request $request, $id = null)
    {
        if(!Permission::check('PRODUCT_UPDATE')) {
            throw new \Exception('You do not have permission');
        }

        if (!empty($id)) {
            $product = Product::query()->find($id);
        } else {
            throw new \Exception('Product not found');
        }

        if ($request->getMethod() === 'POST') {
            $params = $request->all();
            $this->validate($request, [
                'name' => 'required|max:255',
                'cost' => 'required',
                'quantity' => 'required|integer',
                'suppliers_id' => 'required|integer',
                'expired_date' => 'required',
            ]);

            $product->fill($params);
            $product->setAttribute('expired_date', new \DateTime($params['expired_date']));
            $product->save();

            return redirect('products');
        }

        return view('products.edit', ['product' => $product]);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function delete($id)
    {
        if(!Permission::check('PRODUCT_DELETE')) {
            throw new \Exception('You do not have permission');
        }

        try {
            DB::beginTransaction();

            $product = Product::query()->where('products.id', '=', $id);
            if (empty($product)) {
                $errors = ['message' => 'Không tìm thấy hàng hóa'];
                return redirect('products')->withErrors($errors, $this->errorBag());
            }

            OrderHasProducts::query()->where('order_has_products.products_id', '=', $id)->delete();
            ImportHasProduct::query()->where('import_has_product.products_id', '=', $id)->delete();

            $product->delete();
            DB::commit();
        }
        catch (\Exception $exception) {
            DB::rollBack();
            $errors = ['message' => 'Xóa không thành công'];
            return redirect('products')->withErrors($errors, $this->errorBag());
        }

        $errors = ['success' => 'Xóa thành công'];
        return redirect('products')->with($errors);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteAll(Request $request)
    {
        if(!Permission::check('PRODUCT_DELETE')) {
            return new JsonResponse(['success' => false, 'error' => 'Bạn không có quyền xóa']);
        }

        $params = $request->all();

        if (empty($params['ids'])) {
            return new JsonResponse(['success' => false, 'error' => 'Hạy chọn sản phẩm để xóa']);
        }

        try {
            DB::beginTransaction();

            OrderHasProducts::query()->whereIn('order_has_products.products_id', $params['ids'])->delete();
            ImportHasProduct::query()->whereIn('import_has_product.products_id',  $params['ids'])->delete();
            Product::query()->whereIn('products.id', $params['ids'])->delete();

            DB::commit();
        }
        catch (\Exception $exception) {
            DB::rollBack();
            return new JsonResponse(['success' => false, 'error' => 'Xóa không thành công']);
        }

        return new JsonResponse(['success' => true, 'error' => 'Xóa thành công']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function searchByKeyword(Request $request)
    {
        $params = $request->all();
        $query = Product::query()
            ->select('products.*', 'promotions.cost as discount')
            ->leftJoin('promotions', 'products.promotions_id', '=', 'promotions.id');

        if (is_numeric($params['keyword'])) {
            $query->where('products.id', '=', $params['keyword']);
        } else {
            $query->where('products.name', 'like', "%". $params['keyword'] ."%");
        }

        $products = $query->get()->toArray();
        return new JsonResponse($products);
    }
}