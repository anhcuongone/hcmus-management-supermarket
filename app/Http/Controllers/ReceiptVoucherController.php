<?php

namespace App\Http\Controllers;

use App\Model\ReceiptVoucher;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ReceiptVoucherController extends Controller
{
    public $limit = 20;

    public function index(Request $request)
    {
        $query = ReceiptVoucher::query();
        if(!empty($request->keywords))
        {
            $query = $query->where('name_customer','like','%'.$request->keywords.'%')->orWhere('id','like','%'.$request->keywords.'%');
        }
        if(!empty($request->order_by) && !empty($request->order))
        {
            $query = $query->orderBy($request->order_by, $request->order);
        }
        $data = $query->paginate($this->limit);

        return view('receipt_voucher.index',['receipt_vouchers'=>$data]);
    }

    public function add()
    {
    	return view('receipt_voucher.add');
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
	        'type_customer' => 'required',
	        'name_customer' => 'required|alpha_num',
	        'type_voucher' => 'required|numeric',
	        'branch_id' => 'required|numeric',
	        'description' => 'alpha',
	        'method' => 'required|numeric',
	        'cash' => 'required|numeric',
	    ]);

	    $data = $request->all();
	    // $data['birthday'] = new \DateTime($request->birthday);
	    
	    $receipt_voucher = new ReceiptVoucher($data);
        $receipt_voucher->save();

        return redirect('receipt_vouchers');
    }

    public function edit($id)
    {
    	$receipt_voucher = ReceiptVoucher::find($id);

    	return view('receipt_voucher.edit',['receipt_voucher'=>$receipt_voucher]);
    }

    public function update(Request $request)
    {
    	$this->validate($request, [
	        'type_customer' => 'required',
	        'name_customer' => 'required|alpha_num',
	        'type_voucher' => 'required|numeric',
	        'branch_id' => 'required|numeric',
	        'description' => 'alpha',
	        'method' => 'required|numeric',
	        'cash' => 'required|numeric',
	    ]);

	    $data = $request->except(['_token','_method','IssuedOn','Counted']);
	    // $data['birthday'] = new \DateTime($request->birthday);

        $receipt_voucher = new ReceiptVoucher($data);
        $receipt_voucher->where('id',$request->id)->update($data);

        return redirect('receipt_vouchers');
    }

    public function delete(Request $request)
    {
    	$this->validate($request, [
	        'id' => 'required|numeric',]);
    	$receipt_voucher = ReceiptVoucher::find($request->id)->delete();

    	return redirect('receipt_vouchers');
    }

    public function deletes(Request $request)
    {
        $this->validate($request, [
            'list_id' => 'required',]);
        return new JsonResponse(['success' => TRUE, 'error' => ReceiptVoucher::whereIn('id', $request->list_id)->delete()]);
        return new JsonResponse(['success' => FALSE, 'error' => "Có lỗi trong quá trình xóa"]);
    }
}
