<?php
/**
 * Created by PhpStorm.
 * User: cuong.ly
 * Date: 15/05/2017
 * Time: 10:28 CH
 */

namespace App\Http\Controllers;


use App\Facade\Permission;
use App\Model\ImportHasProduct;
use App\Model\ImportProduct;
use App\Model\OrderHasProducts;
use App\Model\Orders;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends  Controller
{
    public function index()
    {
        if(!Permission::check('REPORT_VIEW')) {
            throw new \Exception('You do not have permission');
        }

        return view('report.report');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getReportOrders(Request $request)
    {
        $params = $request->all();
        $range = $this->getRangeDate($params);

        $query = Orders::query()
            ->where('created_at' , '>=', $range['from'])
            ->where('created_at' , '<=', $range['to']);

        $reports = $query->get();

        $results = [];
        $categories = [];
        $data = [];
        if (!empty($reports)) {
            foreach ($reports as $report) {
                $report = $report->toArray();
                $date = $this->formatDate($report, $params['type']);

                if (!isset($results[$date])) {
                    $results[$date] = 0;
                }

                $results[$date] += 1;
                $categories[$date] = $date;
            }

            $data[] = ['name' => 'Hóa đơn bán ra', 'data' => array_values($results)];
        }

        return new JsonResponse(['data' => $data, 'categories' => array_values($categories)]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getReportProducts(Request $request)
    {
        $params = $request->all();
        $range = $this->getRangeDate($params);

        $query = OrderHasProducts::query()
            ->where('created_at' , '>=', $range['from'])
            ->where('created_at' , '<=', $range['to']);

        $reports = $query->get();

        $results = [];
        $categories = [];
        $data = [];
        if (!empty($reports)) {
            foreach ($reports as $report) {
                $report = $report->toArray();
                $date = $this->formatDate($report, $params['type']);

                if (!isset($results[$date])) {
                    $results[$date] = 0;
                }

                $results[$date] += 1;
                $categories[$date] = $date;
            }

            $data[] = ['name' => 'Sản phẩm bán ra', 'data' => array_values($results)];
        }

        return new JsonResponse(['data' => $data, 'categories' => array_values($categories)]);
    }

    /**
     * @param Request $request
     * @return bool
     * @throws \Exception
     */
    public function exportOrders(Request $request)
    {
        if(!Permission::check('REPORT_EXPORT')) {
            throw new \Exception('You do not have permission');
        }

        $params = $request->all();
        $range = $this->getRangeDate($params);

        $query = Orders::query()
            ->join('customers', 'customers.id', '=', 'orders.customers_id')
            ->join('users', 'users.id', '=', 'orders.users_id')
            ->where('orders.created_at' , '>=', $range['from'])
            ->where('orders.created_at' , '<=', $range['to'])
            ->select('orders.*', 'customers.name AS customerName', 'users.name as Username');
        ;

        $reports = $query->get();

        $results = [];
        if (!empty($reports)) {
            foreach ($reports as $report) {
                $report = $report->toArray();
                $results[] = [
                    'Mã hóa đơn'    => $report['id'],
                    'Mã khách hàng' => $report['customers_id'],
                    'Tên khách hàng' => $report['customerName'],
                    'Mã nhân viên' => $report['users_id'],
                    'Tên nhân viên' => $report['Username'],
                    'Tổng số' => $report['total'],
                    'Giảm giá' => $report['discount'],
                    'Trạng thái' => $report['status'] == 1 ? 'Thanh toán' : 'Chưa thanh toán',
                    'Mô tả' => $report['description'],
                    'Ngày lập' => $report['created_at'],
                ];
            }
        }

        Excel::create('HoaDon', function($excel) use ($results) {
            $excel->sheet('HoaDon', function($sheet) use ($results) {
                $sheet->fromArray($results ,null, 'A1', false, true);

            });
        })->export('xlsx');

        return true;
    }

    /**
     * @param Request $request
     * @return bool
     * @throws \Exception
     */
    public function exportOrderDetail(Request $request)
    {
        if(!Permission::check('REPORT_EXPORT')) {
            throw new \Exception('You do not have permission');
        }

        $params = $request->all();
        $range = $this->getRangeDate($params);

        $query = OrderHasProducts::query()
            ->join('orders', 'orders.id', '=' , 'order_has_products.orders_id')
            ->join('products', 'products.id', '=' , 'order_has_products.products_id')
            ->where('order_has_products.created_at' , '>=', $range['from'])
            ->where('order_has_products.created_at' , '<=', $range['to'])
            ->select('order_has_products.*', 'orders.id AS orderId', 'products.name as productName', 'products.id AS productId')
        ;

        $reports = $query->get();

        $results = [];
        if (!empty($reports)) {
            foreach ($reports as $report) {
                $report = $report->toArray();
                $results[] = [
                    'Mã hóa đơn'    => $report['orderId'],
                    'Mã sản phẩm'    => $report['productId'],
                    'Tên sản phẩm' => $report['productName'],
                    'Giá' => $report['cost_product'],
                    'Số lượng' => $report['quantity_product'],
                    'Vat' => $report['vat_product'],
                    'Giảm giá' => $report['discount'],
                    'Tổng tiền' => $report['total'],
                ];
            }
        }

        Excel::create('ChiTietHoaDon', function($excel) use ($results) {
            $excel->sheet('ChiTietHoaDon', function($sheet) use ($results) {
                $sheet->fromArray($results ,null, 'A1', false, true);

            });
        })->export('xlsx');

        return true;

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getReportImports(Request $request)
    {
        $params = $request->all();
        $range = $this->getRangeDate($params);

        $query = ImportProduct::query()
            ->where('created_at' , '>=', $range['from'])
            ->where('created_at' , '<=', $range['to']);

        $reports = $query->get();

        $results = [];
        $categories = [];
        $data = [];
        if (!empty($reports)) {
            foreach ($reports as $report) {
                $report = $report->toArray();
                $date = $this->formatDate($report, $params['type']);

                if (!isset($results[$date])) {
                    $results[$date] = 0;
                }

                $results[$date] += 1;
                $categories[$date] = $date;
            }

            $data[] = ['name' => 'Hóa đơn nhập vào', 'data' => array_values($results)];
        }

        return new JsonResponse(['data' => $data, 'categories' => array_values($categories)]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getReportImportProducts(Request $request)
    {
        $params = $request->all();
        $range = $this->getRangeDate($params);

        $query = ImportHasProduct::query()
            ->where('created_at' , '>=', $range['from'])
            ->where('created_at' , '<=', $range['to']);

        $reports = $query->get();

        $results = [];
        $categories = [];
        $data = [];
        if (!empty($reports)) {
            foreach ($reports as $report) {
                $report = $report->toArray();
                $date = $this->formatDate($report, $params['type']);

                if (!isset($results[$date])) {
                    $results[$date] = 0;
                }

                $results[$date] += 1;
                $categories[$date] = $date;
            }

            $data[] = ['name' => 'Sản phẩm nhập vào', 'data' => array_values($results)];
        }

        return new JsonResponse(['data' => $data, 'categories' => array_values($categories)]);
    }

    /**
     * @param Request $request
     * @return bool
     * @throws \Exception
     */
    public function exportImportProducts(Request $request)
    {
        if(!Permission::check('REPORT_EXPORT')) {
            throw new \Exception('You do not have permission');
        }

        $params = $request->all();
        $range = $this->getRangeDate($params);

        $query = ImportProduct::query()
            ->join('users','users.id','=','import_product.users_id')
            ->where('import_product.created_at' , '>=', $range['from'])
            ->where('import_product.created_at' , '<=', $range['to'])
            ->select('import_product.*', 'users.name AS Username', 'users.id AS userId')
        ;

        $reports = $query->get();

        $results = [];
        if (!empty($reports)) {
            foreach ($reports as $report) {
                $report = $report->toArray();
                $results[] = [
                    'Mã hóa đơn'    => $report['id'],
                    'Nhà cung cấp' => $report['provider'],
                    'Mã nhân viên' => $report['userId'],
                    'Tên nhân viên' => $report['Username'],
                    'Tổng số' => $report['total'],
                    'Giảm giá' => $report['discount'],
                    'Trạng thái' => $report['status'] == 1 ? 'Thanh toán' : 'Chưa thanh toán',
                    'Mô tả' => $report['description'],
                    'Ngày lập' => $report['created_at'],
                ];
            }
        }

        Excel::create('HoaDonNhapHang', function($excel) use ($results) {
            $excel->sheet('HoaDonNhapHang', function($sheet) use ($results) {
                $sheet->fromArray($results ,null, 'A1', false, true);

            });
        })->export('xlsx');

        return true;

    }

    /**
     * @param Request $request
     * @return bool
     * @throws \Exception
     */
    public function exportImportDetailProduct(Request $request)
    {
        if(!Permission::check('REPORT_EXPORT')) {
            throw new \Exception('You do not have permission');
        }

        $params = $request->all();
        $range = $this->getRangeDate($params);

        $query = ImportHasProduct::query()
            ->join('import_product', 'import_product.id', '=', 'import_has_product.import_product_id')
            ->join('products', 'products.id', '=', 'import_has_product.products_id')
            ->where('import_has_product.created_at' , '>=', $range['from'])
            ->where('import_has_product.created_at' , '<=', $range['to'])
            ->select('import_has_product.*', 'import_product.id AS importId', 'products.id AS productId', 'products.name AS productName')
        ;

        $reports = $query->get();

        $results = [];
        if (!empty($reports)) {
            foreach ($reports as $report) {
                $report = $report->toArray();
                $results[] = [
                    'Mã hóa đơn'    => $report['importId'],
                    'Mã sản phẩm'    => $report['productId'],
                    'Tên sản phẩm' => $report['productName'],
                    'Giá' => $report['cost_product'],
                    'Số lượng' => $report['quantity_product'],
                    'Tổng tiền' => $report['total']
                ];
            }
        }

        Excel::create('ChiTietHDNhapHang', function($excel) use ($results) {
            $excel->sheet('ChiTietHDNhapHang', function($sheet) use ($results) {
                $sheet->fromArray($results ,null, 'A1', false, true);

            });
        })->export('xlsx');

        return true;
    }



    /**
     * @param $params
     * @return array
     */
    protected function getRangeDate($params)
    {
        $from = null;
        $to = null;

        switch ($params['type']) {
            case '1':
                $from = (new \DateTime($params['from']))->format('Y-m-d');
                $to = (new \DateTime($params['to']))->format('Y-m-d');
                break;
            case '2':
                $fromDate = \DateTime::createFromFormat('d-m-Y', '01-' . $params['from']);
                $toDate = \DateTime::createFromFormat('d-m-Y', '01-' . $params['to']);
                $from = $fromDate->format('Y-m-d');
                $to = $toDate->add(new \DateInterval('P1M'))->modify('-1 day')->format('Y-m-d');
                break;
            case '3':
                $fromDate = \DateTime::createFromFormat('d-m-Y', '01-01-' . $params['from']);
                $toDate = \DateTime::createFromFormat('d-m-Y', '01-12-' . $params['to']);

                $from = $fromDate->format('Y-m-d');
                $to = $toDate->format('Y-m-d');
                break;
        }

        return ['from' => $from, 'to' => $to];
    }

    /**
     * @param $params
     * @param $type
     * @return string
     */
    protected function formatDate($params, $type)
    {
        $date = (new \DateTime($params['created_at']))->format('Y-m-d');
        switch ($type) {
            case '1':
                $date = (new \DateTime($params['created_at']))->format('Y-m-d');
                break;
            case '2':
                $date = (new \DateTime($params['created_at']))->format('Y-m');
                break;
            case '3':
                $date = (new \DateTime($params['created_at']))->format('Y');
                break;
        }

        return $date;
    }
}