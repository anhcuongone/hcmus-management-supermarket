<?php

namespace App\Http\Controllers;

use App\Model\Permission;
use App\Model\Role;
use App\Model\RolePermission;
use App\Model\UserRole;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class RoleController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function editRole(Request $request)
    {
        $rules = [
            'name' => 'required'
        ];

        $this->validate($request, $rules);
        $params = $request->all();
        if (empty($params['role_id'])) {
            if(!\App\Facade\Permission::check('ROLE_ADD')) {
                throw new \Exception('You do not have permission');
            }
        } else {
            if(!\App\Facade\Permission::check('ROLE_EDIT')) {
                throw new \Exception('You do not have permission');
            }
        }

        if ($request->getMethod() == 'POST') {
            $role = Role::query()->find($params['role_id']);
            if (empty($role)) {
                $role = new Role($params);
            } else {
                $role->name = $params['name'];
                DB::table('role_permissions')->where('role_id', '=', $params['role_id'])->delete();
            }

            $role->save();
            if (!empty($params['permission_id'])) {
                foreach ($params['permission_id'] as $permission_id)
                {
                    $data = [
                        'role_id' => $role->id,
                        'permission_id' => $permission_id
                    ];

                    $role_permission = new RolePermission($data);
                    $role_permission->save();
                }
            }
        }

        return redirect('roles');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function getRoles(Request $request)
    {
        if(!\App\Facade\Permission::check('ROLE_VIEW')) {
            throw new \Exception('You do not have permission');
        }

        $params = $request->all();
        $query = Role::query();
        if (!empty($params['search']))
        {
            $query->where('roles.name', 'like', '%' . $params['search'] . '%');
        }

        $roles = $query->paginate(20);
        $permissions = Permission::all();

        return view('user.roles', ['roles' => $roles, 'permissions' => $permissions]);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        if(!\App\Facade\Permission::check('ROLE_DELETE')) {
            throw new \Exception('You do not have permission');
        }

        try {
            DB::beginTransaction();

            RolePermission::query()->where('role_permissions.role_id', '=', $id)->delete();
            UserRole::query()->where('user_roles.role_id', $id)->delete();
            Role::query()->find($id)->delete();

            DB::commit();
        }
        catch (\Exception $exception) {
            DB::rollBack();
            $errors = ['message' => 'Xóa không thành công'];
            return redirect('roles')->withErrors($errors, $this->errorBag());
        }

        $errors = ['success' => 'Xóa thành công'];
        return redirect('roles')->with($errors);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteAll(Request $request)
    {
        if(!\App\Facade\Permission::check('ROLE_DELETE')) {
            return new JsonResponse(['success' => false, 'error' => 'Bạn không có quyền xóa']);
        }

        $params = $request->all();

        if (empty($params['ids'])) {
            return new JsonResponse(['success' => false, 'error' => 'Hạy chọn role để xóa']);
        }

        try {
            DB::beginTransaction();

            RolePermission::query()->whereIn('role_permissions.role_id', $params['ids'])->delete();
            UserRole::query()->whereIn('user_roles.role_id', $params['ids'])->delete();
            Role::query()->whereIn('roles.id', $params['ids'])->delete();

            DB::commit();
        }
        catch (\Exception $exception) {
            DB::rollBack();
            return new JsonResponse(['success' => false, 'error' => 'Xóa không thành công']);
        }

        return new JsonResponse(['success' => true, 'error' => 'Xóa thành công']);
    }
}