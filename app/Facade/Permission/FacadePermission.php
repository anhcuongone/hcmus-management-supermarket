<?php

namespace App\Facade\Permission;

use App\Model\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FacadePermission implements IFacadePermission
{
    /**
     * @param string $key
     * @return bool
     */
    public function check($key)
    {
        $num = DB::table('user_roles')
            ->join('role_permissions', 'role_permissions.role_id', '=', 'user_roles.role_id')
            ->join('permissions', 'permissions.id', '=', 'role_permissions.permission_id')
            ->where('user_roles.user_id', Auth::id())
            ->where('permissions.key', trim($key))
            ->count()
        ;

        return $num > 0 ? true : false;
    }
}