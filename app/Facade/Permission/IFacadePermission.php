<?php
/**
 * Created by PhpStorm.
 * User: AnhCuong
 * Date: 09-Apr-17
 * Time: 4:26 PM
 */

namespace App\Facade\Permission;


interface IFacadePermission
{
    /**
     * @param string $key
     * @return mixed
     */
    public function check($key);
}