<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @method static bool check()
 */
class Permission extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Permission';
    }
}