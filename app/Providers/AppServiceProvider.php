<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Validator::extend('required_list', function ($attribute, $value, $parameters, $validator) {
            $list_product = json_decode($value);
            if(empty($list_product))
                return false;
            foreach ($list_product as $i => $item) {
                if($item->quantity_product == NULL || $item->quantity_product == 0 || $item->vat_product == NULL)
                    return false;
            }
            return true;
        });

        \Validator::replacer('required_list', function ($message, $attribute, $rule, $parameters) {
        return "Danh sách sản phẩm không được rỗng, số lượng phải khác 0";
    });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
