<?php
namespace App\Providers;


use App\Facade\Permission\FacadePermission;
use Illuminate\Support\ServiceProvider;

class FacadeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Permission', function (){
            return new FacadePermission();
        });
    }
}