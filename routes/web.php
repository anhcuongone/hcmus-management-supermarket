<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'Auth\LoginController@index');

Auth::routes();

Route::group(['middleware' => 'auth'], function (){
    Route::get('/home', 'HomeController@index');

    Route::any('/profile', 'UserController@profile')->name('profile');
    Route::any('/users', 'UserController@getUsers')->name('users');
    Route::any('/editUser', 'UserController@editUser')->name('edit_user');
    Route::any('/users/delete/{id}', 'UserController@delete')->name('delete_user');
    Route::any('/users/delete_all', 'UserController@deleteAll')->name('delete_all');
    Route::any('/getUserRoles', 'UserController@getUserRoles')->name('getUserRoles');
    Route::any('/getPermission', 'UserController@getPermission')->name('getPermission');

    Route::any('/editRole', 'RoleController@editRole')->name('edit_role');
    Route::any('/roles', 'RoleController@getRoles')->name('roles');
    Route::any('/roles/delete/{id}', 'RoleController@delete')->name('delete_role');
    Route::any('/roles/delete_all', 'RoleController@deleteAll')->name('delete_all');

    Route::get('/customers', 'CustomerController@index')->name('customers');
    Route::get('/customer/add', 'CustomerController@add')->name('customer_add');
    Route::post('/customer/store', 'CustomerController@store')->name('customer_store');
    Route::get('/customer/edit/{id}', 'CustomerController@edit')->name('customer_edit');
    Route::put('/customer/update', 'CustomerController@update')->name('customer_update');
    Route::post('/customer/deletes', 'CustomerController@deletes')->name('customer_deletes');
    Route::get('/customer/get_district', 'CustomerController@get_district')->name('get_district');
    Route::post('/customer/searchByKeyWord', 'CustomerController@searchByKeyWord')->name('customer_searchByKeyWord');

    Route::get('/receipt_vouchers', 'ReceiptVoucherController@index')->name('receipt_vouchers');
    Route::get('/receipt_vouchers/add', 'ReceiptVoucherController@add')->name('receipt_vouchers_add');
    Route::post('/receipt_vouchers/store', 'ReceiptVoucherController@store')->name('receipt_vouchers_store');
    Route::get('/receipt_vouchers/edit/{id}', 'ReceiptVoucherController@edit')->name('receipt_vouchers_edit');
    Route::put('/receipt_vouchers/update', 'ReceiptVoucherController@update')->name('receipt_vouchers_update');
    Route::get('/receipt_vouchers/delete', 'ReceiptVoucherController@delete')->name('receipt_vouchers_delete');
    Route::post('/receipt_vouchers/deletes', 'ReceiptVoucherController@deletes')->name('receipt_vouchers_deletes');

    Route::get('/payment_vouchers', 'PaymentVoucherController@index')->name('payment_vouchers');
    Route::get('/payment_vouchers/add', 'PaymentVoucherController@add')->name('payment_vouchers_add');
    Route::post('/payment_vouchers/store', 'PaymentVoucherController@store')->name('payment_vouchers_store');
    Route::get('/payment_vouchers/edit/{id}', 'PaymentVoucherController@edit')->name('payment_vouchers_edit');
    Route::put('/payment_vouchers/update', 'PaymentVoucherController@update')->name('payment_vouchers_update');
    Route::get('/payment_vouchers/delete', 'PaymentVoucherController@delete')->name('payment_vouchers_delete');
    Route::post('/payment_vouchers/deletes', 'PaymentVoucherController@deletes')->name('payment_vouchers_deletes');

    Route::get('/orders', 'OrderController@index')->name('orders');
    Route::get('/order/add', 'OrderController@add')->name('order_add');
    Route::post('/order/store', 'OrderController@store')->name('order_store');
    Route::get('/order/edit/{id}', 'OrderController@edit')->name('order_edit');
    Route::put('/order/update', 'OrderController@update')->name('order_update');
    Route::post('/order/deletes', 'OrderController@deletes')->name('order_deletes');
    Route::post('/order/search_product', 'OrderController@search_product')->name('search_product');

    Route::get('/suppliers', 'SupplierController@index')->name('suppliers');
    Route::get('/supplier/add', 'SupplierController@add')->name('supplier_add');
    Route::post('/supplier/store', 'SupplierController@store')->name('supplier_store');
    Route::get('/supplier/edit/{id}', 'SupplierController@edit')->name('supplier_edit');
    Route::put('/supplier/update', 'SupplierController@update')->name('supplier_update');
    Route::post('/supplier/deletes', 'SupplierController@deletes')->name('supplier_deletes');
    Route::post('/supplier/searchByKeyWord', 'SupplierController@searchByKeyWord')->name('supplier_searchByKeyWord');
    Route::any('/supplier/searchByName', 'SupplierController@searchByKeyWord')->name('supplier_searchByName');
    Route::get('/supplier/show/{id}', 'SupplierController@show');

    Route::get('/promotions', 'PromotionController@index')->name('promotions');
    Route::get('/promotion/add', 'PromotionController@add')->name('promotion_add');
    Route::post('/promotion/store', 'PromotionController@store')->name('promotion_store');
    Route::get('/promotion/edit/{id}', 'PromotionController@edit')->name('promotion_edit');
    Route::put('/promotion/update', 'PromotionController@update')->name('promotion_update');
    Route::post('/promotion/deletes', 'PromotionController@deletes')->name('promotion_deletes');
    Route::post('/promotion/searchByKeyWord', 'PromotionController@searchByKeyWord')->name('promotion_searchByKeyWord');
    Route::get('/promotion/show/{id}', 'PromotionController@show');

    Route::any('/products', 'ProductsController@index')->name('products');
    Route::any('/products/add', 'ProductsController@store')->name('product_add');
    Route::any('/products/edit/{id}', 'ProductsController@edit')->name('product_edit');
    Route::any('/products/delete/{id}', 'ProductsController@delete')->name('product_delete');
    Route::any('/products/delete_all', 'ProductsController@deleteAll')->name('product_delete_all');
    Route::any('/products/searchByKeyword', 'ProductsController@searchByKeyword')->name('product_searchByKeyword');

    Route::any('/report', 'ReportController@index')->name('report');
    Route::any('/report/getReportOrders', 'ReportController@getReportOrders')->name('getReportOrders');
    Route::any('/report/getReportProducts', 'ReportController@getReportProducts')->name('getReportProducts');
    Route::any('/report/exportOrders', 'ReportController@exportOrders')->name('exportOrders');
    Route::any('/report/exportOrders', 'ReportController@exportOrders')->name('exportOrders');
    Route::any('/report/exportOrderDetail', 'ReportController@exportOrderDetail')->name('exportOrderDetail');
    Route::any('/report/getReportImports', 'ReportController@getReportImports')->name('getReportImports');
    Route::any('/report/getReportImportProducts', 'ReportController@getReportImportProducts')->name('getReportImportProducts');
    Route::any('/report/exportImportProducts', 'ReportController@exportImportProducts')->name('exportImportProducts');
    Route::any('/report/exportImportDetailProduct', 'ReportController@exportImportDetailProduct')->name('exportImportDetailProduct');

    Route::any('/order', 'OrderController@index')->name('order');
    // Route::any('/add', 'OrderController@store')->name('order_add');
	
	
	Route::get('/importproduct', 'ImportProductController@index')->name('importproduct');
	Route::any('/importproduct/productaddquick', 'ImportProductController@productaddquick')->name('importproduct_productaddquick');
    Route::get('/importproduct/add', 'ImportProductController@add')->name('importproduct_add');
    Route::post('/importproduct/store', 'ImportProductController@store')->name('importproduct_store');
    Route::get('/importproduct/edit/{id}', 'ImportProductController@edit')->name('importproduct_edit');
    Route::put('/importproduct/update', 'ImportProductController@update')->name('importproduct_update');
    Route::post('/importproduct/deletes', 'ImportProductController@deletes')->name('importproduct_deletes');
	
	
	//Route::get('/importproduct', 'ImportProductController@index')->name('importproduct');
	//Route::get('/importproduct/getImport', 'ImportProductController@getImport');
	//Route::post('/importproduct/postImport', 'ImportProductController@postImport')->name('import_product');
});

